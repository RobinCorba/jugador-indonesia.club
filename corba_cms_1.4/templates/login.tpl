<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Administration login</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="{if $seo_desc}{$seo_desc}, {/if}Publicis S.E. Asia" />
		<meta name="keywords" content="Publicis, Modem, South East Asia, Indonesia, Singapore, Thailand, Phillipines" />
		<meta name="robots" content="follow,index" />
		<meta name="copyright" content="Robin Corba 2012-2016" />
		
		<link rel="shortcut icon" href="{$base_url}favicon.ico" />
		<link rel="stylesheet" type="text/css" href="{$base_url}templates/css/style_login.css" />
	</head>
	<body>
		<img id="header_login" src="{$base_url}templates/img/corba_cms.jpg" alt="Corba CMS version April '13" />
		<form method="post" action="{$base_url}home">
			<div id="login_panel">
				<h1>Login</h1>
				{if !empty($error)}<p id="error">{$error}</p>{/if}
				{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
				{if !empty($message)}<p id="message">{$message}</p>{/if}
				<table>
					<tr>
						<td>Username</td>
						<td><input type="text" name="user_name" style="width:160px;" /></td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input type="password" name="user_pass" style="width:160px;" /></td>
					</tr>
					<tr>
						<td colspan="2" id="submit"><input type="submit"  value="Login" /></td>
					</tr>
				</table>
			</div>
		</form>
		<div id="copyright">
			<a>Developed by Robin Corba 2010 - 2016 &copy;</a>
		</div>
	</body>
</html>