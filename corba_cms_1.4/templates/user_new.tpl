<h1>Create new account</h1>
{if $user_type != "superadmin"}
	<p id="error">Whoopsy daisy, you aren't allowed to open this page.</p>
{else}
	{if $error}<p id="error">{$error}</p>{/if}
	{if $warning}<p id="warning">{$warning}</p>{/if}
	{if $message}<p id="message">{$message}</p>{/if}
	<form method="post" action="{$base_dir}?p=user_new" />
		<table id="details">
			<tr class="odd">
				<td>Username</td>
				<td><input type="text" name="user_name_new" maxlength="30" value="{$data.user_name}" style="width:150px;" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="password" name="user_pass_new" autocomplete="off" style="width:120px;" /></td>
			</tr>
			<tr class="odd">
				<td>Real name</td>
				<td><input type="text" name="user_name_real" maxlength="50" value="{$data.user_name_real}" style="width:180px;" /></td>
			</tr>
			<tr>
				<td>E-mail address (optional)</td>
				<td><input type="text" name="user_email" autocomplete="off" maxlength="128" style="width:200px;" value="{$data.user_email}" /></td>
			</tr>
			<tr class="odd">
				<td>Account type</td>
				<td>
					<select name="user_type">
						<option value="admin" {if $data.user_type == "admin"}selected{/if}>Standard admin</option>
						<option value="superadmin" {if $data.user_type == "superadmin"}selected{/if}>Super administrator</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:10px;"><input type="submit" value="Save changes" style="float:right;"/></td>
			</tr>
		</table>
	</form>
{/if}