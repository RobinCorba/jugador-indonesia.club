<!-- jQuery dataTables -->
<script type="text/javascript" language="javascript" src="{$base_url}js/grid/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="{$base_url}js/grid/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="{$base_url}js/grid/js/jquery.dataTables.dragndrop.js"></script>
<script type="text/javascript" language="javascript" src="{$base_url}js/grid/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="{$base_url}js/grid/css/demo_table_jui.css">
<link rel="stylesheet" type="text/css" href="{$base_url}js/grid/css/jquery-ui-1.8.4.custom.css">
{literal}
<script type="text/javascript">
$(document).ready
(
    function()
    {
        $(".loading").hide();
        $('.grid').dataTable
        ({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers"
        });
    }
);
</script>
{/literal}