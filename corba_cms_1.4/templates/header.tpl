<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>{if isset($seo_title)}{$seo_title}{else}Administration{/if}</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="{if isset($seo_desc)}{$seo_desc}{/if}" />
		<meta name="keywords" content="Corba CMS{if isset($cms)} {$cms.release_version} ({$cms.release_month} {$cms.release_year}){/if}" />
		<meta name="robots" content="nofollow, noindex" />
		<meta name="copyright" content="Robin Corba{if isset($cms)} 2010-{$cms.release_year}{/if}" />
		
        <link rel="stylesheet" type="text/css" href="{$base_url}templates/css/style.css" />
		<link rel="shortcut icon" href="{$base_url}templates/img/favicon.ico" />
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="{$base_url}js/class.ajax.js"></script>
	</head>
	<body>
        <div id="header"><img src="{$base_url}templates/img/client_logo.jpg" alt="" /></div>
        <div id="container">
            <div id="nav">
                <a href="{$base_url}news">News</a>
                <a href="{$base_url}football">Football</a>
                <a href="{$base_url}?p=statistics">Statistics</a>
                {if isset($user_type) && $user_type == "superadmin"}
                <a href="{$base_url}admin">Administration</a>
                {/if}
                <a href="{$base_url}logout">Logout</a>
            </div>
            <div id="content">