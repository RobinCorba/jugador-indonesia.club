<h1>Edit an existing account</h1>
{if $user_type != "superadmin"}
	<p id="error">Whoopsy daisy, you aren't allowed to open this page.</p>
{else}
	{if $error}<p id="error">{$error}</p>{/if}
	{if $warning}<p id="warning">{$warning}</p>{/if}
	{if $message}<p id="message">{$message}</p>{/if}
	<form method="post" action="{$base_dir}?p=user_edit" />
	<input type="hidden" name="user_id" value="{$data.admin_id}" />
		<table id="details">
			<tr class="odd">
				<td>Username</td>
				<td><input type="text" name="user_name_new" maxlength="30" value="{$data.admin_name}" style="width:150px;" /></td>
			</tr>
			<tr>
				<td>Real name</td>
				<td><input type="text" name="user_name_real" maxlength="50" value="{$data.admin_name_real}" style="width:180px;" /></td>
			</tr>
			<tr class="odd">
				<td>E-mail address (optional)</td>
				<td><input type="text" name="user_email" autocomplete="off" maxlength="128" style="width:200px;" value="{$data.admin_email}" /></td>
			</tr>
			<tr>
				<td>Change password (optional)</td>
				<td><input type="password" name="user_pass_new" autocomplete="off" style="width:120px;" /></td>
			</tr>
			<tr class="odd">
				<td>Account type</td>
				<td>
					<select name="user_type">
						<option value="admin" {if $data.admin_type == "admin"}selected{/if}>Standard admin</option>
						<option value="superadmin" {if $data.admin_type == "superadmin"}selected{/if}>Super administrator</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:10px;"><input type="submit" value="Save changes" style="float:right;"/></td>
			</tr>
		</table>
	</form>
{/if}