<div class="player_card">
    <div class="number">69</div>
    <div class="position">ST</div>
    <div class="name">Messi</div>
    <div class="stats" id="stat-1">
        <span>Scored</span>
        <span>12 goals</span>
    </div>
    <div class="stats" id="stat-2">
        <span>Last goal</span>
        <span>2016-12-24</span>
    </div>
    <div class="stats" id="stat-3">
        <span>Joined</span>
        <span>Sept. 2014</span>
    </div>
    <div class="stats-footer">Active player</div>
    <div class="picture" style="background-image:url('./media/images/player_cards/default_card_picture.png');"></div>
</div>