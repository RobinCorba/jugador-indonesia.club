<?php

    $debug_mode = false;
    
    if($debug_mode)
    {
        ini_set('display_errors',true);
        ERROR_REPORTING(E_ALL);
    }
    else
    {
        ini_set('display_errors', false);
        ERROR_REPORTING(NULL);
    }
	
	// Timezone
	date_default_timezone_set('Asia/Jakarta');
    
    // Define constants
    define("DIR_ADMIN",     "admin");
	define("URL_ROOT",      "http://www.jugador-indonesia.club/");
	define("DIR_ROOT",      "/var/www/jugador-indonesia.club/");
    define("DIR_MODULE",    DIR_ROOT.DIR_ADMIN."/modules/");
    define("DIR_IMG_NEWS",  DIR_ROOT."media/images/news/");
	
	// Load modules
    include(DIR_ROOT.DIR_ADMIN."/library/class.modules.php");
    
	
	// PHP static function library
	
	include("../library/class.database.php");
	// PHP libraries (from CMS)
	include("library/class.security.php");
	$class_security	= new Security();
	include("library/class.common.php");
	
	$class_common	= new Common();
	include("library/class.session.php");
	$class_session	= new Session();
	//include("library/class.user.php");
	//$class_user		= new User();
	//include("library/class.football.php");
	//$class_football		= new Football();
	
    
    
    
	// Smarty setup
	include DIR_ROOT.DIR_ADMIN.'/library/smarty/Smarty.class.php';
	$smarty = new Smarty();
	$smarty->template_dir   = DIR_ROOT.DIR_ADMIN."/templates/";
	$smarty->compile_dir    = DIR_ROOT.DIR_ADMIN."/compile/";
	$smarty->cache_dir      = DIR_ROOT.DIR_ADMIN."/cache/";
	$smarty->caching        = 0;
	$smarty->debugging      = false;
	$smarty->assign("temp_dir", $smarty->template_dir[0]);
	$smarty->assign("base_url", URL_ROOT.DIR_ADMIN."/");
	$smarty->assign("site_url", URL_ROOT);
    
    // Corba CMS information
    $cms                    = array();
    $cms["release_version"] = "1.4";
    $cms["release_year"]    = 2016;
    $cms["release_month"]   = "Oct.";
    $smarty->assign("cms",$cms);
?>