<?php

	class User
	{
		private $table_prefix = "jugador_";
		
		/*
		*	@function createNewUser Saves a new account based upon given information
		*	@param array $data An array containing all information of the new account
		*	@return boolean Returns TRUE if the account is saved and returns FALSE when the saving operation has failed
		*/
		function createNewUser($data)
		{
			$now = time();
			Database::connect();
			return mysql_query("INSERT INTO ".$this->table_prefix."cms_admins
			(admin_id,	admin_name, 			admin_pass,			admin_name_real,				admin_email,				admin_date_created,	admin_last_login,	admin_type) VALUES
			('',				'$data[user_name]',	'$data[user_pass]',	'$data[user_name_real]',		'$data[user_email]',	'$now',						'$now',							'$data[user_type]');");
			Database::disconnect();
		}
		
		/*
		*	@function retrieveUserInfo Loads all account information based upon an account identification number
		*	@param int $user_id Account identification number
		*	@return array All account information or returns FALSE if the account doesn't exists
		*/
		function retrieveUserInfo($user_id)
		{
			Database::connect();
			$user_data_raw = mysql_query("SELECT * FROM ".$this->table_prefix."cms_admins WHERE admin_id = '$user_id' LIMIT 0,1;");
			Database::disconnect();
			if($user_data_raw == false)
			{
				return false;
			}
			else
			{
				$user = mysql_fetch_assoc($user_data_raw);
				$user["admin_date_created_real"]	= date("j M. Y", $user["admin_date_created"]);
				$user["admin_last_login_real"]		= date("j M. Y (H:i", $user["admin_last_login"]);
				return $user;
			}
		}
		
		/*
		*	@function retrieveAllUsers Returns all registered accounts
		*	@param int $start Start offset limit of all accounts
		*	@param int $limit Amount of accounts to be returned
		*	@return array Returns all accounts within the offset or returns FALSE if none are found
		*/
		function retrieveAllUsers($start, $limit)
		{
			if($limit == 0)
			{
				$limit_clause = "";
			}
			else
			{
				$limit_clause =  "LIMIT $start,$limit";
			}
			Database::connect();
			$users_raw = mysql_query("SELECT * FROM ".$this->table_prefix."cms_admins WHERE admin_status != 'removed' ORDER BY admin_name_real ASC $limit_clause;");
			Database::disconnect();
			if($users_raw == false)
			{
				return false;
			}
			else
			{
				while($user = mysql_fetch_assoc($users_raw))
				{
					$user["admin_date_created_real"] = date("d M. Y", $user["admin_date_created"]);
					$user["admin_last_login_real"] = date("d M. Y (H:i", $user["admin_last_login"]);
					$users[] = $user;
				}
				return $users;
			}
		}
		
		/*
		*	@function editUser Changes account information
		*	@param array $user All new and/or old information to be saved
		*	@return boolean Returns TRUE is changes are saved or returns FALSE if the save operation failed
		*/
		function editUser($user)
		{
			if($user["user_pass"] != "")
			{
				$pass = "admin_pass = '$user[user_pass]',";
			}
			$now = time();
			Database::connect();
			$result = mysql_query("UPDATE ".$this->table_prefix."cms_admins SET
			admin_name_real	= '$user[user_name_real]',
			admin_name			= '$user[user_name]',
			$pass
			admin_email			= '$user[user_email]',
			admin_type			= '$user[user_type]'
			WHERE admin_id	= '$user[user_id]';");
			Database::disconnect();
			return $result;
		}
		
		/*
		*	@function deleteUser Sets specific user account to 'removed' status
		*	@param int $user_id Account identification number of the account to be deleted
		*	@return boolean Returns TRUE if the account has been 'removed' or returns FALSE if the remove operation failed
		*/
		function deleteUser($user_id)
		{
			Database::connect();
			$result = mysql_query("UPDATE ".$this->table_prefix."cms_admins SET admin_status = 'removed' WHERE admin_id = '$user_id';");
			Database::disconnect();
			return $result;
		}
		
		/*
		*	@function generatePassword Generates a random password
		*	@return string Returns a random password string
		*/
		function generatePassword()
		{
			$length = 8;
			$password = "";
			$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
			$maxlength = strlen($possible);
			if ($length > $maxlength)
			{
				$length = $maxlength;
			}
			$i = 0; 
			while($i < $length)
			{ 
				$char = substr($possible, mt_rand(0, $maxlength-1), 1);
				if(!strstr($password, $char))
				{ 
					$password .= $char;
					$i++;
				}
			}
			return $password;
		}
		
		/*
		*	@function savePassword Saves a new encrypted password in the database
		*	@param $user_id Account identification number of the account
		*	@param $encrypted_pass The new password to bhe stored (needs to be encrypted first)
		*	@return boolean Returns TRUE if the save operation was a succes or returns FALSE if that's not the case
		*/
		function savePassword($user_id, $encrypted_pass)
		{
			Database::connect();
			$result = mysql_query("UPDATE ".$this->table_prefix."users SET user_pass = '$encrypted_pass' WHERE user_id = '$user_id';");
			Database::disconnect();
			return $result;
		}
	}
?>