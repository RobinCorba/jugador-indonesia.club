<?php
	
	class Session
	{
		private $table_prefix = "cms_";
		
		/*
		*	@function checkLogin checks if the given username and password matches a user account
		*	@param array $user User information (user_name and user_pass)
		*	@return array Returns an array of user data or returns FALSE if username/password combination doesn't match
		*/
		function checkLogin($data)
		{
			$user_raw = DB::query("SELECT * FROM ".$this->table_prefix."admins WHERE admin_name = '".$data["user_name"]."' LIMIT 0,1;");
			if($user_raw == false)
			{
				return false;
			}
			else
			{
				$user = mysqli_fetch_assoc($user_raw);
				if($user["admin_pass"] !== $data["user_pass"])
				{	
					return false;
				}
				else
				{
					$user["admin_last_login_real"] = date("d M. Y (H:i", $user["admin_last_login"]);
					return $user;
				}
			}
		}
		
		/*
		*	@function startSession Creates parameters to start a session
		*	@param array $account Account information (user_id, user_type and display_name)
		*	@param string $key A unique session key
		*	@return boolean Returns TRUE if a new session is made or returns FALSE if that failed
		*/
		function startSession($account, $key)
		{
			$now        = time();
			$this_ip    = $_SERVER["REMOTE_ADDR"];
			$result     = DB::query("INSERT INTO ".$this->table_prefix."sessions (admin_id,session_key,start_time,update_time) VALUES ('$account[admin_id]','$key','$now','$now');");
			DB::query("UPDATE ".$this->table_prefix."cms_admins SET admin_last_login = '$now', admin_last_login_ip = '$this_ip' WHERE admin_id = '$account[admin_id]';");
			if($result)
			{
				$_SESSION["admin_id"]						= $account["admin_id"];
				$_SESSION["admin_last_login_real"]		    = date("d M. Y (H:i", $account["admin_last_login"]);
				$_SESSION["admin_name_real"]			    = $account["admin_name_real"];
				$_SESSION["admin_key"]						= $key;
				$_SESSION["admin_start"]					= $now;
				$_SESSION["admin_type"]						= $account["admin_type"];
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/*
		*	@function checkSession Checks if the current session is still active
		*	@return boolean Returns TRUE is the session is active or returns FALSE if the session has expired
		*/
		function checkSession()
		{
			if(!empty($_SESSION["admin_key"]))
			{
				$id	    = $_SESSION["admin_id"];
				$key    = $_SESSION["admin_key"];
				$start  = $_SESSION["admin_start"];
				$data_raw = DB::query("SELECT * FROM ".$this->table_prefix."sessions WHERE admin_id = '$id' AND session_key = '$key' AND start_time = '$start' LIMIT 0,1;");
				if($data_raw === false)
				{
					// Error connecting to database (might be SQL typo)
					return false;
				}
				else
				{
					if($data = mysqli_fetch_assoc($data_raw))
					{
						// Check if the last activity was over 5 mins ago
						$time_check = time() - $data["update_time"];
						if($time_check > 3599)
						{
							// Session has expired
							return false;
						}
						else
						{
							DB::query("UPDATE ".$this->table_prefix."sessions SET update_time = '".time()."' WHERE admin_id = '$id' AND session_key = '$key' AND start_time = '$start';");
							return true;
						}
					}
					else
					{
						// Failed to retrieve valid session information
						return false;
					}
				}
			}
			else
			{
				return false;
			}
		}
		
		/*
		*	@function terminateSession Ends the current active session
		*/
		function terminateSession($id = "")
		{
            if($id != "")
            {
                DB::query("DELETE FROM ".$this->table_prefix."cms_sessions WHERE admin_id = '".$id."';");
            }
            unset($_SESSION);
            session_destroy();
            return true;
		}
	}
	
?>