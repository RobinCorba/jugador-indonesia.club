<?php

	class Common
	{
		// Class global variables
		var $type;
		var $config;
		
		/*
		*	@function retrieveSiteConfig Get site configuration information from the database
		*	@param string $interface The interface type it applies to (standard, mobile)
		*	@return array Site configuration information
		*/
		function retrieveSiteConfig($interface = "standard")
		{
			$result = DB::query("SELECT * FROM site_config WHERE site_interface = '$interface';");
            if($result === false)
            {
                return false;
            }
			return mysqli_fetch_assoc($result);
		}
		
		/*
		*	@function saveSiteConfiguration Saves the site configuration information in the database
		*	@param array $data The new information to be processed
		*	@return boolean Returns TRUE if the save operation succeeded or returns FALSE if that is not the case
		*/
		function saveSiteConfiguration($data)
		{
			return DB::query("UPDATE site_config SET
			site_url 	          = '$data[site_url]',
			site_root 	          = '$data[site_root]',
			site_title 	          = '$data[site_title]',
			site_description 	  = '$data[site_description]',
			site_keywords 	      = '$data[site_keywords]',
			site_webmaster_name   = '$data[site_webmaster_name]',
			site_webmaster_email  = '$data[site_webmaster_email]'
			WHERE site_interface  = '$data[site_interface]';");
		}
		
		/*
		*	@function getRandomBackground Returns a random image url from a folder
		*	@param string $folder The folder where to get a random image from
		*	@return string The image url
		*/
		function getRandomBackground($folder)
		{
			$total					= 0;
			$dir						= opendir($folder);
			while(($file = readdir($dir)) !== false)
			{
				if(!is_dir($file) && $file != ".." && $file != "." && substr(strrchr($file, '.'),1) != "php" )
				{
					$all_images[] = $file;
					$total++;
				}
			}
			closedir($dir);
			$random = mt_rand(0, $total-1);
			//  Check of er plaatjes aanwezig zijn
			if ($total != 0)
			{
				return $folder . $all_images[$random];
			}
			else
			{
				return $folder . "01.gif";
			}
		}
		
		/*
		*	@function retrieveNavigation Gets all the navigation links
		*	@return array Returns all navigation titles, links etc. or returns FALSE of none are found.
		*/
		function retrieveNavigation()
		{
			$result = DB::query("SELECT page_id, page_nav_title, page_url FROM pages WHERE page_status = 'published' AND page_parent_id = '0' AND page_nav_title != '';") or die ("Error retrieving website navigation (first level): " . mysql_error());
			$menu = array();
			// Main menu
			while($toplevel = mysqli_fetch_assoc($result))
			{
				$main_menu["page_nav_title"]	= $toplevel["page_nav_title"];
				$main_menu["page_url"]				= $toplevel["page_url"];
				
				// Submenu
				$result2 = DB::query("SELECT page_nav_title, page_url FROM pages WHERE page_status = 'published' AND page_parent_id = '$toplevel[page_id]';") or die ("Error retrieving website navigation (second level): " . mysql_error());
				unset($toplevel);
				while($secondlevel = mysqli_fetch_assoc($result2))
				{
					$main_menu["submenu"][]	= $secondlevel;
				}
				$menu[]	= $main_menu;
				unset($main_menu);
			}
			if(!empty($menu))
			{
				return $menu;
			}
			else
			{
				return false;
			}
		}
		
		/*
		*	@function retrievePage Get a specific page from the database
		*	@param int $page_id The page identification number (default is 1)
		*	@return array Returns an array of page information or returns FALSE if no information is found
		*/
		function retrievePage($page_id = 1)
        {
            $result = DB::query("SELECT * FROM pages WHERE page_id = '$page_id' LIMIT 0,1;") or die ("Error retrieving page id '$page_id': " . mysql_error());
            if($result !== false)
            {
            return mysqli_fetch_assoc($result);
            }
            else
            {
            return false;
            }
        }
		
		
		function getPage($id)
		{
			$query		= DB::query("SELECT nav_title AS name, full_title, content, left_widget, right_widget FROM tbl_pagecontent WHERE id = '$id';")or die("Error retrieving page content: ".mysql_error());
			$page_data	= mysqli_fetch_assoc($query);
			return $page_data;
		}
		
		/*
		*	@function getIndonesianMonth Retrieves the Indonesian word for a month
		*	@param int $month_no The number of the month
		*	@return string Name of the month
		*/
		public static function getIndonesianMonth($month_no)
		{
			switch($month_no)
			{
				case 1:
				return "Januari";
				break;
				
				case 2:
				return "Februari";
				break;
				
				case 3:
				return "Maret";
				break;
				
				case 4:
				return "April";
				break;
				
				case 5:
				return "Mei";
				break;
				
				case 6:
				return "Juni";
				break;
				
				case 7:
				return "Juli";
				break;
				
				case 8:
				return "Agustus";
				break;
				
				case 9:
				return "September";
				break;
				
				case 10:
				return "Oktober";
				break;
				
				case 11:
				return "November";
				break;
				
				case 12:
				return "Desember";
				break;
				
				default:
				return false;
				break;
			}
		}
	}
	
?>