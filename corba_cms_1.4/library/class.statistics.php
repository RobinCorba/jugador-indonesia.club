<?php

class Statistics
{
	private $table_prefix = "jugador_";
	
	function retrieveGameStatistics()
	{
		$result_games           = DB::query("SELECT * FROM ".$this->table_prefix."games;");
		$result_opp             = DB::query("SELECT * FROM ".$this->table_prefix."opponents;");
		$result_goals           = DB::query("SELECT * FROM ".$this->table_prefix."goals;");
		$result_clean_sheets    = DB::query("SELECT * FROM ".$this->table_prefix."clean_sheets;");
		$result_players         = DB::query("SELECT * FROM ".$this->table_prefix."players ORDER BY player_display_name ASC;");
		$result_pos             = DB::query("SELECT * FROM ".$this->table_prefix."positions;");
        $games = array();
        while($game = mysqli_fetch_assoc($result_games))
        {
            $games[$game["game_id"]] = $game;
        }
        $opponents = array();
        while($o = mysqli_fetch_assoc($result_opp))
        {
            $opponents[$o["opponent_id"]] = $o["opponent_name"];
        }
        $players = array();
        $stats["players"] = array();
        while($p = mysqli_fetch_assoc($result_players))
        {
            $p["total_goals"]           = 0;
            $p["total_clean_sheets"]    = 0;
            $p["last_goal_date"]        = 00000000;
            $p["last_clean_sheet_date"] = 00000000;
            $month                      = "";
            switch($p["player_since_month"])
            {
                case 1:  $month = "Jan.";    break;
                case 2:  $month = "Feb.";    break;
                case 3:  $month = "Mar.";    break;
                case 4:  $month = "Apr.";    break;
                case 5:  $month = "May";     break;
                case 6:  $month = "June";    break;
                case 7:  $month = "July";    break;
                case 8:  $month = "Aug.";    break;
                case 9:  $month = "Sept.";   break;
                case 10: $month = "Oct.";    break;
                case 11: $month = "Nov.";    break;
                case 12: $month = "Dec.";    break;
            }
            $p["member_since"]        = $month." ".$p["player_since_year"];
            
            if($p["player_stopped_year"] > 0)
            {
                switch($p["player_stopped_month"])
                {
                    case 1:  $month = "Jan.";    break;
                    case 2:  $month = "Feb.";    break;
                    case 3:  $month = "Mar.";    break;
                    case 4:  $month = "Apr.";    break;
                    case 5:  $month = "May";     break;
                    case 6:  $month = "June";    break;
                    case 7:  $month = "July";    break;
                    case 8:  $month = "Aug.";    break;
                    case 9:  $month = "Sept.";   break;
                    case 10: $month = "Oct.";    break;
                    case 11: $month = "Nov.";    break;
                    case 12: $month = "Dec.";    break;
                }
                $p["stopped_on"]        = $month." ".$p["player_stopped_year"];
            }
            
            $p["player_name"]         = $p["player_name"];
            $p["player_display_name"] = $p["player_display_name"];
            $p["player_status_readable"] = "Active player";
            switch($p["player_status"])
            {
                case "former":      $p["player_status_readable"] = "Former player";        break;
                case "inactive":    $p["player_status_readable"] = "Inactive player";      break;
                case "new":         $p["player_status_readable"] = "New player";           break;
                case "active":      $p["player_status_readable"] = "Active player";        break;
                case "top":         $p["player_status_readable"] = "Top player";           break;
                case "legendary":   $p["player_status_readable"] = "Legendary player";     break;
            }
            $stats["players"][$p["player_id"]]  = $p;
        }
        
        $goals = array();
        while($g = mysqli_fetch_assoc($result_goals))
        {
            // Calculate total goals per player
            $stats["players"][$g["goal_player_id"]]["total_goals"] += 1;
            // Date of the last goal
            if(empty($stats["players"][$g["goal_player_id"]]["last_goal_date"]) || $games[$g["goal_game_id"]]["game_date_year"].$games[$g["goal_game_id"]]["game_date_month"].$games[$g["goal_game_id"]]["game_date_day"] > $stats["players"][$g["goal_player_id"]]["last_goal_date"])
            {
                $stats["players"][$g["goal_player_id"]]["last_goal_date"]           = $games[$g["goal_game_id"]]["game_date_year"].$games[$g["goal_game_id"]]["game_date_month"].$games[$g["goal_game_id"]]["game_date_day"];
                $stats["players"][$g["goal_player_id"]]["last_goal_date_formatted"] = $games[$g["goal_game_id"]]["game_date_year"]."-".$games[$g["goal_game_id"]]["game_date_month"]."-".$games[$g["goal_game_id"]]["game_date_day"];
            }
            $goals[$g["goal_id"]] = $g;
        }
        
        $clean_sheets = array();
        while($g = mysqli_fetch_assoc($result_clean_sheets))
        {
            // Calculate total clean sheets per player
            $stats["players"][$g["clean_sheet_player_id"]]["total_clean_sheets"] += 1;
            // Date of the last clean_sheet
            if(empty($stats["players"][$g["clean_sheet_player_id"]]["last_clean_sheet_date"]) || $games[$g["clean_sheet_game_id"]]["game_date_year"].$games[$g["clean_sheet_game_id"]]["game_date_month"].$games[$g["clean_sheet_game_id"]]["game_date_day"] > $stats["players"][$g["clean_sheet_player_id"]]["last_goal_date"])
            {
                $stats["players"][$g["clean_sheet_player_id"]]["last_clean_sheet_date"]           = $games[$g["clean_sheet_game_id"]]["game_date_year"].$games[$g["clean_sheet_game_id"]]["game_date_month"].$games[$g["clean_sheet_game_id"]]["game_date_day"];
                $stats["players"][$g["clean_sheet_player_id"]]["last_clean_sheet_date_formatted"] = $games[$g["clean_sheet_game_id"]]["game_date_year"]."-".$games[$g["clean_sheet_game_id"]]["game_date_month"]."-".$games[$g["clean_sheet_game_id"]]["game_date_day"];
            }
            $clean_sheets[$g["clean_sheet_id"]] = $g;
        }
        
        // Calculate top scorers
        $top_scorers = array();
        foreach($stats["players"] as $player)
        {
            $top_scorers[$player["player_id"]]["player_id"]             = $player["player_id"];
            $top_scorers[$player["player_id"]]["player_name"]           = $player["player_name"];
            $top_scorers[$player["player_id"]]["player_display_name"]   = $player["player_display_name"];
            $top_scorers[$player["player_id"]]["player_picture"]        = $player["player_picture"];
            $top_scorers[$player["player_id"]]["total_goals"]           = $player["total_goals"];
            $top_scorers[$player["player_id"]]["last_goal_date"]        = $player["last_goal_date"];
            
            // In case we have a shared top scorer, prioritize the person who got his goal earlier
            $ts_sort["total_goals"][$player["player_id"]]       = $player["total_goals"];
            $ts_sort["last_goal_date"][$player["player_id"]]    = $player["last_goal_date"];
        }
        array_multisort($ts_sort['total_goals'], SORT_DESC, $ts_sort['last_goal_date'], SORT_ASC, $top_scorers);
        $stats["top_scorers"] = $top_scorers;
        
        // Calculate top defenders/goalkeepers
        $top_defenders = array();
        foreach($stats["players"] as $player)
        {
            $top_defenders[$player["player_id"]]["player_id"]             = $player["player_id"];
            $top_defenders[$player["player_id"]]["player_name"]           = $player["player_name"];
            $top_defenders[$player["player_id"]]["player_display_name"]   = $player["player_display_name"];
            $top_defenders[$player["player_id"]]["player_picture"]        = $player["player_picture"];
            $top_defenders[$player["player_id"]]["total_clean_sheets"]    = $player["total_clean_sheets"];       
            $top_defenders[$player["player_id"]]["last_clean_sheet_date"] = $player["last_clean_sheet_date"];
            
            // In case we have a shared top defender, prioritize the person who got his clean sheet earlier
            $ts_sort["total_clean_sheets"][$player["player_id"]]       = $player["total_clean_sheets"];
            $ts_sort["last_clean_sheet_date"][$player["player_id"]]    = $player["last_clean_sheet_date"];
        }
        array_multisort($ts_sort['total_clean_sheets'], SORT_DESC, $ts_sort['last_clean_sheet_date'], SORT_DESC, $top_defenders);
        $stats["top_defenders"] = $top_defenders;
        
        $positions = array();
        while($pos = mysqli_fetch_assoc($result_pos))
        {
            $positions[$pos["position_id"]] = $p;
        }
		;
		$stats["total"]         = 0;
		$stats["won"]           = 0;
		$stats["drew"]          = 0;
		$stats["lost"]          = 0;
        $stats["biggest_win"]   = array();
        $tmp_biggest_win        = 0;
        // Calculate total games played, won, drew, lost and biggest game won
		foreach($games as $game)
		{
            $stats["total"]++;
            switch($game["game_result"])
            {
                case "won":
                $stats["won"]++;
                $score_saldo = 0;
                $score_saldo = explode("-",$game["game_score"]);
                rsort($score_saldo);
                $score_saldo = $score_saldo[0] - $score_saldo[1];
                if($score_saldo > $tmp_biggest_win)
                {
                    $tmp_biggest_win = $score_saldo;
                    
                    switch($game["game_date_month"])
                    {
                        case 1:  $month = "January";     break;
                        case 2:  $month = "February";    break;
                        case 3:  $month = "March";       break;
                        case 4:  $month = "April";       break;
                        case 5:  $month = "May";         break;
                        case 6:  $month = "June";        break;
                        case 7:  $month = "July";        break;
                        case 8:  $month = "August";      break;
                        case 9:  $month = "September";   break;
                        case 10: $month = "October";     break;
                        case 11: $month = "November";    break;
                        case 12: $month = "December";    break;
                    }
                    $biggest_win_data             = array();
                    $biggest_win_data["date"]     = $game["game_date_day"]." ".$month." ".$game["game_date_year"];
                    $biggest_win_data["score"]    = $game["game_score"];
                    $biggest_win_data["opponent"] = $opponents[$game["game_opponent"]];
                    $stats["biggest_win"]         = $biggest_win_data;
                }
                break;
                
                case "drew":
                $stats["drew"]++;
                break;
                
                case "lost":
                $stats["lost"]++;
                break;
            }
		}
        /*echo"<pre>";
        print_r($stats);
        echo"</pre>";*/
		return $stats;
	}
    
    
}

?>