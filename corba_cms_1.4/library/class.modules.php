<?php
    
	class Modules
	{
        /*
		*	@function load Loads one or multiple modules
		*	@param string $modules Comma separated list of modules to be loaded
		*	@return boolean Returns TRUE if all modules are loaded, returns FALSE if one or more failed to load
		*/
		public static function load($modules)
		{
			$modules = explode(",",trim($modules));
            foreach($modules as $module)
            {
                if(file_exists(DIR_MODULE.$module."/library/class.".$module.".php"))
                {
                    include_once(DIR_MODULE.$module."/library/class.".$module.".php");
                    $new_class_name = "class_".$module;
                    if(class_exists($module))
                    {
                        return new $module();
                    }
                    else
                    {
                        // Class does not exist
                        echo"Class '".$module."' not found.";
                    }
                }
                else
                {
                    // Class file not found
                    echo"File of class '".$module."' not found.";
                }
            }
		}
        
        /*
		*	@function load Loads one or multiple modules
		*	@param string $modules Comma separated list of modules to be loaded
		*	@return boolean Returns TRUE if all modules are loaded, returns FALSE if one or more failed to load
		*/
		public static function call()
		{
            $args       = func_get_args();
            $module     = $args[0];
            $class_name = "class_".$args[0];
            $function   = $args[1];
            $params     = array_slice($args,2);
            if(!class_exists($class_name) || empty($$class_name))
            {
                self::load($module);
            }
            if(class_exists($module))
            {
                call_user_func_array(array($module,$function),$params);
            }
        }
	}
    
?>