<?php

	class Security
	{
		/*
		*	@function makeSafeNumber Filters a potential unsafe number, strips letters, tags, special characters, quotes and periods
		*	@param int $unsafe Potential unsafe integer
		*	@return int Filtered safe integer
		*/
		function makeSafeNumber($unsafe)
		{
			// Throw all away except numbers
			$safe_ver1	= preg_replace("/[^0-9]/", "", $unsafe);
			// Remove any detected tags
			$safe_ver2	= strip_tags($safe_ver1);
			// Replace any special characters in HTML friendly versions
			$safe_ver3	= htmlspecialchars($safe_ver2);
			// If there still would be any quote signs, escape them with slashes
			$safe_ver4	= addslashes($safe_ver3);
			// Remove any periods (values like 12.000)
			$safe_ver5	= str_replace(".", "", $safe_ver4);
			// Return a safe int
			return $safe_ver5;
		}
		
		function makeSafeArray($arr)
		{
			foreach($arr as $key => $value)
			{
				if(is_array($value))
				{
					$arr[$this->makeSafeString($key)] = $this->makeSafeArray($value);
				}
				else
				{
					$arr[$this->makeSafeString($key)] = $this->makeSafeString($value);
				}
			}
			return $arr;
		}
		
		/*
		*	@function makeSafeString Filters a potential unsafe String, strips tags, special characters and quotes
		*	@param string $unsafe Potential unsafe String
		*	@return string Filtered safe String
		*/
		function makeSafeString($unsafe)
		{
			// Remove any detected tags
			$safe_ver1	= strip_tags($unsafe);
			// Replace any special characters in HTML friendly versions
			$safe_ver2	= htmlspecialchars($safe_ver1);
			// Throw all away except letters, numbers, @, -, _, $, &, comma, space and '.'
			$safe_ver4	=  addslashes($safe_ver2);
			// Return a safe String
			return $safe_ver4;
		}
		
		/*
		*	@function checkEmail Checks if the given e-mail address is a valid one
		*	@param string $email E-mail address to be checked
		*	@return boolean Returns TRUE is the address is valid and returns FALSE if the address is invalid
		*/
		function checkEmail($email)
		{
			$isValid = true;
			$atIndex = strrpos($email, "@");
			if (is_bool($atIndex) && !$atIndex)
			{
				$isValid = false;
			}
			else
			{
				$domain		= substr($email, $atIndex+1);
				$local		= substr($email, 0, $atIndex);
				$localLen 	= strlen($local);
				$domainLen 	= strlen($domain);
				if ($localLen < 1 || $localLen > 64)
				{
					// local part length exceeded
					$isValid = false;
				}
				elseif ($domainLen < 1 || $domainLen > 255)
				{
					// domain part length exceeded
					$isValid = false;
				}
				elseif ($local[0] == '.' || $local[$localLen-1] == '.')
				{
					// local part starts or ends with '.'
					$isValid = false;
				}
				elseif (preg_match('/\\.\\./', $local))
				{
					// local part has two consecutive dots
					$isValid = false;
				}
				elseif (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
				{
					// character not valid in domain part
					$isValid = false;
				}
				elseif (preg_match('/\\.\\./', $domain))
				{
					// domain part has two consecutive dots
					$isValid = false;
				}
				elseif(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local)))
				{
					// character not valid in local part unless 
					// local part is quoted
					if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local)))
					{
						$isValid = false;
					}
				}
				if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
				{
					// domain not found in DNS
					$isValid = false;
				}
			}
			return $isValid;
		}
    
		/*
		*	@function mail2image Creates an .png image of a given e-mail address on the server
		*	@param string $color RGB format of the text color (RRR,GGG,BBB)
		*	@param string $dir Folder destination where to save the image on the server
		*	@param string $mail The e-mail address to be converted
		*	@return string Returns the name of the created image
		*/
		function mail2image($color, $dir, $mail)
		{
			$color = explode(',', $color);
			putenv('GDFONTPATH=' . realpath('.') . "/font/");
			$font = 'arial.ttf';
			$size	= 80;
			$bbox = imagettfbbox($size, 0, $font, $mail);
			$width = $bbox[2];
			$height = $bbox[3] + $size;
			$image = imagecreatetruecolor($width, $height);
			$trns = imagecolortransparent($image, imagecolorallocate($image, 1, 1, 1)); //Gebruiken we hiervoor 0,0,0 dan kunnen we deze kleur niet meer voor de tekst gebruiken
			$color = imagecolorallocate($image, $color[0], $color[1], $color[2]);
			imagefilledrectangle($image, 0, 0, $width, $height, $trns);
			imagettftext($image, $size, 0, 0, $size, $color, $font, $mail);
			$new_img = sha1($mail).'.png';
			imagepng($image, $dir.$new_img);
			imagedestroy($image);
			return $new_img;
		}
		
		/*
		*	@function encryptString Creates an encrypted string based upon a new salt and SHA encryption protocols
		*	@param string $original The String to be encrypted
		*	@return string An encrypted String
		*/
		function encryptString($original)
		{
			$salt		= substr(sha1(substr(md5($original . uniqid(mt_rand(), true)), 0, 10)), 0, 10);
			$hash	= $salt . hash('sha256', strtolower($original));
			for($i = 0; $i < 1000; $i++)
			{
				$hash = substr(hash('sha256', $hash), 0, 54);
			}
			$new_pass	= $hash . $salt;
			return $new_pass;
		}
		
		/*
		*	@function encryptStringWithSalt Creates an encrypted string based upon a given salt and SHA encryption protocols
		*	@param string $original The String to be encrypted
		*	@param string $salt Known salt to be included
		*	@return string An encrypted String
		*/
		function encryptStringWithSalt($original, $salt)
		{
			$hash	= $salt . hash('sha256', strtolower($original));
			for($i = 0; $i < 1000; $i++)
			{
				$hash = substr(hash('sha256', $hash), 0, 54);
			}
			$new_pass	= $hash . $salt;
			return $new_pass;
		}
		
		function getSalt($user_name)
		{
			$user_raw = DB::query("SELECT * FROM cms_admins WHERE admin_name = '$user_name' LIMIT 0,1;");
			if($user_raw == false)
			{
				return false;
			}
			else
			{
				$user = mysqli_fetch_assoc($user_raw);
				return substr($user["admin_pass"], -10);
			}
		}
		
		/*
		*	@function createUniqueSessionKey Creates a unique session key based upon a username and the current time
		*	@param string $username The username to be used in the algorithm
		*	@return string A unique session identification number
		*/
		function createUniqueSessionKey($username)
		{
			$step_2		= md5($username);
			$step_3		= substr($step_2, -5);
			$step_4		= substr($step_3, 0, 2) . substr(time(), -3);
			$step_5		= substr(md5($step_4), 0, 5);
			$step_6		= sha1(date("md") . $step_5);
			$unique_id	= substr($step_6, 10, 10);
			return $unique_id;
		}
		
		/*
		*	@function createSlug Creates a URL-friendly version of a phrase
		*	@param string $phrase The phrase to be slugged
		*	@param int $maxLength defines the maximum length of the new slug
		*	@return string Returns a slug
		*/
		function createSlug($phrase, $maxLength)
		{
			$result = strtolower($phrase);
			$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
			$result = trim(preg_replace("/[\s-]+/", " ", $result));
			$result = trim(substr($result, 0, $maxLength));
			$result = preg_replace("/\s/", "_", $result);
			return $result;
		}
	}

?>