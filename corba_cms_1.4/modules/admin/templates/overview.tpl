{include file="header.tpl"}
{include file="datatable.tpl"}
<h1>All accounts</h1>
{if isset($error)}<p id="error">{$error}</p>{/if}
{if isset($warning)}<p id="warning">{$warning}</p>{/if}
{if isset($message)}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a href="{$base_url}admin/new-user">Create new user</a>
</div>
{if empty($users)}
    <p id="warning">No CMS users registered</p><p id="error">Contact your system administrator</p>
{else}
    <table class="grid display">
        <thead>
        <tr>
            <td class="title" style="width:200px;">Admin name</td>
            <td class="title" style="width:150px;">Account type</td>
            <td class="title" style="width:150px;">Registered</td>
            <td class="title" style="width:200px;">Last login</td>
            <td class="title" style="width:100px;">&nbsp;</td>
        </tr>
        </thead>
        <tbody>
    {foreach from=$users item="u"}
            <tr>
                <td>{$u.real_name}</td>
                <td>{$u.type}</td>
                <td>{$u.date_created}</td>
                <td>{$u.last_login_date} ({$u.last_login_time} hrs)</td>
                <td><a href="{$base_url}edit-user/{$u.admin_id}">Edit</a> | <a href="{$base_url}admin/reset/{$u.admin_id}">Reset password</a>{if $user_type == "superadmin"} | <a href="javascript:confirmDelete('{$u.admin_name_real}','{$base_url}admin/reset{$u.admin_id}')">Delete</a>{/if}</td>
            </tr>
    {/foreach}
        </tbody>
    </table>
{/if}
{include file="footer.tpl"}