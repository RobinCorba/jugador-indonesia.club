{include file="header.tpl"}
<h1>Create new account</h1>
{if $user_type != "superadmin"}
	<p id="error">Whoopsy daisy, you aren't allowed to open this page.</p>
{else}
	{if isset($error)}<p id="error">{$error}</p>{/if}
	{if isset($warning)}<p id="warning">{$warning}</p>{/if}
	{if isset($message)}<p id="message">{$message}</p>{/if}
	<form method="post" action="{$base_url}admin/overview" />
		<table id="details">
			<tr>
				<td>Username (to login)</td>
				<td><input type="text" name="admin_name" maxlength="20" value="" style="width:120px;" placeholder="Username" /></td>
			</tr>
            <tr>
				<td>Password (to login)</td>
				<td><input type="password" name="admin_pass" autocomplete="off" style="width:140px;" /></td>
			</tr>
            <tr>
				<td>Full name</td>
				<td><input type="text" name="admin_name_real" maxlength="32" value="" style="width:200px;" placeholder="Full name" /></td>
			</tr>
			
			
			<tr>
				<td>E-mail address<br /><em>Optional</em></td>
				<td><input type="text" name="admin_email" autocomplete="off" maxlength="128" style="width:200px;" placeholder="E-mail address" /></td>
			</tr>
			<tr>
				<td>Account type</td>
				<td>
					<select name="admin_type">
						<option value="admin">Standard admin</option>
						<option value="superadmin">Super administrator</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:10px;"><input type="submit" name="save_type" value="Add user" style="float:right;"/></td>
			</tr>
		</table>
	</form>
{/if}
{include file="footer.tpl"}