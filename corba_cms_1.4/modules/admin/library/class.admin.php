<?php

	class Admin
	{
		private $table_prefix = "cms_";
        
        function __construct()
        {
            // Make sure the necessary tables are available
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."admins (admin_id tinyint(2) UNSIGNED NOT NULL,admin_name varchar(20) NOT NULL DEFAULT '',admin_name_real varchar(32) NOT NULL,admin_pass varchar(96) NOT NULL,admin_email varchar(30) NOT NULL DEFAULT '',admin_last_login varchar(14) NOT NULL,admin_last_login_ip varchar(15) NOT NULL DEFAULT '',admin_type enum('admin','superadmin') NOT NULL DEFAULT 'admin',admin_date_created varchar(14) NOT NULL,admin_alert enum('yes','no') NOT NULL DEFAULT 'yes',admin_status enum('active','removed') NOT NULL DEFAULT 'active') ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Administrators that have access to the CMS';");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."sessions (session_key varchar(10) NOT NULL,admin_id int(4) NOT NULL,start_time int(12) NOT NULL,update_time int(12) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        }
		
		/*
		*	@function createNewUser Saves a new account based upon given information
		*	@param array $data An array containing all information of the new account
		*	@return boolean Returns TRUE if the account is saved and returns FALSE when the saving operation has failed
		*/
		function createNewUser($data)
		{
			$now    = time();
			$result = DB::query("INSERT INTO ".$this->table_prefix."admins
            (
                admin_name,
                admin_pass,
                admin_name_real,
                admin_email,
                admin_date_created,
                admin_last_login,
                admin_type
            ) VALUES (
                '$data[admin_name]',
                '$data[admin_pass]',
                '$data[admin_name_real]',
                '$data[admin_email]',
                '$now',
                '$now',
                '$data[admin_type]'
            );");
			return $result;
		}
		
		/*
		*	@function retrieveUserInfo Loads all account information based upon an account identification number
		*	@param int $user_id Account identification number
		*	@return array All account information or returns FALSE if the account doesn't exists
		*/
		function retrieveUserInfo($user_id)
		{
			Database::connect();
			$user_data_raw = mysql_query("SELECT * FROM ".$this->table_prefix."cms_admins WHERE admin_id = '$user_id' LIMIT 0,1;");
			Database::disconnect();
			if($user_data_raw == false)
			{
				return false;
			}
			else
			{
				$user = mysql_fetch_assoc($user_data_raw);
				$user["admin_date_created_real"]	= date("j M. Y", $user["admin_date_created"]);
				$user["admin_last_login_real"]		= date("j M. Y (H:i", $user["admin_last_login"]);
				return $user;
			}
		}
		
		/*
		*	@function retrieveAllUsers Returns all registered accounts
		*	@return array Returns all accounts
		*/
		function retrieveAllUsers()
		{
			$users_raw = DB::query("SELECT admin_id id,admin_name name,admin_name_real real_name,admin_pass password,admin_email email,admin_last_login last_login,admin_last_login_ip last_login_ip,admin_type type, admin_date_created date_created, admin_alert alert, admin_status status FROM ".$this->table_prefix."admins WHERE admin_status != 'removed' ORDER BY admin_name_real ASC;");
			if($users_raw == false)
			{
				return false;
			}
			else
			{
				while($user = mysqli_fetch_assoc($users_raw))
				{
					$user["date_created"]       = date("j F Y", $user["date_created"]);
					$user["last_login_date"]    = date("j F Y", $user["last_login"]);
					$user["last_login_time"]    = date("H:i",   $user["last_login"]);
					$users[] = $user;
				}
				return $users;
			}
		}
		
		/*
		*	@function editUser Changes account information
		*	@param array $user All new and/or old information to be saved
		*	@return boolean Returns TRUE is changes are saved or returns FALSE if the save operation failed
		*/
		function editUser($user)
		{
			if($user["user_pass"] != "")
			{
				$pass = "admin_pass = '$user[user_pass]',";
			}
			$now = time();
			Database::connect();
			$result = mysql_query("UPDATE ".$this->table_prefix."cms_admins SET
			admin_name_real	= '$user[user_name_real]',
			admin_name			= '$user[user_name]',
			$pass
			admin_email			= '$user[user_email]',
			admin_type			= '$user[user_type]'
			WHERE admin_id	= '$user[user_id]';");
			Database::disconnect();
			return $result;
		}
		
		/*
		*	@function deleteUser Sets specific user account to 'removed' status
		*	@param int $user_id Account identification number of the account to be deleted
		*	@return boolean Returns TRUE if the account has been 'removed' or returns FALSE if the remove operation failed
		*/
		function deleteUser($user_id)
		{
			Database::connect();
			$result = mysql_query("UPDATE ".$this->table_prefix."cms_admins SET admin_status = 'removed' WHERE admin_id = '$user_id';");
			Database::disconnect();
			return $result;
		}
		
		/*
		*	@function generatePassword Generates a random password
		*	@return string Returns a random password string
		*/
		function generatePassword()
		{
			$length = 8;
			$password = "";
			$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
			$maxlength = strlen($possible);
			if ($length > $maxlength)
			{
				$length = $maxlength;
			}
			$i = 0; 
			while($i < $length)
			{ 
				$char = substr($possible, mt_rand(0, $maxlength-1), 1);
				if(!strstr($password, $char))
				{ 
					$password .= $char;
					$i++;
				}
			}
			return $password;
		}
		
		/*
		*	@function savePassword Saves a new encrypted password in the database
		*	@param $user_id Account identification number of the account
		*	@param $encrypted_pass The new password to bhe stored (needs to be encrypted first)
		*	@return boolean Returns TRUE if the save operation was a succes or returns FALSE if that's not the case
		*/
		function savePassword($user_id, $encrypted_pass)
		{
			Database::connect();
			$result = mysql_query("UPDATE ".$this->table_prefix."users SET user_pass = '$encrypted_pass' WHERE user_id = '$user_id';");
			Database::disconnect();
			return $result;
		}
	}
?>