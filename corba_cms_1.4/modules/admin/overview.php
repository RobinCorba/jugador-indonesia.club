<?php

	$class_admin = Modules::load("admin");
    if(!empty($_POST["save_type"]))
	{
        if($_POST["save_type"] == "Add user")
        {
            $_POST                  = $class_security->makeSafeArray($_POST);
            $_POST["admin_pass"]    = $class_security->encryptString($_POST["admin_pass"]);
            
            $check_mail = true;
            if($_POST["admin_email"] != "")
            {
                $check_mail = $class_security->checkEmail($_POST["admin_email"]);
            }
            if($check_mail === true)
            {
                if($_POST["admin_name_real"] != "" && $_POST["admin_name"] != "" && $_POST["admin_pass"] != "")
                {
                    if($class_admin->createNewUser($_POST))
                    {
                        $smarty->assign("message","Saved new user '".$_POST["admin_name_real"]."'.");
                    }
                    else
                    {
                        $smarty->assign("data",	$_POST);
                        $smarty->assign("error","Could not save new user, please try again.");
                        require_once("new-user.php");
                    }
                }
                else
                {
                    $smarty->assign("data",	$_POST);
                    $smarty->assign("warning","Please fill in all required fields.");
                    require_once("new-user.php");
                }
            }
            else
            {
                $smarty->assign("data",	$_POST);
                $smarty->assign("warning","The given e-mail address is invalid.");
                require_once("new-user.php");
            }
        }
    }
    
	if(!empty($_GET["del"]))
	{
		if($class_admin->deleteUser($class_security->makeSafeNumber($_GET["del"])))
		{
			$smarty->assign("message","User has been deleted.");
		}
		else
		{
			$smarty->assign("error","Whoopsy Daisy, could not delete this user.");
		}
	}
	if(!empty($_GET["reset"]))
	{
		$new_pass = $class_admin->generatePassword();
		if($class_admin->savePassword($class_security->makeSafeNumber($_GET["reset"]), $class_security->encryptString($new_pass)))
		{
			$smarty->assign("message","Password has been reset! New password is: $new_pass");
		}
		else
		{
			$smarty->assign("error","Could not reset the password.");
		}
	}
	$smarty->assign("users",$class_admin->retrieveAllUsers(0,0));
	$smarty->assign("seo_title", "User Management");
	$smarty->assign("seo_desc", "An overview of all CMS users.");
    $smarty->display(dirname(__FILE__)."/templates/overview.tpl");
?>