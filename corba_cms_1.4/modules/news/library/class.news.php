<?php
	
	class News
	{
		var $id;
		var $start;
		var $offset;
		var $lang;
        var $prefix = "news_";
        
        
        function __construct()
        {
            // Make sure the necessary tables are available
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->prefix."categories (category_id int(3) NOT NULL AUTO_INCREMENT,category_title varchar(48) NOT NULL,PRIMARY KEY (category_id),UNIQUE KEY category_title (category_title)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->prefix."comments (comment_id int(5) NOT NULL AUTO_INCREMENT,comment_author varchar(48) NOT NULL,comment_author_email varchar(128) NOT NULL,comment_author_site varchar(128) NOT NULL,comment_author_ip varchar(15) NOT NULL,comment_content text NOT NULL,comment_date bigint(10) NOT NULL,comment_post_id int(5) NOT NULL,comment_status enum('unchecked','approved','declined') NOT NULL DEFAULT 'unchecked',PRIMARY KEY (comment_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->prefix."posts (post_id int(5) NOT NULL AUTO_INCREMENT,post_title varchar(128) NOT NULL,post_title_slug varchar(128) DEFAULT NULL,post_picture varchar(128) DEFAULT NULL,post_desc varchar(256) NOT NULL,post_tags varchar(128) DEFAULT NULL,post_content text NOT NULL,post_date_published bigint(10) NOT NULL,post_date_edited bigint(10) NOT NULL,post_status enum('draft','published','removed') NOT NULL DEFAULT 'draft',PRIMARY KEY (post_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        }
        
		/*
		*	@function retrieveAllArticles Gets all articles from the database
		*	@param boolean $include_removed If set to TRUE it will include removed articles (default is FALSE)
		*	@param int $limit The limit of how many articles to retrieve (default is 0)
		*	@return array Returns an array with articles or returns FALSE if none are found
		*/
		function retrieveAllArticles($include_removed = false, $limit = 0)
		{
			$include_clause = "";
			if($include_removed === false)
			{
				$include_clause = "WHERE post_status != 'removed'";
			}
            $limit_clause = "";
            if($limit > 0)
            {
               $limit_clause = "LIMIT 0,$limit"; 
            }
			$result = DB::query("SELECT post_id id, post_title title, post_date_published publish_date, post_date_edited edit_date, post_content content, post_picture picture, post_status status
            FROM ".$this->prefix."posts $include_clause ORDER BY post_date_published DESC ".$limit_clause.";");
			
			if($result !== false)
			{
				while($article = mysqli_fetch_assoc($result))
				{
					$article['seo_title']	= $article['title'];
					// Create short title
					$c_array            = explode(" ", $article["title"]);
					$n_array	        = Array();
					$word_limit		    = 8;
					$already_short      = false;
					for($i = 0; $i < $word_limit; $i++)
					{
						if(isset($c_array[$i]))
						{
							$n_array[] = $c_array[$i];
						}
						else
						{
							// Title is shorter than cap-off
							$already_short = true;
						}
					}
					if($already_short === false && count($c_array) > 5)
					{
						$article["short_title"] = implode(" ",$n_array) . " [...]";
					}
					else
					{
						$article["short_title"] = $article["title"];
					}
					$article['publish_time']        = date('H:i',		$article['publish_date']);
                    $article['publish_date_alt']    = date('Y/m/d',	    $article['publish_date']);
					$article['publish_date']        = date('j F Y',	$article['publish_date']);
					if($article['edit_date'] != 0)
					{
						$article['edit_time']   = date('H:i',		$article['edit_date']);
						$article['edit_date']   = date('d M Y',	$article['edit_date']);
					}
					//$article["total_comments"]	= $this->retrieveTotalComments($article["article_id"]);
					$all_articles[]						= $article;
				}
				if(!empty($all_articles))
				{
					return $all_articles;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		
		/*
		*	@function retrieveTotalComments Gets the total amount of comments on an article
		*	@param int $article_id The article identification number
		*	@return array Returns the total number of found comments
		*/
		function retrieveTotalComments($article_id)
		{
			
			$result = DB::query("SELECT COUNT(comment_id) AS total FROM ".$this->prefix."comments WHERE comment_article_id = '$article_id';") or die ("Error retrieving news comments: " . mysql_error());
			
			if($result !== false)
			{
				$data = mysqli_fetch_assoc($result);
				return $data["total"];
			}
			else
			{
				return 0;
			}
		}
		
		/*
		*	@function createArticle Saves a new article to the database
		*	@param array $data All information about the new article
		*	@return boolean Returns TRUE if save operation succeeded or returns FALSE if that failed
		*/
		function createArticle($data)
		{
			$now		= time();	
			$seo_title	= self::createArticleSlug($data["article_title"]);
			$result = DB::query("INSERT INTO ".$this->prefix."posts (post_title, post_title_slug, post_picture, post_desc, post_tags, post_content, post_date_published, post_status)
			VALUES ('$data[article_title]', '$seo_title', '$data[article_img_name]', '$data[article_description]', '$data[article_keywords]', '$data[article_content]', '".time()."', '$data[article_status]');");
			return $result;
		}
		
		/*
		*	@function editArticle Saves changes to an existing article in the database
		*	@param array $data All (new) information about the article
		*	@return boolean Returns TRUE if save operation succeeded or returns FALSE if that failed
		*/
		function editArticle($data)
		{
			// Update the article
			$now = time();
			$seo_title	= $this->createArticleSlug($data["post_title"]);
			
            $picture_clause = "";
            
            if(!empty($data["article_img_name"]))
            {
                $picture_clause = "post_picture = '".$data["article_img_name"]."',";
            }
            
			$result = DB::query("UPDATE ".$this->prefix."posts SET
			post_title          = '$data[post_title]',
			post_title_slug     = '$seo_title',
			post_content        = '$data[post_content]',
			post_desc           = '$data[post_desc]',
			post_tags           = '$data[post_tags]',
            $picture_clause
			post_date_edited    = '$now'
			WHERE post_id       = '$data[post_id]';") or die ("Error: " . mysql_error());
			
			return $result;
		}
		
		/*
		*	@function deleteArticle Sets an existing article status to removed in the database
		*	@param int $article_id The article identification number
		*	@return boolean Returns TRUE if 'delete' operation succeeded or returns FALSE if that failed
		*/
		function deleteArticle($article_id)
		{
			$result = DB::query("UPDATE ".$this->prefix."posts SET post_status = 'removed' WHERE post_id = '$article_id';");
			return $result;
		}
		
		/*
		*	@function retrieveArticle Gets a specific article from the database
		*	@param int $post_id Article identification number
		*	@return array Returns an array of article operation or returns FALSE if no article is found
		*/
		function retrieveArticle($id)
		{
			
			$result = DB::query("SELECT post_id id, post_title title, post_content content, post_date_published publish_date, post_date_edited edit_date, post_status status, post_desc description, post_tags tags, post_picture picture
            FROM ".$this->prefix."posts WHERE post_id = '$id' LIMIT 0,1;");
			
			if($result === false)
			{
				return false;
			}
			else
			{
				$article = mysqli_fetch_assoc($result);
                $article['seo_title']	= $article['title'];
                // Create short title
                $c_array            = explode(" ", $article["title"]);
                $n_array	        = Array();
                $word_limit		    = 8;
                $already_short      = false;
                for($i = 0; $i < $word_limit; $i++)
                {
                    if(isset($c_array[$i]))
                    {
                        $n_array[] = $c_array[$i];
                    }
                    else
                    {
                        // Title is shorter than cap-off
                        $already_short = true;
                    }
                }
                if($already_short === false && count($c_array) > 5)
                {
                    $article["short_title"] = implode(" ",$n_array) . " [...]";
                }
                else
                {
                    $article["short_title"] = $article["title"];
                }
                $article['publish_time']        = date('H:i',		$article['publish_date']);
                $article['publish_date_alt']    = date('Y/m/d',	    $article['publish_date']);
                $article['publish_date']        = date('j F Y',	$article['publish_date']);
                if($article['edit_date'] != 0)
                {
                    $article['edit_time']   = date('H:i',		$article['edit_date']);
                    $article['edit_date']   = date('d M Y',	$article['edit_date']);
                }
				return $article;
			}
		}
		
		/*
		*	@function createArticleSlug Creates an SEO friendly slug 
		*	@param string $r Text to convert
		*	@return string Returns an SEO friendly slug
		*/
		function createArticleSlug($r)
		{
			$r = strtolower($r);
			$r = preg_replace("/[^a-z0-9\s-]/", "", $r);
			$r = trim(preg_replace("/[\s-]+/", " ", $r));
			$r = trim($r);
			$r = preg_replace("/\s/", "-", $r);
			return $r;
		}
		
		/*
		*	@function retrieveAllCategories Gets all article categories from the database
		*	@return array Returns an array with categories or returns FALSE if none are found
		*/
		function retrieveAllCategories()
		{
			
			$result = DB::query("SELECT * FROM ".$this->prefix."categories ORDER BY category_title ASC;");
			
			if($result === false)
			{
				return false;
			}
			else
			{
				$cats = null;
				while($cat = mysqli_fetch_assoc($result))
				{
					$cats[]			= $cat;
				}
				if($cats === null)
				{
					return false;
				}
				else
				{
					return $cats;
				}
			}
		}
	}
	
?>