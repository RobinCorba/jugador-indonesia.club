<?php

    $class_news = Modules::load("news");
	
	// Show the template
	$smarty->assign("categories", $class_news->retrieveAllCategories());
	$smarty->assign("seo_title", "New article");
	$smarty->assign("seo_desc", "Write a new article");
	$smarty->display(dirname(__FILE__)."/templates/new.tpl");
	
?>