{include file="header.tpl"}
{include file="datatable.tpl"}

<h1>News Management</h1>
<p>An overview of all articles.</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a id="new" href="{$base_url}news/new">New article</a>
</div>
{if $articles == ""}
	<p>No articles written yet.</p>
{else}
	<table class="grid display">
		<thead>
		<tr>
			<td>Publish date</td>
            <td>Article title</td>
			<td>Status</td>
			<td>&nbsp;</td>
		</tr>
		</thead>
		<tbody>
	{foreach from=$articles item="article"}
		<tr {cycle values=',class="odd"'}>
			<td style="width:220px;">{$article.publish_date_alt} ({$article.publish_time} hrs)</td>
            <td style="width:500px;">{$article.short_title}</td>
			<td style="width:60px;">{$article.status}</td>
			<td style="width:80px;"><a href="{$base_url}news/edit/{$article.id}">Edit</a> | <a href="javascript:confirmDelete('article \'{$article.title}\'','{$base_url}news/overview/delete/{$article.id}')">Delete</a></td>
		</tr>
	{/foreach}
		</tbody>
	</table>
{/if}
{include file="footer.tpl"}