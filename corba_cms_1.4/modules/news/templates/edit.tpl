{include "header.tpl"}
<script type="text/javascript" src="{$base_url}js/tinymce/tiny_mce.js"></script>
{literal}
<script type="text/javascript">
// TinyMCE
tinyMCE.init
({
    mode:                           "textareas",
    theme:                          "advanced",
    plugins:                        "paste",
    theme_advanced_buttons1:        "bold, italic, underline, strikethrough, sub, sup, justifyleft, justifycenter, justifyright, justifyfull",
    theme_advanced_buttons2:        "numlist, bullist, outdent, indent, link, fontsizeselect",
    theme_advanced_buttons3:        "cut, copy, paste, code",
    /*content_css:                  "templates/style_tinymce.css",*/
    relative_urls:                  false,
    remove_script_host:             false,
    paste_auto_cleanup_on_paste:    true,
    paste_preprocess:
    function(pl,o)
    {
        // Content string containing the HTML from the clipboard
        o.content = o.content;
    },
    paste_postprocess:
    function(pl,o)
    {
        // Content DOM node containing the DOM structure of the clipboard
        o.node.innerHTML = o.node.innerHTML;
    }
});
</script>
{/literal}
<h1>Edit article</h1>
<p>Make changes to an article of the news section</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<form enctype="multipart/form-data" method="post" action="{$base_url}news/overview">
<input type="hidden" name="article_id" value="{$data.id}" />
<table id="details" style="width:800px;">
	<tr>
		<td>Article title</td>
		<td>
			<input type="text" name="article_title" maxlength="128" style="width:350px;" {if !empty($data.title)}value="{$data.title}"{/if} /><br />
			<em>Appears on top of the page, browser tab and search engines.</em>
		</td>
	</tr>
	<tr>
		<td>Picture</td>
		<td>
            <img id="img_upload_preview_post" src="{$site_url}media/images/news/thumb_600_{$data.picture}" alt="" /></div>
			<input id="img_upload_post" name="article_picture" type="file" accept="image/*" /><br />
			<em>Minimum size 1200 x 800 pixels (max. 1 MB)</em>
		</td>
	</tr>
	<tr>
		<td colspan="2">Article content</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="article_content" style="width:800px; height:600px;">{if !empty($data.content)}{$data.content}{/if}</textarea>
		</td>
	</tr>
	<tr>
		<td>Summary (or description)</td>
		<td>
			<input type="text" name="article_description" style="width:500px;" value="{if !empty($data.description)}{$data.description}{/if}" /><br />
			<em>A paragraph; This will help search engines (Google, Bing etc.) to find this article.</em>
		</td>
	</tr>
	<tr>
		<td>Tags</td>
		<td>
			<input type="text" name="article_keywords" maxlength="256" style="width:400px;" value="{if !empty($data.tags)}{$data.tags}{/if}" /><br />
			<em>Separate tags with a comma ','. This will improve search results and connecting to related articles</em>
		</td>
	</tr> 
	<tr>
		<td colspan="2"><input type="submit" name="save_type" value="Save changes" style="float:right;" /></td>
	</tr>
</table>
</form>
{include "footer.tpl"}