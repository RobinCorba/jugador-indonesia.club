<?php
    
    $class_news = Modules::load("news");
    
    include("library/class.image.php");
    $class_image = new Image();
    
	
	if(!empty($_POST["save_type"]))
	{
        // Update of an existing post
        if(isset($_POST["article_id"]))
        {
            $data["post_id"]        = $class_security->makeSafeNumber($_POST["article_id"]);
            $data["post_title"]     = $class_security->makeSafeString($_POST["article_title"]);
            $data["post_content"]   = addslashes($_POST["article_content"]);
            $data["post_desc"]      = $class_security->makeSafeString($_POST["article_description"]);
            $data["post_tags"]      = trim(strtolower($class_security->makeSafeString($_POST["article_keywords"])));
            if(substr($data["post_tags"], -1) == ",")
            {
                $data["post_tags"] = substr($data["post_tags"], 0, -1);
            }
            // Check if all mandatory fields are filled
            $warning = "";
            if($data["post_title"] == "")
            {
                $warning .= "This post needs a title.<br />";
            }
            elseif($data["post_content"] == "")
            {
                $warning .= "This post needs some content.<br />";
            }
            elseif($data["post_desc"] == "")
            {
                $warning .= "This article needs a description.<br />";
            }
            elseif($data["post_tags"] == "")
            {
                $warning .= "This article needs some keywords.<br />";
            }
            if($warning !== "")
            {
                $smarty->assign("warning", $warning);
                require_once("edit.php");
            }
            else
            {
                // Process new picture
                if(!empty($_FILES["article_picture"]["name"]))
                {
                    $thumb_sizes	= array(array(1200, 800),array(600,400));
                    $result			= $class_image->uploadImageWithThumbnails($_FILES["article_picture"], $data["post_title"], DIR_IMG_NEWS, $thumb_sizes);
                    if($result[0] === false)
                    {
                        $smarty->assign("error", "Could not upload the image: " . $result[1].". Using default picture.");
                    }
                    else
                    {
                        $data["article_img_name"] = $result[1];
                        // Retrieve old article data for picture removal later
                        $old_article = $class_news->retrieveArticle($data["post_id"]);
                    }
                }
                
                if($class_news->editArticle($data))
                {
                    // Process the changes
                    $smarty->assign("message", "Saved changes to '$data[post_title]'.");
                    // Delete the old pictures from the server
                    if(!empty($old_article))
                    {
                        if(file_exists(DIR_IMG_NEWS.$old_article["picture"]))
                        {
                            unlink(DIR_IMG_NEWS.$old_article["picture"]);
                        }
                        if(file_exists(DIR_IMG_NEWS."thumb_1200_".$old_article["picture"]))
                        {
                            unlink(DIR_IMG_NEWS."thumb_1200_".$old_article["picture"]);
                        }
                        if(file_exists(DIR_IMG_NEWS."thumb_600_".$old_article["picture"]))
                        {
                            unlink(DIR_IMG_NEWS."thumb_600_".$old_article["picture"]);
                        }
                    }
                }
                else
                {
                    $smarty->assign("error","An error occured, no changes saved to '$data[post_title]'.");
                    require_once("edit.php");
                }
            }
        }
        else
        {
            // This is a new post
            $data["article_title"]			= $class_security->makeSafeString($_POST["article_title"]);
            $data["article_content"]		= addslashes($_POST["article_content"]);
            $data["article_description"]	= $class_security->makeSafeString($_POST["article_description"]);
            $data["article_keywords"]		= trim(strtolower($class_security->makeSafeString($_POST["article_keywords"])));
            
            // Remove the last comma from the list of keywords
            if(substr($data["article_keywords"], -1) == ",")
            {
                $data["article_keywords"] = substr($data["article_keywords"], 0, -1);
            }
            
            // Check whether this article needs to be saved as a draft or published
            if($_POST["save_type"] == "Save draft")
            {
                $data["article_status"]	= "draft";
            }
            elseif($_POST["save_type"] == "Publish article")
            {
                $data["article_status"]	= "published";
            }
            
            // Check if all mandatory fields are filled
            if($data["article_title"] == "")
            {
                $smarty->assign("warning", "This article needs a title.");
                $smarty->assign("data", $data);
            }
            elseif($data["article_content"] == "")
            {
                $smarty->assign("warning", "This article needs some content.");
                $smarty->assign("data", $data);
            }
            elseif($data["article_description"] == "")
            {
                $smarty->assign("warning", "This article needs a description.");
                $smarty->assign("data", $data);
            }
            elseif($data["article_keywords"] == "")
            {
                $smarty->assign("warning", "This article needs some keywords.");
                $smarty->assign("data", $data);
            }
            else
            {
                $data["article_img_name"] = "default_picture.jpg";
                // All required fields filled in, process the picture
                if(!empty($_FILES["article_picture"]["name"]))
                {
                    $thumb_sizes	= array(array(1200, 800),array(600,400));
                    $result			= $class_image->uploadImageWithThumbnails($_FILES["article_picture"], $data["article_title"], DIR_IMG_NEWS, $thumb_sizes);
                    if($result[0] === false)
                    {
                        $smarty->assign("error", "Could not upload the image: " . $result[1].". Using default picture.");
                    }
                    else
                    {
                        $data["article_img_name"] = $result[1];
                    }
                }
                else
                {
                    $smarty->assign("warning", "No picture was uploaded, it's strongly advisable to upload a picture!");
                }
                // Process the article
                if($class_news->createArticle($data))
                {
                    if($data["article_status"] == "draft")
                    {
                        $smarty->assign("message","New article draft saved.");
                    }
                    elseif($data["article_status"] == "published")
                    {
                        $smarty->assign("message", "New article published!");
                    }
                }
                else
                {
                    $smarty->assign("error","Could not save new article: ".DB::getError());
                }
            }
		}
	}
    
    if(isset($_GET["action"]) && $_GET["action"] == "delete")
	{
        $article_id = $class_security->makeSafeNumber($_GET["id"]);
        $article    = $class_news->retrieveArticle($article_id);
        if($article["id"] != "")
        {
            if($class_news->deleteArticle($article_id))
            {
                $smarty->assign("message","Article '".$article["title"]."' has been deleted.");
            }
            else
            {
                $smarty->assign("error","Could not delete article '".$article["title"]."'.");
            }
        }
        else
        {
            $smarty->assign("error","Could not find article.");
        }
	}
	$smarty->assign("articles", $class_news->retrieveAllArticles());
	$smarty->assign("seo_title", "News overview");
	$smarty->assign("seo_desc", "An overview of all news articles.");
    $smarty->display(dirname(__FILE__)."/templates/overview.tpl");
    
?>