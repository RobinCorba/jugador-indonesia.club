<?php
	
	class Football
	{
		private $table_prefix = "football_";
        
        function __construct()
        {
            // Make sure the necessary tables are available
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."games (game_id int(6) NOT NULL, game_date_day int(2) UNSIGNED ZEROFILL NOT NULL, game_date_month int(2) UNSIGNED ZEROFILL NOT NULL, game_date_year int(4) NOT NULL, game_opponent int(3) NOT NULL, game_score varchar(128) NOT NULL, game_result enum('won','drew','lost') NOT NULL DEFAULT 'won', 'game_played_home' enum('yes','no') NOT NULL DEFAULT 'yes') ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."goals (goal_id int(6) NOT NULL, goal_player_id int(6) NOT NULL, goal_game_id int(6) NOT NULL, goal_time int(4) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."clean_sheets (clean_sheet_id int(4) NOT NULL, clean_sheet_player_id int(3) NOT NULL, clean_sheet_game_id int(5) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."opponents ('opponent_id' int(4) NOT NULL,  'opponent_name' varchar(128) NOT NULL,  'opponent_color' varchar(128) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."players ('player_id' int(4) NOT NULL,  'player_name' varchar(128) NOT NULL,  'player_display_name' varchar(128) NOT NULL,  'player_no' int(4) NOT NULL,  'player_positions' varchar(64) NOT NULL,  'player_status' enum('new','active','inactive','former','top','legendary') NOT NULL DEFAULT 'new',  'player_since_month' int(2) UNSIGNED ZEROFILL DEFAULT NULL,  'player_since_year' int(4) DEFAULT NULL,  'player_stopped_month' int(2) UNSIGNED ZEROFILL DEFAULT NULL,  'player_stopped_year' int(4) UNSIGNED ZEROFILL DEFAULT NULL,  'player_picture' varchar(128) NOT NULL DEFAULT 'default_picture.jpg',  'player_card_picture' varchar(128) NOT NULL DEFAULT 'default_card_picture.png',  'player_card_position' varchar(5) NOT NULL DEFAULT '') ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            DB::query("CREATE TABLE IF NOT EXISTS ".$this->table_prefix."positions ('position_id' int(4) NOT NULL,  'position_code' varchar(5) NOT NULL,  'position_name' varchar(128) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        }
	
        function addGoal($player_id, $game_id)
        {
            $result = DB::query("INSERT INTO ".$this->table_prefix."goals (goal_player_id, goal_game_id) VALUES ('".$player_id."', '".$game_id."');");
            $return_values[0] = $result;
            $return_values[1] = DB::getError();
            return $return_values;
        }
        
        function retrieveGoals($game_id)
        {
            $result = DB::query("SELECT * FROM ".$this->table_prefix."goals WHERE goal_game_id = '".$game_id."';");
            $return_values[0] = $result;
            $return_values[1] = DB::getError();
            if($result === false)
            {
                return $return_values;
            }
            else
            {
                $goals = array();
                while($goal = mysqli_fetch_assoc($result))
                {
                    if(empty($goals[$goal["goal_player_id"]]))
                    {
                        $goals[$goal["goal_player_id"]] = 1;
                    }
                    else
                    {
                        $goals[$goal["goal_player_id"]] += 1;
                    }
                }
                return $goals;
            }
        }
        
        function addCleanSheet($player_id, $game_id)
        {
            $result = DB::query("INSERT INTO ".$this->table_prefix."clean_sheets (clean_sheet_player_id, clean_sheet_game_id) VALUES ('".$player_id."', '".$game_id."');");
            $return_values[0] = $result;
            $return_values[1] = DB::getError();
            return $return_values;
        }
        
        function retrieveCleanSheets($game_id)
        {
            $result = DB::query("SELECT * FROM ".$this->table_prefix."clean_sheets WHERE clean_sheet_game_id = '".$game_id."' ORDER BY clean_sheet_player_id;");
            $return_values[0] = $result;
            $return_values[1] = DB::getError();
            if($result === false)
            {
                return $return_values;
            }
            else
            {
                $clean_sheets = array();
                while($clean_sheet = mysqli_fetch_assoc($result))
                {
                    if(empty($clean_sheets[$clean_sheet["clean_sheet_player_id"]]))
                    {
                        $clean_sheets[$clean_sheet["clean_sheet_player_id"]] = 1;
                    }
                    else
                    {
                        $clean_sheets[$clean_sheet["clean_sheet_player_id"]] += 1;
                    }
                }
                return $clean_sheets;
            }
        }
        
        function retrieveGame($game_id)
        {
            $result = DB::query("SELECT * FROM ".$this->table_prefix."games WHERE game_id = '".$game_id."';");
            $return_values[0] = $result;
            $return_values[1] = DB::getError();
            if($result !== false)
            {
                $game                   = mysqli_fetch_assoc($result);
                $game["goals"]          = $this->retrieveGoals($game["game_id"]);
                $game["clean_sheets"]   = $this->retrieveCleanSheets($game["game_id"]);
                return $game;
            }
            else
            {
                return $return_values;
            }
        }
        
        function editGame($data)
        {
            $result = DB::query("UPDATE ".$this->table_prefix."games SET
            game_date_day		= '".$data["game_date_day"]."',
            game_date_month	= '".$data["game_date_month"]."',
            game_date_year	= '".$data["game_date_year"]."',
            game_opponent		= '".$data["game_opponent"]."',
            game_score			= '".$data["game_score"]."',
            game_result			= '".$data["game_result"]."'
            WHERE game_id	= '".$data["game_id"]."';");
            $return_values[0] = $result;
            $return_values[1] = DB::getError();
            if($result !== false)
            {
                // Update goals too
                DB::query("DELETE FROM ".$this->table_prefix."goals WHERE goal_game_id = '".$data["game_id"]."';");
                foreach($data["goals"] as $player_id => $total_goals)
                {
                    for($i = 0; $i < $total_goals; $i++)
                    {
                        $this->addGoal($player_id, $data["game_id"]);
                    }
                }
                // Update clean sheets too
                DB::query("DELETE FROM ".$this->table_prefix."clean_sheets WHERE clean_sheet_game_id = '".$data["game_id"]."';");
                foreach($data["clean_sheets"] as $player_id => $got_clean_sheet)
                {
                    if($got_clean_sheet == "yes")
                    {
                        $this->addCleanSheet($player_id, $data["game_id"]);
                    }
                }
                return $return_values;
            }
        }
        
        function addGame($data)
        {
            $result = DB::query("INSERT INTO ".$this->table_prefix."games (game_date_day, game_date_month, game_date_year, game_opponent, game_score, game_result) VALUES
            ('".$data["game_date_day"]."', '".$data["game_date_month"]."', '".$data["game_date_year"]."', '".$data["game_opponent"]."', '".$data["game_score"]."', '".$data["game_result"]."');");
            $return_values[0]   = $result;
            $return_values[1]   = DB::getError();
            $new_game_id        = DB::getInsertID();
            if($result === true)
            {
                // Insert goals
                foreach($data["goals"] as $player_id => $total_goals)
                {
                    for($i = 0; $i < $total_goals; $i++)
                    {
                        $this->addGoal($player_id, $new_game_id);
                    }
                }
                // Insert clean sheets
                foreach($data["clean_sheets"] as $player_id => $got_clean_sheet)
                {
                    if($got_clean_sheet == "yes")
                    {
                        $this->addCleanSheet($player_id, $new_game_id);
                    }
                }
            }
            return $return_values;
        }
        
        function retrieveGames()
        {
            $opponents = $this->retrieveOpponents();
            if($opponents[0] === false)
            {
                $return_values[0]   = false;
                $return_values[1]   = $opponents[1];
            }
            
            $d = DB::query("SELECT * FROM ".$this->table_prefix."games ORDER BY game_date_year DESC, game_date_month DESC, game_date_day DESC;");
            if($d === false)
            {
                $return_values[0]   = false;
                $return_values[1]   = DB::getError();
            }
            else
            {
                $games = array();
                while($game = mysqli_fetch_assoc($d))
                {
                    $game["game_opponent"]  = $opponents[1][$game["game_opponent"]];
                    $games[]                = $game;
                }
                $return_values[0]   = true;
                $return_values[1]   = $games;
            }
            return $return_values;
        }
        
        function retrieveOpponents()
        {
            $d = DB::query("SELECT opponent_id id, opponent_name name, opponent_color color FROM ".$this->table_prefix."opponents ORDER BY name ASC;");
            if($d === false)
            {
                $return_values[0] = false;
                $return_values[1] = DB::getError();
            }
            else
            {
                $opponents = array();
                while($opponent = mysqli_fetch_assoc($d))
                {
                    $opponents[$opponent["id"]] = $opponent;
                }
                $return_values[0]   = true;
                $return_values[1]   = $opponents;
            }
            return $return_values;
            
        }
        
        function addOpponent($data)
        {
            $d = DB::query("INSERT INTO ".$this->table_prefix."opponents (opponent_name, opponent_color) VALUES ('".$data["opponent_name"]."', '".$data["opponent_color"]."');");
            $return_values[0] = $d;
            $return_values[1] = DB::getError();
            return $return_values;
        }
        
        function editOpponent($data)
        {
            $d = DB::query("UPDATE ".$this->table_prefix."opponents SET
            opponent_name = '".$data["opponent_name"]."',
            opponent_color = '".$data["opponent_color"]."'
            WHERE opponent_id = '".$data["opponent_id"]."';");
            $return_values[0] = $d;
            $return_values[1] = DB::getError();
            return $return_values;
        }
        
        function retrieveOpponent($id)
        {
            $return_values      = array(2);
            $d                  = DB::query("SELECT opponent_id id, opponent_name name, opponent_color color FROM ".$this->table_prefix."opponents WHERE opponent_id = '".$id."';");
            $return_values[0]   = $d;
            if($d === false)
            {
                $return_values[1] = DB::getError();
            }
            else
            {
                $return_values[1] = mysqli_fetch_assoc($d);
            }
            return $return_values;
        }
        
        function retrievePlayer($id)
        {
            $positions = $this->retrievePositions();
            $d = DB::query("SELECT player_id id, player_name name, player_display_name display_name, player_no no, player_positions positions, player_status status, player_since_month since_month, player_since_year since_year, player_stopped_month stopped_month, player_stopped_year stopped_year, player_picture picture, player_card_picture card_picture, player_card_position card_position FROM ".$this->table_prefix."players WHERE player_id = '".$id."';");
            if($d === false)
            {
                $return_values[0] = false;
                $return_values[1] = DB::getError();
                return $return_values;
            }
            $player = mysqli_fetch_assoc($d);
            $player["positions"] = json_decode($player["positions"]);
            foreach($player["positions"] as $key => $value)
            {
                $player["positions"][$key] = $positions[$value];
            }
            
            $player["since_month_readable"] = date("M").".";
            switch($player["since_month"])
            {
                case "01":  $player["since_month_readable"] = "Jan.";   break;
                case "02":  $player["since_month_readable"] = "Feb.";   break;
                case "03":  $player["since_month_readable"] = "Mar.";   break;
                case "04":  $player["since_month_readable"] = "Apr.";   break;
                case "05":  $player["since_month_readable"] = "May";   break;
                case "06":  $player["since_month_readable"] = "Jun.";   break;
                case "07":  $player["since_month_readable"] = "Jul.";   break;
                case "08":  $player["since_month_readable"] = "Aug.";   break;
                case "09":  $player["since_month_readable"] = "Sep.";   break;
                case "10":  $player["since_month_readable"] = "Oct.";   break;
                case "11":  $player["since_month_readable"] = "Nov.";   break;
                case "12":  $player["since_month_readable"] = "Dec.";   break;
            }
            
            $player["status_readable"] = "Active player";
            switch($player["status"])
            {
                case "former":      $player["status_readable"] = "Former player";        break;
                case "inactive":    $player["status_readable"] = "Inactive player";      break;
                case "new":         $player["status_readable"] = "New player";           break;
                case "active":      $player["status_readable"] = "Active player";        break;
                case "top":         $player["status_readable"] = "Top player";           break;
                case "legendary":   $player["status_readable"] = "Legendary player";     break;
            }
            return $player;
        }
        
        function retrievePlayers($include_former_players = true)
        {
            $positions      = $this->retrievePositions();
            $exclude_clause = "";
            $return_values  = array();
            if($include_former_players !== true)
            {
                $exclude_clause = "WHERE player_status != 'former'";
            }
            
            $d = DB::query("SELECT player_id id, player_name name, player_display_name display_name, player_no no, player_positions positions, player_status status, player_since_month since_month, player_since_year since_year, player_stopped_month stopped_month, player_stopped_year stopped_year, player_picture picture, player_card_picture card_picture, player_card_position card_position FROM ".$this->table_prefix."players $exclude_clause ORDER BY name, no ASC;");
            if($d === false)
            {
                $return_values[0] = false;
                $return_values[1] = DB::getError();
            }
            else
            {
                $players = array();
                while($player = mysqli_fetch_assoc($d))
                {
                    $player["positions"] = json_decode($player["positions"]);
                    if(is_array($player["positions"]))
                    {
                        foreach($player["positions"] as $key => $value)
                        {
                            $player["positions"][$key] = $positions[$value];
                        }
                    }
                    $player["status_readable"] = "Active player";
                    switch($player["status"])
                    {
                        case "former":      $player["status_readable"] = "Former player";        break;
                        case "inactive":    $player["status_readable"] = "Inactive player";      break;
                        case "new":         $player["status_readable"] = "New player";           break;
                        case "active":      $player["status_readable"] = "Active player";        break;
                        case "top":         $player["status_readable"] = "Top player";           break;
                        case "legendary":   $player["status_readable"] = "Legendary player";     break;
                    }
                    $players[$player["id"]] = $player;
                }
                $return_values[0]   = true;
                $return_values[1]   = $players;
            }
            return $return_values;
        }
        
        function addPlayer($p)
        {
            $result = DB::query("INSERT INTO ".$this->table_prefix."players (player_name,player_display_name,player_no,player_positions,player_status,player_since_month,player_since_year, player_stopped_month,player_stopped_year, player_card_position)
            VALUES ('".$p["player_name"]."', '".$p["player_display_name"]."','".$p["player_no"]."', '".json_encode($p["player_positions"])."', '".$p["player_status"]."', '".$p["player_since_month"]."', '".$p["player_since_year"]."', '".$p["player_stopped_month"]."', '".$p["player_stopped_year"]."', '".$p["player_card_position"]."');");
            $return_values[0]   = $result;
            $return_values[1]   = DB::getError();
            $new_player_id      = DB::getInsertID();
            if(isset($p["player_picture"]))
            {
                DB::query("UPDATE ".$this->table_prefix."players SET player_picture = '".$p["player_picture"]."' WHERE player_id = '".$new_player_id."';");
            }
            if(isset($p["player_card_picture"]))
            {
                DB::query("UPDATE ".$this->table_prefix."players SET player_card_picture = '".$p["player_card_picture"]."' WHERE player_id = '".$new_player_id."';");
            }
            return $return_values;
        }
        
        function editPlayer($p, $root)
        {
            $picture_clause = "";
            $picture_card_clause = "";
            $old_data = $this->retrievePlayer($p["player_id"]);
            
            // Update the player picture
            if(isset($p["player_picture"]))
            {
                if(!empty($old_data["picture"]) && $old_data["picture"] != "default_picture.jpg")
                {
                    // Remove old picture
                    $sizes = array(180,120);
                    foreach($sizes as $size)
                    {
                        unlink($root."media/images/players/thumb_".$size."_".$old_data["picture"]);
                    }
                    unlink($root."media/images/players/".$old_data["picture"]);
                }
                $picture_clause = "player_picture = '".$p["player_picture"]."',";
            }
            
            // Update the player card picture
            if(isset($p["player_card_picture"]))
            {
                if(!empty($old_data["card_picture"]) && $old_data["card_picture"] != "default_card_picture.png")
                {
                    // Remove old picture
                    $sizes = array(120);
                    foreach($sizes as $size)
                    {
                        unlink($root."media/images/player_cards/thumb_".$size."_".$old_data["card_picture"]);
                    }
                    unlink($root."media/images/player_cards/".$old_data["card_picture"]);
                }
                $picture_card_clause = "player_card_picture = '".$p["player_card_picture"]."',";
            }

            $result = DB::query("UPDATE ".$this->table_prefix."players SET
            player_name             = '".$p["player_name"]."',
            player_display_name     = '".$p["player_display_name"]."',
            player_no               = '".$p["player_no"]."',
            player_positions        = '".json_encode($p["player_positions"])."',
            player_card_position    = '".$p["player_card_position"]."',
            player_status           = '".$p["player_status"]."',
            player_since_month      = '".$p["player_since_month"]."',
            player_stopped_month    = '".$p["player_stopped_month"]."',
            player_stopped_year     = '".$p["player_stopped_year"]."',
            ".$picture_clause.$picture_card_clause."
            player_since_year = '".$p["player_since_year"]."' WHERE player_id = '".$p["player_id"]."';");
            $return_values[0] = $result;
            $return_values[1] = DB::getError();
            return $return_values;
        }
        
        function retrievePositions()
        {
            $p = DB::query("SELECT * FROM ".$this->table_prefix."positions;");
            
            if($p === false)
            {
                $return_values[0] = false;
                $return_values[1] = DB::getError();
                return $return_values;
            }
            $positions = array();
            while($position = mysqli_fetch_assoc($p))
            {
                $positions[$position["position_id"]] = $position;
            }
            return $positions;
        }
        
        function retrieveGameStatistics()
        {
            $result_games           = DB::query("SELECT * FROM ".$this->table_prefix."games;");
            $result_opp             = DB::query("SELECT * FROM ".$this->table_prefix."opponents;");
            $result_goals           = DB::query("SELECT * FROM ".$this->table_prefix."goals;");
            $result_clean_sheets    = DB::query("SELECT * FROM ".$this->table_prefix."clean_sheets;");
            $result_players         = DB::query("SELECT * FROM ".$this->table_prefix."players ORDER BY player_display_name ASC;");
            $result_pos             = DB::query("SELECT * FROM ".$this->table_prefix."positions;");
            $games = array();
            while($game = mysqli_fetch_assoc($result_games))
            {
                $games[$game["game_id"]] = $game;
            }
            $opponents = array();
            while($o = mysqli_fetch_assoc($result_opp))
            {
                $opponents[$o["opponent_id"]] = $o["opponent_name"];
            }
            $players = array();
            $stats["players"] = array();
            while($p = mysqli_fetch_assoc($result_players))
            {
                $p["total_goals"]           = 0;
                $p["total_clean_sheets"]    = 0;
                $p["last_goal_date"]        = 00000000;
                $p["last_clean_sheet_date"] = 00000000;
                $month                      = "";
                switch($p["player_since_month"])
                {
                    case 1:  $month = "Jan.";    break;
                    case 2:  $month = "Feb.";    break;
                    case 3:  $month = "Mar.";    break;
                    case 4:  $month = "Apr.";    break;
                    case 5:  $month = "May";     break;
                    case 6:  $month = "June";    break;
                    case 7:  $month = "July";    break;
                    case 8:  $month = "Aug.";    break;
                    case 9:  $month = "Sept.";   break;
                    case 10: $month = "Oct.";    break;
                    case 11: $month = "Nov.";    break;
                    case 12: $month = "Dec.";    break;
                }
                $p["member_since"]        = $month." ".$p["player_since_year"];
                
                if($p["player_stopped_year"] > 0)
                {
                    switch($p["player_stopped_month"])
                    {
                        case 1:  $month = "Jan.";    break;
                        case 2:  $month = "Feb.";    break;
                        case 3:  $month = "Mar.";    break;
                        case 4:  $month = "Apr.";    break;
                        case 5:  $month = "May";     break;
                        case 6:  $month = "June";    break;
                        case 7:  $month = "July";    break;
                        case 8:  $month = "Aug.";    break;
                        case 9:  $month = "Sept.";   break;
                        case 10: $month = "Oct.";    break;
                        case 11: $month = "Nov.";    break;
                        case 12: $month = "Dec.";    break;
                    }
                    $p["stopped_on"]        = $month." ".$p["player_stopped_year"];
                }
                
                $p["player_name"]           = $p["player_name"];
                $p["player_display_name"]   = $p["player_display_name"];
                $p["status_readable"]       = "Active player";
                switch($p["player_status"])
                {
                    case "former":      $p["status_readable"] = "Former player";        break;
                    case "inactive":    $p["status_readable"] = "Inactive player";      break;
                    case "new":         $p["status_readable"] = "New player";           break;
                    case "active":      $p["status_readable"] = "Active player";        break;
                    case "top":         $p["status_readable"] = "Top player";           break;
                    case "legendary":   $p["status_readable"] = "Legendary player";     break;
                }
                $stats["players"][$p["player_id"]]  = $p;
            }
            
            $goals = array();
            while($g = mysqli_fetch_assoc($result_goals))
            {
                // Calculate total goals per player
                $stats["players"][$g["goal_player_id"]]["total_goals"] += 1;
                // Date of the last goal
                if(empty($stats["players"][$g["goal_player_id"]]["last_goal_date"]) || $games[$g["goal_game_id"]]["game_date_year"].$games[$g["goal_game_id"]]["game_date_month"].$games[$g["goal_game_id"]]["game_date_day"] > $stats["players"][$g["goal_player_id"]]["last_goal_date"])
                {
                    $stats["players"][$g["goal_player_id"]]["last_goal_date"]           = $games[$g["goal_game_id"]]["game_date_year"].$games[$g["goal_game_id"]]["game_date_month"].$games[$g["goal_game_id"]]["game_date_day"];
                    $stats["players"][$g["goal_player_id"]]["last_goal_date_formatted"] = $games[$g["goal_game_id"]]["game_date_year"]."-".$games[$g["goal_game_id"]]["game_date_month"]."-".$games[$g["goal_game_id"]]["game_date_day"];
                }
                $goals[$g["goal_id"]] = $g;
            }
            
            $clean_sheets = array();
            while($g = mysqli_fetch_assoc($result_clean_sheets))
            {
                // Calculate total clean sheets per player
                $stats["players"][$g["clean_sheet_player_id"]]["total_clean_sheets"] += 1;
                // Date of the last clean_sheet
                if(empty($stats["players"][$g["clean_sheet_player_id"]]["last_clean_sheet_date"]) || $games[$g["clean_sheet_game_id"]]["game_date_year"].$games[$g["clean_sheet_game_id"]]["game_date_month"].$games[$g["clean_sheet_game_id"]]["game_date_day"] > $stats["players"][$g["clean_sheet_player_id"]]["last_goal_date"])
                {
                    $stats["players"][$g["clean_sheet_player_id"]]["last_clean_sheet_date"]           = $games[$g["clean_sheet_game_id"]]["game_date_year"].$games[$g["clean_sheet_game_id"]]["game_date_month"].$games[$g["clean_sheet_game_id"]]["game_date_day"];
                    $stats["players"][$g["clean_sheet_player_id"]]["last_clean_sheet_date_formatted"] = $games[$g["clean_sheet_game_id"]]["game_date_year"]."-".$games[$g["clean_sheet_game_id"]]["game_date_month"]."-".$games[$g["clean_sheet_game_id"]]["game_date_day"];
                }
                $clean_sheets[$g["clean_sheet_id"]] = $g;
            }
            
            // Calculate top scorers
            $top_scorers = array();
            foreach($stats["players"] as $player)
            {
                $top_scorers[$player["player_id"]]["player_id"]             = $player["player_id"];
                $top_scorers[$player["player_id"]]["player_name"]           = $player["player_name"];
                $top_scorers[$player["player_id"]]["player_display_name"]   = $player["player_display_name"];
                $top_scorers[$player["player_id"]]["player_picture"]        = $player["player_picture"];
                $top_scorers[$player["player_id"]]["total_goals"]           = $player["total_goals"];
                $top_scorers[$player["player_id"]]["last_goal_date"]        = $player["last_goal_date"];
                
                // In case we have a shared top scorer, prioritize the person who got his goal earlier
                $ts_sort["total_goals"][$player["player_id"]]       = $player["total_goals"];
                $ts_sort["last_goal_date"][$player["player_id"]]    = $player["last_goal_date"];
            }
            array_multisort($ts_sort['total_goals'], SORT_DESC, $ts_sort['last_goal_date'], SORT_ASC, $top_scorers);
            $stats["top_scorers"] = $top_scorers;
            
            // Calculate top defenders/goalkeepers
            $top_defenders = array();
            foreach($stats["players"] as $player)
            {
                $top_defenders[$player["player_id"]]["player_id"]             = $player["player_id"];
                $top_defenders[$player["player_id"]]["player_name"]           = $player["player_name"];
                $top_defenders[$player["player_id"]]["player_display_name"]   = $player["player_display_name"];
                $top_defenders[$player["player_id"]]["player_picture"]        = $player["player_picture"];
                $top_defenders[$player["player_id"]]["total_clean_sheets"]    = $player["total_clean_sheets"];       
                $top_defenders[$player["player_id"]]["last_clean_sheet_date"] = $player["last_clean_sheet_date"];
                
                // In case we have a shared top defender, prioritize the person who got his clean sheet earlier
                $ts_sort["total_clean_sheets"][$player["player_id"]]       = $player["total_clean_sheets"];
                $ts_sort["last_clean_sheet_date"][$player["player_id"]]    = $player["last_clean_sheet_date"];
            }
            array_multisort($ts_sort['total_clean_sheets'], SORT_DESC, $ts_sort['last_clean_sheet_date'], SORT_DESC, $top_defenders);
            $stats["top_defenders"] = $top_defenders;
            
            $positions = array();
            while($pos = mysqli_fetch_assoc($result_pos))
            {
                $positions[$pos["position_id"]] = $p;
            }
            ;
            $stats["total"]         = 0;
            $stats["won"]           = 0;
            $stats["drew"]          = 0;
            $stats["lost"]          = 0;
            $stats["biggest_win"]   = array();
            $tmp_biggest_win        = 0;
            // Calculate total games played, won, drew, lost and biggest game won
            foreach($games as $game)
            {
                $stats["total"]++;
                switch($game["game_result"])
                {
                    case "won":
                    $stats["won"]++;
                    $score_saldo = 0;
                    $score_saldo = explode("-",$game["game_score"]);
                    rsort($score_saldo);
                    $score_saldo = $score_saldo[0] - $score_saldo[1];
                    if($score_saldo > $tmp_biggest_win)
                    {
                        $tmp_biggest_win = $score_saldo;
                        
                        switch($game["game_date_month"])
                        {
                            case 1:  $month = "January";     break;
                            case 2:  $month = "February";    break;
                            case 3:  $month = "March";       break;
                            case 4:  $month = "April";       break;
                            case 5:  $month = "May";         break;
                            case 6:  $month = "June";        break;
                            case 7:  $month = "July";        break;
                            case 8:  $month = "August";      break;
                            case 9:  $month = "September";   break;
                            case 10: $month = "October";     break;
                            case 11: $month = "November";    break;
                            case 12: $month = "December";    break;
                        }
                        $biggest_win_data             = array();
                        $biggest_win_data["date"]     = $game["game_date_day"]." ".$month." ".$game["game_date_year"];
                        $biggest_win_data["score"]    = $game["game_score"];
                        $biggest_win_data["opponent"] = $opponents[$game["game_opponent"]];
                        $stats["biggest_win"]         = $biggest_win_data;
                    }
                    break;
                    
                    case "drew":
                    $stats["drew"]++;
                    break;
                    
                    case "lost":
                    $stats["lost"]++;
                    break;
                }
            }
            /*echo"<pre>";
            print_r($stats);
            echo"</pre>";*/
            return $stats;
        }
	}
	
?>