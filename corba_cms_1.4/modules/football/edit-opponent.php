<?php
	
    $class_football     = Modules::load("football");
    $opponent_id        = false;
    
    // Check if we are loading opponent data
    if(!empty($_GET["id"]))
    {
        $opponent_id    = (int) $class_security->makeSafeString($_GET["id"]);
        $result         = $class_football->retrieveOpponent($opponent_id);
        if($result[0] !== false)
        {
            if(!empty($result[1]["id"]))
            {
                $smarty->assign("seo_title",    "Edit Opponent ".$result[1]["name"]);
                $smarty->assign("seo_desc",     "Football Management: edit opponent data.");
                $smarty->assign("opponent",     $result[1]);
            }
            else
            {
                // No data found with giving ID
                failedLoadingData($smarty);
            }
        }
        else
        {
            // Error selecting data
            failedLoadingData($smarty);
        }
    }
    else
    {
        // Missing ID
        failedLoadingData($smarty);
    }
    
    function failedLoadingData($smarty, $error)
    {
        $smarty->assign("seo_title",    "Edit Opponent (failed)");
        $smarty->assign("seo_desc",     "Football Management: edit opponent data (failed).");
        $smarty->assign("error",        "Could not retrieve opponent data: ".$error.". Please try again.");
        $smarty->assign("block_save",   true);
    }
    
    $smarty->display(dirname(__FILE__)."/templates/edit-opponent.tpl");
	
?>