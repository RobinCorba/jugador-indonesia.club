<?php
	
    $class_news = Modules::load("news");
    include("library/class.image.php");
    $class_image = new Image();
    define("DIR_NEWS","media/images/news/");
	
    $article = array();
	// Show the template
	if(empty($data))
	{
        $article_id = $class_security->makeSafeNumber($_GET["id"]);
        $article    = $class_news->retrieveArticle($article_id);
	}
    $smarty->assign("data", $article);
	$smarty->assign("seo_title", "Edit article '".$article["title"]."'.");
	$smarty->assign("seo_desc", "Edit article '".$article["title"]."'.");
	$smarty->display(dirname(__FILE__)."/templates/edit.tpl");
	
?>