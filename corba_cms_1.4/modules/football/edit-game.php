<?php
	
    $class_football = Modules::load("football");
    
	if(!empty($_POST))
	{
		$_POST = $class_security->makeSafeArray($_POST);
		$result = $class_football->addGame($_POST);
		if($result[0] === true)
		{
			$smarty->assign("message", "Game results ".$_POST["game_score"]." (".$_POST["game_result"].") has been added.");
			include("games.php");
			exit();
		}
		else
		{
			$smarty->assign("error", "Error occurred: ".$result[1]);
			$smarty->assign("game", $_POST);
		}
	}
    $smarty->assign("this_day",date("d"));
    $smarty->assign("this_month",date("m"));
    $smarty->assign("this_year",date("Y"));
	$smarty->assign("players", $class_football->retrievePlayers(false));
    $opponents  = $class_football->retrieveOpponents();
    if($opponents[0] === false)
    {
        $smarty->assign("error", "Could not retrieve oppponents: ".$opponents[1]);
    }
    else
    {
        $smarty->assign("opponents", $opponents[1]);
    }
    
    $smarty->assign("game", $class_football->retrieveGame($class_security->makeSafeString($_GET["id"])));
	$players    = $class_football->retrievePlayers(false);
    $smarty->assign("players", $players[1]);
    
    $opponents  = $class_football->retrieveOpponents();
	$smarty->assign("opponents", $opponents[1]);
    
	$smarty->assign("seo_title", "Edit Game");
	$smarty->assign("seo_desc", "Edit new match results.");
    $smarty->display(dirname(__FILE__)."/templates/edit-game.tpl");
	
?>