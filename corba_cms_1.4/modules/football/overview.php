<?php
    
    $class_football = Modules::load("football");
    
    include("library/class.image.php");
    $class_image = new Image();
    
	
	if(!empty($_POST["save_type"]))
	{
        if($_POST["save_type"] == "Add game")
        {
            $_POST  = $class_security->makeSafeArray($_POST);
            $result = $class_football->addGame($_POST);
            if($result[0] === true)
            {
                $smarty->assign("message", "Game results ".$_POST["game_score"]." (".$_POST["game_result"].") has been added.");
            }
            else
            {
                $smarty->assign("error", "Could not add game: ".$result[1]);
                $smarty->assign("game", $_POST);
                require_once("new-game.php");
                exit();
            }
        }
        elseif($_POST["save_type"] == "Save game")
        {
            $_POST = $class_security->makeSafeArray($_POST);
            $result = $class_football->editGame($_POST);
            if($result[0] === true)
            {
                $smarty->assign("message", "Saved changes to game ".$_POST["game_score"]." (".$_POST["game_result"].").");
            }
            else
            {
                $smarty->assign("error", "Error occurred: ".$result[1]);
                $smarty->assign("game", $_POST);
                require_once("edit-game.php");
                exit();
            }
        }
        elseif($_POST["save_type"] == "Add opponent")
        {
            $_POST  = $class_security->makeSafeArray($_POST);
            $result = $class_football->addOpponent($_POST);
            if($result[0] === true)
            {
                $smarty->assign("message", "Opponent team ".$_POST["opponent_name"]." has been added.");
            }
            else
            {
                $smarty->assign("error", "Error occurred: ".$result[1]);
                $smarty->assign("opponent", $_POST);
                require_once("new-opponent.php");
                exit();
            }
        }
        elseif($_POST["save_type"] == "Add player")
        {
            $_POST = $class_security->makeSafeArray($_POST);
            if(!empty($_FILES["player_picture"]))
            {
                $thumb_sizes = array(array(180,240),array(120,160));
                $result = $class_image->uploadImageWithThumbnails($_FILES["player_picture"], $_POST["player_name"], DIR_ROOT."media/images/players/", $thumb_sizes, 91);
                if($result[0] === true)
                {
                    $_POST["player_picture"] = $result[1];
                }
            }
            if(!empty($_FILES["player_card_picture"]))
            {
                $thumb_sizes = array(array(120,120));
                $result = $class_image->uploadImageWithThumbnails($_FILES["player_card_picture"], $_POST["player_name"], DIR_ROOT."media/images/player_cards/", $thumb_sizes, 91);
                if($result[0] === true)
                {
                    $_POST["player_card_picture"] = $result[1];
                }
            }
            $result = $class_football->addPlayer($_POST);
            if($result[0] === true)
            {
                
                $smarty->assign("message", "Player ".$_POST["player_name"]." has been added to the roster");
            }
            else
            {
                $smarty->assign("error", "Error occurred: ".$result[1]);
                $smarty->assign("player", $_POST);
                require_once("new-player.php");
                exit();
            }
        }
        elseif($_POST["save_type"] == "Save changes to player")
        {
            $_POST = $class_security->makeSafeArray($_POST);
            // Store pictures
            if(!empty($_FILES["player_picture"]))
            {
                $thumb_sizes = array(array(180,240),array(120,160));
                $result = $class_image->uploadImageWithThumbnails($_FILES["player_picture"], $_POST["player_name"], DIR_ROOT."media/images/players/", $thumb_sizes, 91);
                if($result[0] === true)
                {
                    $_POST["player_picture"] = $result[1];
                }
            }
            if(!empty($_FILES["player_card_picture"]))
            {
                $thumb_sizes = array(array(120,120));
                $result = $class_image->uploadImageWithThumbnails($_FILES["player_card_picture"], $_POST["player_name"], DIR_ROOT."media/images/player_cards/", $thumb_sizes, 91);
                if($result[0] === true)
                {
                    $_POST["player_card_picture"] = $result[1];
                }
            }
            
            // Save changes to the database
            $result = $class_football->editPlayer($_POST, DIR_ROOT);
            if($result[0] === true)
            {
                $smarty->assign("message", "Saved changes to player ".$_POST["player_name"].".");
            }
            else
            {
                $smarty->assign("error", "Error occurred: ".$result[1]);
                $smarty->assign("player", $_POST);
                require_once("edit-player.php");
                exit();
            }
        }
        elseif($_POST["save_type"] == "Save changes to opponent")
        {
            $data   = $class_security->makeSafeArray($_POST);
            $result = $class_football->editOpponent($data);
            if($result[0] === true)
            {
                $smarty->assign("message",      "Saved changes to opponent ".$data["opponent_name"]);
            }
            else
            {
                $smarty->assign("error", "Error occurred: ".$result[1]);
                $smarty->assign("opponent", $data);
                require_once("edit-opponent.php");
                exit();
            }
        }
	}
    
    if(isset($_GET["action"]) && $_GET["action"] == "delete")
	{
        $article_id = $class_security->makeSafeNumber($_GET["id"]);
        $article    = $class_news->retrieveArticle($article_id);
        if($article["id"] != "")
        {
            if($class_news->deleteArticle($article_id))
            {
                $smarty->assign("message","Article '".$article["title"]."' has been deleted.");
            }
            else
            {
                $smarty->assign("error","Could not delete article '".$article["title"]."'.");
            }
        }
        else
        {
            $smarty->assign("error","Could not find article.");
        }
	}
    
    // Load all games
    $games  = $class_football->retrieveGames();
    if($games[0] === false)
    {
        $smarty->assign("error", "Could not retrieve games: ".$games[1]);
    }
    else
    {
        $smarty->assign("games", $games[1]);
    }
    
    // Load all players
    $players  = $class_football->retrievePlayers();
    if($players[0] === false)
    {
        $smarty->assign("error", "Could not retrieve players: ".$players[1]);
    }
    else
    {
        $smarty->assign("players", $players[1]);
    }
    
    // Load all opponents
    $opponents  = $class_football->retrieveOpponents();
    if($opponents[0] === false)
    {
        $smarty->assign("error", "Could not retrieve opponents: ".$opponents[1]);
    }
    else
    {
        $smarty->assign("opponents", $opponents[1]);
    }
    
	$smarty->assign("seo_title", "Football Management");
	$smarty->assign("seo_desc", "An overview of all football games, opponents, etc.");
    $smarty->display(dirname(__FILE__)."/templates/overview.tpl");
    
?>