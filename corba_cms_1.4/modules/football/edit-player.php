<?php
	
    $class_football = Modules::load("football");
    $player = $class_football->retrievePlayer($class_security->makeSafeString($_GET["id"]));
	$smarty->assign("player", $player);
	$smarty->assign("positions", $class_football->retrievePositions());
    $smarty->assign("this_month_readable", date("M").".");
    $smarty->assign("this_month", date("m"));
    $smarty->assign("this_year", date("Y"));
	$smarty->assign("seo_title","Edit player: ".$player["name"]);
	$smarty->assign("seo_desc", "Edit details of player ".$player["name"].".");
    $smarty->display(dirname(__FILE__)."/templates/edit-player.tpl");
	
?>