{include file="header.tpl"}
<h1>Edit Game</h1>
<p>Edit new match results.</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<form method="post" action="{$base_url}football/overview">
	<input type="hidden" name="game_id" maxlength="4" {if isset($game)}value="{$game.game_id}"{/if} />
	<table class="details">
		<tbody>
			<tr>
				<td>Date</td>
				<td>
					<input type="text" name="game_date_day" maxlength="2" placeholder="DD" value="{if isset($game)}{$game.game_date_day}{else}{$this_day}{/if}" style="width:20px; text-align:center;" />
					<input type="text" name="game_date_month" maxlength="2" placeholder="MM" value="{if isset($game)}{$game.game_date_month}{else}{$this_month}{/if}" style="width:20px; text-align:center;" />
					<input type="text" name="game_date_year" maxlength="4" placeholder="YYYY" value="{if isset($game)}{$game.game_date_year}{else}{$this_year}{/if}" style="width:40px; text-align:center;" />
				</td>
			</tr>
			<tr>
				<td>Opponent</td>
				<td>
					<select name="game_opponent">
					{foreach from=$opponents item="o"}
						<option value="{$o.id}" {if isset($game) && $game.game_opponent == $o.id}selected{/if}>{$o.name}</option>
					{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Match result</td>
				<td><input type="text" maxlength="64" name="game_score" placeholder="Score" {if isset($game)}value="{$game.game_score}"{/if} style="width:50px;" /></td>
			</tr>
            <tr>
				<td>Result</td>
				<td>
					<select name="game_result">
						<option value="won" {if isset($game) && $game.game_result == "won"}selected{/if}>Win</option>
						<option value="drew" {if isset($game) && $game.game_result == "drew"}selected{/if}>Draw</option>
						<option value="lost" {if isset($game) && $game.game_result == "lost"}selected{/if}>Loss</option>
					</select>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top; padding-right:25px;">Goals and clean sheets </td>
				<td>
					
					<table id="goal_overview">
						<thead>
							<tr style="font-weight:bold;">
								<td>Player name (no.)</td>
								<td>Pos.</td>
								<td style="padding-right:15px;">Total goals</td>
								<td>Clean sheet?</td>
							</tr>
						</thead>
						<tbody>
						{foreach from=$players item="p"}
							<tr>
								<td style="padding-right:15px;">{$p.name}{if $p.no != 0} ({$p.no}){/if}</td>
								<td style="padding-right:15px;">{$p.card_position}</td>
								<td style="padding-right:15px;"><input type="text" name="goals[{$p.id}]" maxlength="1" value="{if isset($game.goals[$p.id])}{$game.goals[$p.id]}{else}0{/if}" style="width:12px; text-align:center;" /></td>
                                <td>
                                    <select name="clean_sheets[{$p.id}]">
                                        <option value="yes"{if isset($game.clean_sheets[$p.id])}selected{/if}>Yes</option>
                                        <option value="no"{if !isset($game.clean_sheets[$p.id])}selected{/if}>No</option>
                                    </select>
                                </td>
							</tr>
						{/foreach}
						</tbody>
					</table>
					
				</td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="save_type" value="Save game" style="float:right; margin-top:10px;" /></td>
			</tr>
		</tbody>
	</table>
</form>
{include file="footer.tpl"}