{include file="header.tpl"}
{include file="datatable.tpl"}
<h1>Footbal Management</h1>
<p>Manage your games, opponents, goals, clean sheets, and more!</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a id="new" href="{$base_url}football/new-game">Add game</a>
    <a id="new" href="{$base_url}football/new-opponent">Add opponent</a>
    <a id="new" href="{$base_url}football/new-player">Add player</a>
</div>

<h2>Game History</h2>
{if empty($games)}
<p id="warning">No games registered.</p>
{else}
<table class="grid display">
    <thead>
    <tr>
        <td class="title" style="width:200px;">Date</td>
        <td class="title" style="width:200px;">Opponent</td>
        <td class="title" style="width:200px;">Score</td>
        <td class="title" style="width:200px;">Result</td>
        <td class="title" style="width:100px;">&nbsp;</td>
    </tr>
    </thead>
    <tbody>
{foreach from=$games item="g"}
        <td>{$g.game_date_year}-{$g.game_date_month}-{$g.game_date_day}</td>
        <td>{$g.game_opponent.name}</td>
        <td>{$g.game_score}</td>
        <td>{$g.game_result}</td>
        <td><a href="{$base_url}football/edit-game/{$g.game_id}">Edit game</a></td>
    </tr>
{/foreach}
    </tbody>
</table>
{/if}

<h2>Players</h2>
{if empty($players)}
<p id="warning">No players registered.</p>
{else}
<table class="grid display">
    <thead>
        <tr>
            <td class="title" style="width:200px;">Name</td>
            <td class="title" style="width:200px;">Full name</td>
            <td class="title" style="width:200px;">Number</td>
            <td class="title" style="width:200px;">Position</td>
            <td class="title" style="width:200px;">Status</td>
            <td class="title" style="width:100px;">&nbsp;</td>
        </tr>
    </thead>
    <tbody>
{foreach from=$players item="p"}
        <td class="table_pic" style="background-image:url('{$site_url}media/images/player_cards/{$p.card_picture}');">{$p.display_name}</td>
        <td>{$p.name}</td>
        <td>{$p.no}</td>
        <td>{$p.card_position}</td>
        <td>{$p.status}</td>
        <td><a href="{$base_url}football/edit-player/{$p.id}">Edit</a></td>
    </tr>
{/foreach}
    </tbody>
</table>
{/if}

<h2>Opponents</h2>
{if empty($opponents)}
<p id="warning">No opponents registered.</p>
{else}
<table class="grid display">
    <thead>
    <tr>
        <td class="title" style="width:200px;">Opponent name</td>
        <td class="title" style="width:200px;">Jersey colour</td>
        <td class="title" style="width:100px;">&nbsp;</td>
    </tr>
    </thead>
    <tbody>
{foreach from=$opponents item="o"}
        <td>{$o.name}</td>
        <td>{$o.color}</td>
        <td><a href="{$base_url}football/edit-opponent/{$o.id}">Edit</a></td>
    </tr>
{/foreach}
    </tbody>
</table>
{/if}
{include file="footer.tpl"}