{include file="header.tpl"}
<h1>Add Opponent</h1>
<p>Save a new opponent.</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<form method="post" action="{$base_url}football/overview">
	<input type="hidden" name="opponent_id" maxlength="4" {if isset($opponent)}value="{$opponent.id}"{/if} />
	<table class="details">
		<tbody>
			<tr>
				<td>Opponent name</td>
				<td><input type="text" name="opponent_name" maxlength="128" placeholder="Name of the opponent team" {if isset($opponent)}value="{$opponent.name}"{/if} /></td>
			</tr>
			<tr>
				<td style="padding-right:25px;">Opponent jersey colour</td>
				<td><input type="text" name="opponent_color" maxlength="64" placeholder="Jersey colour" {if isset($opponent)}value="{$opponent.color}"{/if} /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="save_type" value="Add opponent" style="float:right; margin-top:10px;" /></td>
			</tr>
		</tbody>
	</table>
</form>
{include file="footer.tpl"}