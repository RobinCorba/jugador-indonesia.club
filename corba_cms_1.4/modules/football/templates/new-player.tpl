{include file="header.tpl"}
<h1>Add Player</h1>
{if isset($error)}<p id="error">{$error}</p>{/if}
{if isset($warning)}<p id="warning">{$warning}</p>{/if}
{if isset($message)}<p id="message">{$message}</p>{/if}
<link rel="stylesheet" type="text/css" href="{$site_url}css/cards.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<form method="post" enctype='multipart/form-data' action="{$base_url}football/overview">
	<input type="hidden" name="player_id" maxlength="4" {if isset($player)}value="{$player.player_id}"{/if} />
	<table class="details">
		<tbody>
			<tr>
				<td>Full Name</td>
				<td><input type="text" name="player_name" maxlength="128" placeholder="Name of the player" {if isset($player)}value="{$player.player_name}"{/if} /></td>
                <td rowspan="6" style="vertical-align:top;">
                    Card
                    <div class="player_card new" id="player_status_display">
                        <div class="number" id="player_no_display">99</div>
                        <div class="position" id="player_pos_display">GK</div>
                        <div class="name" id="player_name_display">???</div>
                        <div class="stats" id="stat-1">
                            <span>Goals</span>
                            <span>0</span>
                        </div>
                        <div class="stats" id="stat-2">
                            <span>Joined</span>
                            <span id="player_since_display">{$this_month_readable} {$this_year}</span>
                        </div>
                        <div class="stats" id="stat-3">
                            <span></span>
                            <span></span>
                        </div>
                        <div class="stats-footer" id="player_status_display_text">New player</div>
                        <div id="img_card_upload_preview" class="picture" style="background-image:url('{$site_url}media/images/player_cards/{if isset($player)}{$player.player_card_picture}{else}default_card_picture.png{/if}');"></div>
                    </div>
                </td>
			</tr>
            <tr>
                <td>Display Name</td>
				<td><input id="player_name_input" type="text" name="player_display_name" maxlength="12" placeholder="Short name" {if isset($player)}value="{$player.player_display_name}"{/if} style="width:120px;" /></td>
            </tr>
			<tr>
				<td>Number</td>
				<td><input id="player_no_input" class="numbersonly" type="text" name="player_no" maxlength="2" placeholder="No." value="{if isset($player)}{$player.player_no}{else}99{/if}" style="width:20px; text-align:center;" /></td>
			</tr>
			<tr>
				<td>Position(s)</td>
				<td>
					<select name="player_positions[]" multiple style="height:224px;">
					{foreach from=$positions item="p"}
						<option value="{$p.position_id}" />{$p.position_code}</option>
					{/foreach}
					</select>
				</td>
			</tr>
            <tr>
				<td>Favourite position</td>
				<td>
					<select name="player_card_position" id="player_pos_input">
					{foreach from=$positions item="p"}
						<option value="{$p.position_code}" />{$p.position_code}</option>
					{/foreach}
					</select>
				</td>
			</tr>
            <tr>
				<td>Playing since</td>
				<td>
					<select name="player_since_month" id="player_since_month_input">
						<option value="01" {if (isset($player) && $player.player_since_month == "01") || $this_month == "01"}selected{/if}>Jan.</option>
						<option value="02" {if (isset($player) && $player.player_since_month == "02") || $this_month == "02"}selected{/if}>Feb.</option>
						<option value="03" {if (isset($player) && $player.player_since_month == "03") || $this_month == "03"}selected{/if}>Mar.</option>
						<option value="04" {if (isset($player) && $player.player_since_month == "04") || $this_month == "04"}selected{/if}>Apr.</option>
						<option value="05" {if (isset($player) && $player.player_since_month == "05") || $this_month == "05"}selected{/if}>May.</option>
						<option value="06" {if (isset($player) && $player.player_since_month == "06") || $this_month == "06"}selected{/if}>Jun.</option>
						<option value="07" {if (isset($player) && $player.player_since_month == "07") || $this_month == "07"}selected{/if}>Jul.</option>
						<option value="08" {if (isset($player) && $player.player_since_month == "08") || $this_month == "08"}selected{/if}>Aug.</option>
						<option value="09" {if (isset($player) && $player.player_since_month == "09") || $this_month == "09"}selected{/if}>Sep.</option>
						<option value="10" {if (isset($player) && $player.player_since_month == "10") || $this_month == "10"}selected{/if}>Oct.</option>
						<option value="11" {if (isset($player) && $player.player_since_month == "11") || $this_month == "11"}selected{/if}>Nov.</option>
						<option value="12" {if (isset($player) && $player.player_since_month == "12") || $this_month == "12"}selected{/if}>Dec.</option>
					</select>
					<input type="text" maxlength="4" id="player_since_year_input" class="numbersonly" name="player_since_year" placeholder="Year" value="{if isset($player)}{$player.player_since_year}{else}{$this_year}{/if}" style="text-align:center; width:40px;" />
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
					<select name="player_status" id="player_status_input">
						<option value="new" {if isset($player) && $player.player_status == "new"}selected{/if}>New player</option>
						<option value="active" {if isset($player) && $player.player_status == "active"}selected{/if}>Active player</option>
						<option value="top" {if isset($player) && $player.player_status == "top"}selected{/if}>Top player</option>
                        <option value="legendary" {if isset($player) && $player.player_status == "legendary"}selected{/if}>Legendary player</option>
                        <option value="inactive" {if isset($player) && $player.player_status == "inactive"}selected{/if}>Inactive player</option>
                        <option value="former" {if isset($player) && $player.player_status == "former"}selected{/if}>Former player</option>
					</select>
				</td>
			</tr>
            <tr id="retired_row" style="display:none;">
				<td>Retired on</td>
				<td>
					<select name="player_stopped_month">
						<option value="01" {if isset($player) && $player.player_stopped_month == "01"}selected{/if}>Jan.</option>
						<option value="02" {if isset($player) && $player.player_stopped_month == "02"}selected{/if}>Feb.</option>
						<option value="03" {if isset($player) && $player.player_stopped_month == "03"}selected{/if}>Mar.</option>
						<option value="04" {if isset($player) && $player.player_stopped_month == "04"}selected{/if}>Apr.</option>
						<option value="05" {if isset($player) && $player.player_stopped_month == "05"}selected{/if}>May.</option>
						<option value="06" {if isset($player) && $player.player_stopped_month == "06"}selected{/if}>Jun.</option>
						<option value="07" {if isset($player) && $player.player_stopped_month == "07"}selected{/if}>Jul.</option>
						<option value="08" {if isset($player) && $player.player_stopped_month == "08"}selected{/if}>Aug.</option>
						<option value="09" {if isset($player) && $player.player_stopped_month == "09"}selected{/if}>Sep.</option>
						<option value="10" {if isset($player) && $player.player_stopped_month == "10"}selected{/if}>Oct.</option>
						<option value="11" {if isset($player) && $player.player_stopped_month == "11"}selected{/if}>Nov.</option>
						<option value="12" {if isset($player) && $player.player_stopped_month == "12"}selected{/if}>Dec.</option>
					</select>
					<input type="text" maxlength="4" name="player_stopped_year" placeholder="YYYY" {if isset($player)}value="{$player.player_stopped_year}"{/if} style="text-align:center; width:40px;" />
				</td>
			</tr>
			<tr>
				<td>Picture</td>
				<td>
					<img id="img_upload_preview" src="{$site_url}media/images/players/thumb_180_{if isset($player)}{$player.player_picture}{else}default_picture.jpg{/if}" alt="No preview" class="profile_picture" />
					<input type="file" name="player_picture" {if isset($player)}value="{$player.player_picture}"{/if} id="img_upload" />
				</td>
                <td rowspan="2">
                    Card Picture
					<input type="file" name="player_card_picture" {if isset($player)}value="{$player.player_card_picture}"{/if} id="img_card_upload" />
                </td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" name="save_type" value="Add player" style="float:right;" /></td>
			</tr>
		</tbody>
	</table>
</form>
{literal}
<script type="text/javascript">
    
    // Preload card name
    $("#player_name_input").keyup(function()
    {
        $("#player_name_display").text($(this).val());
    });
    // Preload card number
    $("#player_no_input").keyup(function()
    {
        $("#player_no_display").text($(this).val());
    });
    // Preload card position
    $("#player_pos_input").change(function()
    {
        $("#player_pos_display").text($(this).val());
    });
    // Preload card status
    $("#player_status_input").change(function()
    {
        $("#player_status_display").removeClass();
        $("#player_status_display").addClass("player_card "+$(this).val());
        $("#player_status_display_text").text($("#player_status_input option:selected").text());
        if($(this).val() == "former")
        {
            $("#retired_row").show();
        }
        else
        {
            $("#retired_row").hide();
        }
    });
    // Preload card since
    $("#player_since_month_input").change(updateSince);
    $("#player_since_year_input").keyup(updateSince);
    
    function updateSince()
    {
        $("#player_since_display").text($("#player_since_month_input option:selected").text()+" "+$("#player_since_year_input").val());
    }
    
    $(".numbersonly").keydown(function(e){
        // Allow: backspace, delete, tab, escape and enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
</script>
{/literal}
{include file="footer.tpl"}