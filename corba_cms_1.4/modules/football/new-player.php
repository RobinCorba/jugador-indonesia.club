<?php
	
    $class_football = Modules::load("football");
    $smarty->assign("positions", $class_football->retrievePositions());
    $smarty->assign("this_month_readable", date("M").".");
    $smarty->assign("this_month", date("m"));
    $smarty->assign("this_year", date("Y"));
	$smarty->assign("seo_title", "Add Player");
	$smarty->assign("seo_desc", "Add a new player.");
    $smarty->display(dirname(__FILE__)."/templates/new-player.tpl");
	
?>