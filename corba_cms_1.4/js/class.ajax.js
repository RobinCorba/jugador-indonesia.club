onload=function()
{
	if (document.getElementsByClassName == undefined)
	{
		document.getElementsByClassName = function(className)
		{
			var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
			var allElements = document.getElementsByTagName("*");
			var results = [];

			var element;
			for (var i = 0; (element = allElements[i]) != null; i++)
			{
				var elementClass = element.className;
				if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
				{
					results.push(element);
				}
			}

			return results;
		}
	}
	
	$("#img_upload_post").change(function(){         readURL(this,"img_upload_preview_post", "img",1200,800);	            });
    $("#img_upload").change(function(){         readURL(this,"img_upload_preview", "img");	            });
	$("#img_card_upload").change(function(){    readURL(this,"img_card_upload_preview", "background");	});
}


function confirmDelete(text, url)
{
	if(text == "")
	{
		text = "this";
	}
	if(confirm("Are you sure you want to delete " + text + "?"))
	{
		window.location = url;
	}
}

// Image upload preview
function readURL(input, id_preview_element, preview_type, min_width, min_height)
{
    if(input.files && input.files[0])
	{
        // Check the file size first
        var file_fize_mb            = input.files[0].size / 1024 / 1024;
        var file_upload_limit_mb    = 1;
        if(file_fize_mb > file_upload_limit_mb)
        {
            alert("Image exceeds max. allowed file size (1 MB).");
            input.value = "";
        }
        else
        {
            // Check the minimum image dimensions
            var fr = new FileReader;
            fr.onload = function()
            {
                var img = new Image;
                img.onload = function()
                {
                    if(img.width < min_width || img.height < min_height)
                    {
                        alert("This picture is too small ("+img.width+"x"+img.height+"), the minimum size is "+min_width+"x"+min_height+".");
                        input.value = "";
                    }
                    else
                    {
                        // Everything is okay, read the file from client
                        var reader = new FileReader();
                        reader.onload = function(e)
                        {
                            if(preview_type == "img")
                            {
                                $('#'+id_preview_element).attr('src', e.target.result);
                            }
                            else if(preview_type == "background")
                            {
                                $('#'+id_preview_element).css('background-image', "url('"+e.target.result+"')");
                            }
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                };
                img.src = fr.result;
            };
            fr.readAsDataURL(input.files[0]);
        }
    }
}