<?php
/* Smarty version 3.1.30, created on 2016-10-09 02:58:25
  from "/var/www/jugador-indonesia.club/admin/templates/api_overview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f94fe1366b83_54061831',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '370068db2939360dc3c1a32a9420bcf68b5675f7' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/api_overview.tpl',
      1 => 1475909522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57f94fe1366b83_54061831 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="header">Corba CMS - API documentation (version April 12th, 2013, generated on <?php echo $_smarty_tpl->tpl_vars['today']->value;?>
)</div>
<div id="container">
	<div id="nav">
	<h2>Library</h2>
		<?php
$__section_this_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_section_this']) ? $_smarty_tpl->tpl_vars['__smarty_section_this'] : false;
$__section_this_0_loop = (is_array(@$_loop=((string)$_smarty_tpl->tpl_vars['library']->value)) ? count($_loop) : max(0, (int) $_loop));
$__section_this_0_total = $__section_this_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_this'] = new Smarty_Variable(array());
if ($__section_this_0_total != 0) {
for ($__section_this_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_this']->value['index'] = 0; $__section_this_0_iteration <= $__section_this_0_total; $__section_this_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_this']->value['index']++){
?>
			<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=api_overview&class=<?php echo $_smarty_tpl->tpl_vars['library']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_this']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_this']->value['index'] : null)];?>
"><?php echo $_smarty_tpl->tpl_vars['library']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_this']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_this']->value['index'] : null)];?>
</a><br />
		<?php
}
}
if ($__section_this_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_section_this'] = $__section_this_0_saved;
}
?>
	</div>
	<div id="class">
		<?php if ($_smarty_tpl->tpl_vars['class_name']->value != '') {?>
			<h1><?php echo $_smarty_tpl->tpl_vars['class_name']->value;?>
 class</h1>
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['functions']->value), 'this');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['this']->value) {
?>
				<hr />
				<h2>Function <?php echo $_smarty_tpl->tpl_vars['this']->value['name_full'];?>
)</h2>
				<?php if ($_smarty_tpl->tpl_vars['this']->value['desc'] != '') {?>
					<span class="desc"><?php echo $_smarty_tpl->tpl_vars['this']->value['desc'];?>
</span>
				<?php } else { ?>
					<span class="desc"><em>Missing documentation</em></span>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['this']->value['return_type'] != '') {?>
					<strong>Returns</strong><br />
					<span class="return">
						<a class="type"><?php echo $_smarty_tpl->tpl_vars['this']->value['return_type'];?>
</a>	<a class="desc"><?php echo $_smarty_tpl->tpl_vars['this']->value['return_desc'];?>
</a>
					</span>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['this']->value['params'] != '') {?>
					<strong>Input variables</strong><br />
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['this']->value['params'], 'param');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['param']->value) {
?>
						<span class="param">
							<a class="type"><?php echo $_smarty_tpl->tpl_vars['param']->value['type'];?>
</a> <?php echo $_smarty_tpl->tpl_vars['param']->value['name'];?>
<br />
							<a class="desc"><?php echo $_smarty_tpl->tpl_vars['param']->value['desc'];?>
</a>
						</span>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

				<?php } else { ?>
				
				<?php }?>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['error']->value) {
echo $_smarty_tpl->tpl_vars['error']->value;
}?>
	</div>
</div><?php }
}
