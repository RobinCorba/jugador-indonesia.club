<?php /* Smarty version 2.6.3, created on 2016-08-27 17:05:19
         compiled from header.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php if ($this->_tpl_vars['seo_title'] != ""):  echo $this->_tpl_vars['seo_title'];  else: ?>Administration<?php endif; ?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="<?php if ($this->_tpl_vars['seo_desc']):  echo $this->_tpl_vars['seo_desc']; ?>
, <?php endif; ?>Jugador CF" />
		<meta name="keywords" content="Corba CMS" />
		<meta name="robots" content="nofollow, noindex" />
		<meta name="copyright" content="Robin Corba 2012-2016" />
		
		<?php if ($this->_tpl_vars['p'] == 'api_overview'): ?>
			<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['base_url']; ?>
templates/css/style_api.css" />
		<?php else: ?>
			<link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['base_url']; ?>
templates/css/style.css" />
		<?php endif; ?>
		<link rel="shortcut icon" href="<?php echo $this->_tpl_vars['base_url']; ?>
favicon.ico" />
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['base_url']; ?>
js/class.ajax.js"></script>
	</head>
	<body>
		<?php if ($this->_tpl_vars['p'] != 'api_overview'): ?>
			<div id="header"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
templates/img/theboumas_logo.jpg" alt="Corba CMS built for www.theboumas.net" /></div>
			<div id="container">
				<div id="nav">
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
news">News</a>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
?p=players">Players</a>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
?p=opponents">Opponents</a>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
?p=games">Games</a>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
?p=statistics">Statistics</a>
					<?php if ($this->_tpl_vars['user_type'] == 'superadmin'): ?>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
?p=user_overview">CMS Users</a>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
?p=site_config">Site configuration</a>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
?p=api_overview">API Documentation</a>
					<?php endif; ?>
					<a href="<?php echo $this->_tpl_vars['base_url']; ?>
logout">Logout</a>
				</div>
				<div id="content">
			<?php endif; ?>