<?php /* Smarty version 2.6.3, created on 2013-02-27 17:42:33
         compiled from banner_overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'banner_overview.tpl', 21, false),)), $this); ?>
<h1>Banners</h1>
<p>Manage Banners for the Pizza Hut Indonesia website</p>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=banner_new">Upload new banner</a>
</div>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['banners']): ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:200px;">Banner name</td>
		<td style="width:200px;">Location</td>
        <td style="width:200px;">Published on</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['banners']))):
    foreach ($_from as $this->_tpl_vars['banner']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td><?php echo $this->_tpl_vars['banner']['banner_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['banner']['banner_location']; ?>
</td>
        <td><?php echo $this->_tpl_vars['banner']['banner_date_uploaded_real']; ?>
 hrs)</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=banner_edit&id=<?php echo $this->_tpl_vars['banner']['banner_id']; ?>
">Edit</a> | <a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['banner']['banner_name']; ?>
', '<?php echo $this->_tpl_vars['base_dir']; ?>
?p=banner_overview&del=<?php echo $this->_tpl_vars['banner']['banner_id']; ?>
');">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<?php else: ?>
	<p id="warning">There are no active banners on the website</p>
<?php endif; ?>