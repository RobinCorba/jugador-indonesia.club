<?php
/* Smarty version 3.1.30, created on 2016-10-09 14:33:04
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/news/templates/overview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f9f2b0797bf7_01372885',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9e47ba630c58a37e712be67fec422554c1c636c1' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/news/templates/overview.tpl',
      1 => 1475998374,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:datatable.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f9f2b0797bf7_01372885 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_cycle')) require_once '/var/www/jugador-indonesia.club/corba_cms_1.4/library/smarty_3.1.30/plugins/function.cycle.php';
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:datatable.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<h1>News Management</h1>
<p>An overview of all articles.</p>
<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<div id="actions">
	<a id="new" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news/new">New article</a>
</div>
<?php if ($_smarty_tpl->tpl_vars['articles']->value == '') {?>
	<p>No articles written yet.</p>
<?php } else { ?>
	<table class="grid display">
		<thead>
		<tr>
			<td>Publish date</td>
            <td>Article title</td>
			<td>Status</td>
			<td>&nbsp;</td>
		</tr>
		</thead>
		<tbody>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['articles']->value, 'article');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['article']->value) {
?>
		<tr <?php echo smarty_function_cycle(array('values'=>',class="odd"'),$_smarty_tpl);?>
>
			<td style="width:220px;"><?php echo $_smarty_tpl->tpl_vars['article']->value['publish_date_alt'];?>
 (<?php echo $_smarty_tpl->tpl_vars['article']->value['publish_time'];?>
 hrs)</td>
            <td style="width:500px;"><?php echo $_smarty_tpl->tpl_vars['article']->value['short_title'];?>
</td>
			<td style="width:60px;"><?php echo $_smarty_tpl->tpl_vars['article']->value['status'];?>
</td>
			<td style="width:80px;"><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news/edit/<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
">Edit</a> | <a href="javascript:confirmDelete('article \'<?php echo $_smarty_tpl->tpl_vars['article']->value['title'];?>
\'','<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news/overview/delete/<?php echo $_smarty_tpl->tpl_vars['article']->value['id'];?>
')">Delete</a></td>
		</tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</tbody>
	</table>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
