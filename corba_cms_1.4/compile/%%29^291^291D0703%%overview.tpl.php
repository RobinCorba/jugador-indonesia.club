<?php /* Smarty version 2.6.3, created on 2016-04-15 00:17:08
         compiled from /var/www/html/jugador-indonesia.club/admin/modules/news/templates/overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', '/var/www/html/jugador-indonesia.club/admin/modules/news/templates/overview.tpl', 27, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "datatable.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<h1>News Management</h1>
<p>An overview of all articles.</p>
<?php if (! empty ( $this->_tpl_vars['error'] )): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if (! empty ( $this->_tpl_vars['warning'] )): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if (! empty ( $this->_tpl_vars['message'] )): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_url']; ?>
news/new">New article</a>
</div>
<?php if ($this->_tpl_vars['articles'] == ""): ?>
	<p>No articles written yet.</p>
<?php else: ?>
	<table id="grid" class="display">
		<thead>
		<tr>
			<td>Article title</td>
			<td>Type</td>
			<td>Published</td>
			<td>Status</td>
			<td>&nbsp;</td>
		</tr>
		</thead>
		<tbody>
	<?php if (count($_from = (array)$this->_tpl_vars['articles'])):
    foreach ($_from as $this->_tpl_vars['article']):
?>
		<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
			<td style="width:500px;"><?php echo $this->_tpl_vars['article']['post_title_shortened']; ?>
</td>
			<td style="width:80px;"><?php echo $this->_tpl_vars['article']['post_type']; ?>
</td>
			<td style="width:220px;"><?php echo $this->_tpl_vars['article']['post_date_published_converted']; ?>
 <?php echo $this->_tpl_vars['article']['post_time_published_real']; ?>
 hrs</td>
			<td style="width:60px;"><?php echo $this->_tpl_vars['article']['post_status']; ?>
</td>
			<td style="width:80px;"><a href="<?php echo $this->_tpl_vars['base_url']; ?>
news/edit/<?php echo $this->_tpl_vars['article']['post_id']; ?>
">Edit</a> | <a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['article']['post_title']; ?>
','<?php echo $this->_tpl_vars['base_url']; ?>
news/overview/delete/<?php echo $this->_tpl_vars['article']['post_id']; ?>
')">Delete</a></td>
		</tr>
	<?php endforeach; unset($_from); endif; ?>
		</tbody>
	</table>
<?php endif;  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>