<?php /* Smarty version 2.6.3, created on 2016-07-17 15:53:37
         compiled from games.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>All recorded games</h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=games_new">Add game results</a>
</div>
<?php if ($this->_tpl_vars['games'] != ""): ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Date</td>
		<td class="title" style="width:200px;">Opponent</td>
		<td class="title" style="width:200px;">Score</td>
		<td class="title" style="width:200px;">Result</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['games']))):
    foreach ($_from as $this->_tpl_vars['g']):
?>
		<td><?php echo $this->_tpl_vars['g']['game_date_year']; ?>
-<?php echo $this->_tpl_vars['g']['game_date_month']; ?>
-<?php echo $this->_tpl_vars['g']['game_date_day']; ?>
</td>
		<td><?php echo $this->_tpl_vars['g']['game_opponent']['opponent_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['g']['game_score']; ?>
</td>
		<td><?php echo $this->_tpl_vars['g']['game_result']; ?>
</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=game_edit&id=<?php echo $this->_tpl_vars['g']['game_id']; ?>
">Edit</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<?php else: ?>
	<p>No games available.</p>
<?php endif;  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>