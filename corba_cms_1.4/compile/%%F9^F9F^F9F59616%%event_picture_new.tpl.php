<?php /* Smarty version 2.6.3, created on 2013-04-14 23:10:33
         compiled from event_picture_new.tpl */ ?>
<h1>Upload new picture</h1>
<p>Upload new picture for event</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form enctype="multipart/form-data" method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=event_picture_new">
<table id="details" style="width:600px;">
	<tr class="odd">
		<td>For which event?</td>
		<td>
			<select name="event_id">
				<?php if (count($_from = (array)($this->_tpl_vars['events']))):
    foreach ($_from as $this->_tpl_vars['e']):
?>
				<option value="<?php echo $this->_tpl_vars['e']['id']; ?>
"><?php echo $this->_tpl_vars['e']['name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Picture</td>
		<td>
			<input name="event_picture" type="file" accept="image/*" /><br />
			<em>Minimum size 450 x 300 pixels (max. 2MB)</em>
		</td>
	</tr>
	<tr class="odd">
		<td colspan="2"><input type="submit" value="Upload picture" style="float:right;" /></td>
	</tr>
</table>
</form>