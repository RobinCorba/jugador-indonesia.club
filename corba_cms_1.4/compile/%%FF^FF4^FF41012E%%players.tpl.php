<?php /* Smarty version 2.6.3, created on 2016-07-17 15:53:08
         compiled from players.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>Team Roster</h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=players_new">Add player</a>
</div>
<?php if ($this->_tpl_vars['players'] != ""): ?>
<table id="grid" class="display">
	<thead>
	<tr>
        <td class="title" style="width:100px;">Photo</td>
		<td class="title">Name</td>
		<td class="title">#</td>
		<td class="title">Position</td>
		<td class="title">Status</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['players']))):
    foreach ($_from as $this->_tpl_vars['p']):
?>
		<td>
            <img src="<?php echo $this->_tpl_vars['site_dir']; ?>
media/images/players/thumb_120_<?php echo $this->_tpl_vars['p']['player_picture']; ?>
" alt="<?php echo $this->_tpl_vars['p']['player_name']; ?>
" class="profile_picture icon" style="float:left; margin-right:5px;" />
            <img src="<?php echo $this->_tpl_vars['site_dir']; ?>
media/images/player_cards/<?php echo $this->_tpl_vars['p']['player_card_picture']; ?>
" alt="<?php echo $this->_tpl_vars['p']['player_name']; ?>
" style="width:50px; height:auto;" />
        </td>
        <td>
            <span style="font-size:16px;"><?php echo $this->_tpl_vars['p']['player_name']; ?>
</span><br />
            <em><?php echo $this->_tpl_vars['p']['player_display_name']; ?>
</em>
        </td>
		<td><?php echo $this->_tpl_vars['p']['player_no']; ?>
</td>
		<td>
		<?php if (count($_from = (array)$this->_tpl_vars['p']['player_positions'])):
    foreach ($_from as $this->_tpl_vars['position']):
?>
			<?php echo $this->_tpl_vars['position']['position_code']; ?>
,
		<?php endforeach; unset($_from); endif; ?>
		</td>
		<td><?php echo $this->_tpl_vars['p']['player_status']; ?>
</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=players_edit&id=<?php echo $this->_tpl_vars['p']['player_id']; ?>
">Edit</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<?php else: ?>
	<p>No players are available.</p>
<?php endif;  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>