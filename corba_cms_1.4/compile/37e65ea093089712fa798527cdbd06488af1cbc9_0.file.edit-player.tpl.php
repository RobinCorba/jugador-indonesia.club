<?php
/* Smarty version 3.1.30, created on 2016-10-15 19:00:00
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/football/templates/edit-player.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58021a406d7838_79472211',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '37e65ea093089712fa798527cdbd06488af1cbc9' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/football/templates/edit-player.tpl',
      1 => 1476532798,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_58021a406d7838_79472211 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Edit Player <?php echo $_smarty_tpl->tpl_vars['player']->value['name'];?>
</h1>
<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (isset($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (isset($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
css/cards.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<form method="post" enctype='multipart/form-data' action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/overview">
	<input type="hidden" name="player_id" maxlength="4" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['player']->value['id'];?>
"<?php }?> />
	<table class="details">
		<tbody>
			<tr>
				<td>Full Name</td>
				<td><input type="text" name="player_name" maxlength="128" placeholder="Name of the player" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['player']->value['name'];?>
"<?php }?> /></td>
                <td rowspan="6" style="vertical-align:top;">
                    <h2 style="text-align:center;">Player Card</h2>
                    <div class="player_card <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['status'];
} else { ?>new<?php }?>" id="player_status_display">
                        <div class="number" id="player_no_display"><?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['no'];
} else { ?>99<?php }?></div>
                        <div class="position" id="player_pos_display"><?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['card_position'];
} else { ?>GK<?php }?></div>
                        <div class="name" id="player_name_display"><?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['display_name'];
} else { ?>???<?php }?></div>
                        <div class="stats" id="stat-1">
                            <span>Goals</span>
                            <span>0</span>
                        </div>
                        <div class="stats" id="stat-2">
                            <span>Joined</span>
                            <span id="player_since_display"><?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['since_month_readable'];?>
 <?php echo $_smarty_tpl->tpl_vars['player']->value['since_year'];
} else {
echo $_smarty_tpl->tpl_vars['this_month_readable']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['this_year']->value;
}?></span>
                        </div>
                        <div class="stats" id="stat-3">
                            <span></span>
                            <span></span>
                        </div>
                        <div class="stats-footer" id="player_status_display_text"><?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['status_readable'];
} else { ?>New player<?php }?></div>
                        <div id="img_card_upload_preview" class="picture" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
media/images/player_cards/<?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['card_picture'];
} else { ?>default_card_picture.png<?php }?>');"></div>
                    </div>
                </td>
			</tr>
            <tr>
                <td>Display Name</td>
				<td><input id="player_name_input" type="text" name="player_display_name" maxlength="12" placeholder="Short name" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['player']->value['display_name'];?>
"<?php }?> style="width:120px;" /></td>
            </tr>
			<tr>
				<td>Number</td>
				<td><input id="player_no_input" class="numbersonly" type="text" name="player_no" maxlength="2" placeholder="No." value="<?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['no'];
} else { ?>99<?php }?>" style="width:20px; text-align:center;" /></td>
			</tr>
			<tr>
				<td>Position(s)</td>
				<td>
					<select name="player_positions[]" multiple style="height:224px;">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['positions']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['p']->value['position_id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['player']->value['positions'], 'pp');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['pp']->value) {
if ($_smarty_tpl->tpl_vars['pp']->value == $_smarty_tpl->tpl_vars['p']->value) {?>selected<?php }
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
}?> /><?php echo $_smarty_tpl->tpl_vars['p']->value['position_code'];?>
</option>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					</select>
				</td>
			</tr>
            <tr>
				<td>Favourite position</td>
				<td>
					<select name="player_card_position" id="player_pos_input">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['positions']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['p']->value['position_code'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
if ($_smarty_tpl->tpl_vars['player']->value['card_position'] == $_smarty_tpl->tpl_vars['p']->value['position_code']) {?>selected<?php }
}?> /><?php echo $_smarty_tpl->tpl_vars['p']->value['position_code'];?>
</option>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					</select>
				</td>
			</tr>
            <tr>
				<td>Playing since</td>
				<td>
					<select name="player_since_month" id="player_since_month_input">
						<option value="01" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "01")) {?>selected<?php }?>>Jan.</option>
						<option value="02" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "02")) {?>selected<?php }?>>Feb.</option>
						<option value="03" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "03")) {?>selected<?php }?>>Mar.</option>
						<option value="04" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "04")) {?>selected<?php }?>>Apr.</option>
						<option value="05" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "05")) {?>selected<?php }?>>May.</option>
						<option value="06" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "06")) {?>selected<?php }?>>Jun.</option>
						<option value="07" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "07")) {?>selected<?php }?>>Jul.</option>
						<option value="08" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "08")) {?>selected<?php }?>>Aug.</option>
						<option value="09" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "09")) {?>selected<?php }?>>Sep.</option>
						<option value="10" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "10")) {?>selected<?php }?>>Oct.</option>
						<option value="11" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "11")) {?>selected<?php }?>>Nov.</option>
						<option value="12" <?php if ((isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['since_month'] == "12")) {?>selected<?php }?>>Dec.</option>
					</select>
					<input type="text" maxlength="4" id="player_since_year_input" class="numbersonly" name="player_since_year" placeholder="Year" value="<?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['since_year'];
} else {
echo $_smarty_tpl->tpl_vars['this_year']->value;
}?>" style="text-align:center; width:40px;" />
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
					<select name="player_status" id="player_status_input">
						<option value="new" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['status'] == "new") {?>selected<?php }?>>New player</option>
						<option value="active" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['status'] == "active") {?>selected<?php }?>>Active player</option>
						<option value="top" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['status'] == "top") {?>selected<?php }?>>Top player</option>
                        <option value="legendary" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['status'] == "legendary") {?>selected<?php }?>>Legendary player</option>
                        <option value="inactive" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['status'] == "inactive") {?>selected<?php }?>>Inactive player</option>
                        <option value="former" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['status'] == "former") {?>selected<?php }?>>Former player</option>
					</select>
				</td>
			</tr>
            <tr id="retired_row" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['status'] != "former") {?>style="display:none;"<?php }?>>
				<td>Retired on</td>
				<td>
					<select name="player_stopped_month">
						<option value="01" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "01") {?>selected<?php }?>>Jan.</option>
						<option value="02" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "02") {?>selected<?php }?>>Feb.</option>
						<option value="03" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "03") {?>selected<?php }?>>Mar.</option>
						<option value="04" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "04") {?>selected<?php }?>>Apr.</option>
						<option value="05" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "05") {?>selected<?php }?>>May.</option>
						<option value="06" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "06") {?>selected<?php }?>>Jun.</option>
						<option value="07" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "07") {?>selected<?php }?>>Jul.</option>
						<option value="08" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "08") {?>selected<?php }?>>Aug.</option>
						<option value="09" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "09") {?>selected<?php }?>>Sep.</option>
						<option value="10" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "10") {?>selected<?php }?>>Oct.</option>
						<option value="11" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "11") {?>selected<?php }?>>Nov.</option>
						<option value="12" <?php if (isset($_smarty_tpl->tpl_vars['player']->value) && $_smarty_tpl->tpl_vars['player']->value['stopped_month'] == "12") {?>selected<?php }?>>Dec.</option>
					</select>
					<input type="text" maxlength="4" name="player_stopped_year" placeholder="YYYY" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['player']->value['stopped_year'];?>
"<?php }?> style="text-align:center; width:40px;" />
				</td>
			</tr>
			<tr>
				<td>Picture</td>
				<td>
					<img id="img_upload_preview" src="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
media/images/players/thumb_180_<?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {
echo $_smarty_tpl->tpl_vars['player']->value['picture'];
} else { ?>default_picture.jpg<?php }?>" alt="No preview" class="profile_picture" />
					<input type="file" name="player_picture" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['player']->value['player_picture'];?>
"<?php }?> id="img_upload" />
				</td>
                <td rowspan="2">
                    Card Picture
					<input type="file" name="player_card_picture" <?php if (isset($_smarty_tpl->tpl_vars['player']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['player']->value['player_card_picture'];?>
"<?php }?> id="img_card_upload" />
                </td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" name="save_type" value="Save changes to player" style="float:right;" /></td>
			</tr>
		</tbody>
	</table>
</form>

<?php echo '<script'; ?>
 type="text/javascript">
    
    // Preload card name
    $("#player_name_input").keyup(function()
    {
        $("#player_name_display").text($(this).val());
    });
    // Preload card number
    $("#player_no_input").keyup(function()
    {
        $("#player_no_display").text($(this).val());
    });
    // Preload card position
    $("#player_pos_input").change(function()
    {
        $("#player_pos_display").text($(this).val());
    });
    // Preload card status
    $("#player_status_input").change(function()
    {
        $("#player_status_display").removeClass();
        $("#player_status_display").addClass("player_card "+$(this).val());
        $("#player_status_display_text").text($("#player_status_input option:selected").text());
        if($(this).val() == "former")
        {
            $("#retired_row").show();
        }
        else
        {
            $("#retired_row").hide();
        }
    });
    // Preload card since
    $("#player_since_month_input").change(updateSince);
    $("#player_since_year_input").keyup(updateSince);
    
    function updateSince()
    {
        $("#player_since_display").text($("#player_since_month_input option:selected").text()+" "+$("#player_since_year_input").val());
    }
    
    $(".numbersonly").keydown(function(e){
        // Allow: backspace, delete, tab, escape and enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
<?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
