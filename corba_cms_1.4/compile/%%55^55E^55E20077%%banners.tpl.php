<?php /* Smarty version 2.6.3, created on 2012-07-09 14:39:29
         compiled from banners.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'banners.tpl', 21, false),)), $this); ?>
<h1>Banners</h1>
<p>Manage Banners for the Pizza Hut Indonesia website</p>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=banner_new">New Banners</a>
</div>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:300px;">Preview</td>
		<td style="width:200px;">Banner Name</td>
		<td style="width:200px;">Banner Position</td>
        <td style="width:200px;">Banner Type</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['banners']))):
    foreach ($_from as $this->_tpl_vars['row']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td>
         <a href="<?php echo $this->_tpl_vars['site_dir']; ?>
media/<?php echo $this->_tpl_vars['row']['ban_type']; ?>
/<?php echo $this->_tpl_vars['row']['ban_image']; ?>
" class="thickbox">
        <img src="<?php echo $this->_tpl_vars['site_dir']; ?>
media/<?php echo $this->_tpl_vars['row']['ban_type']; ?>
/thumb_<?php echo $this->_tpl_vars['row']['ban_image']; ?>
" />
        </a></td>
		<td><?php echo $this->_tpl_vars['row']['ban_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['row']['ban_position']; ?>
</td>
        <td><?php echo $this->_tpl_vars['row']['ban_type']; ?>
</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=banner_edit&id=<?php echo $this->_tpl_vars['row']['ban_id']; ?>
">Edit</a> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=products&del=<?php echo $this->_tpl_vars['row']['ban_id']; ?>
" onclick="return sure()">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>