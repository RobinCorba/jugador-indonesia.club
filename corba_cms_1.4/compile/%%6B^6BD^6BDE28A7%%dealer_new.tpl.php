<?php /* Smarty version 2.6.3, created on 2013-02-24 15:50:28
         compiled from dealer_new.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>Create new dealership</h1>
<p>Add a new dealership for a specific brand</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=dealer_new">
<table id="details" style="width:800px;">
	<tr>
		<td style="width:220px;">Brand</td>
		<td>
			<select name="dealer_brand_id">
				<option value="">Choose a brand..</option>
				<?php if (count($_from = (array)$this->_tpl_vars['brands'])):
    foreach ($_from as $this->_tpl_vars['b']):
?>
				<option value="<?php echo $this->_tpl_vars['b']['brand_id']; ?>
" <?php if ($this->_tpl_vars['data']['dealer_brand_id'] == $this->_tpl_vars['b']['brand_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['b']['brand_name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td>Dealership name</td>
		<td>
			<input type="text" name="dealer_name" maxlength="64" style="width:250px;" value="<?php echo $this->_tpl_vars['data']['dealer_name']; ?>
" /><br />
			<em>Example: Honda Dealership Slipi</em>
		</td>
	</tr>
	<tr>
		<td>Address</td>
		<td>
			<em>Max. 128 characters</em>
			<textarea name="dealer_address" style="width:400px; height:200px;"><?php echo $this->_tpl_vars['data']['dealer_address']; ?>
</textarea>
		</td>
	</tr>
	<tr class="odd">
		<td colspan="2"><input type="submit" value="Save this dealership" style="float:right;" /></td>
	</tr>
</table>
</form>