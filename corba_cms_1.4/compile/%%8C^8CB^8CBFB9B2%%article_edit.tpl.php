<?php /* Smarty version 2.6.3, created on 2016-07-17 23:59:28
         compiled from article_edit.tpl */ ?>
<h1>Edit article</h1>
<p>Make changes to an article of the news section</p>
<?php if (! empty ( $this->_tpl_vars['error'] )): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if (! empty ( $this->_tpl_vars['warning'] )): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if (! empty ( $this->_tpl_vars['message'] )): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_url']; ?>
?p=article_edit&id=<?php echo $this->_tpl_vars['data']['post_id']; ?>
">
<input type="hidden" name="post_id" value="<?php echo $this->_tpl_vars['data']['post_id']; ?>
" />
<table id="details" style="width:800px;">
	<tr class="odd">
		<td colspan="2" style="font-weight:normal; font-size:11px;">
			<input type="text" name="post_title" maxlength="128" value="<?php echo $this->_tpl_vars['data']['post_title']; ?>
" id="preview_title" /><br />
			<input type="text" name="post_desc" maxlength="256" value="<?php echo $this->_tpl_vars['data']['post_desc']; ?>
" id="preview_summary" /><br />
			<textarea name="post_content" id="post_content"><?php echo $this->_tpl_vars['data']['post_content']; ?>
</textarea>
		</td>
	</tr>
	<tr>
		<td>Article type</td>
		<td>
			<select name="post_type">
				<option value="personal" <?php if ($this->_tpl_vars['data']['post_type'] == 'personal'): ?>selected<?php endif; ?>>Personal story</option>
				<option value="business"<?php if ($this->_tpl_vars['data']['post_type'] == 'business'): ?>selected<?php endif; ?>>Business</option>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td>Tags</td>
		<td>
			<input type="text" name="post_tags" maxlength="128" style="width:400px;" value="<?php echo $this->_tpl_vars['data']['post_tags']; ?>
" /><br />
			<em>Separate tags with a comma ','. This will improve search results and connecting to related articles</em>
		</td>
	</tr> 
	<tr>
		<td>Article status</td>
		<td>
			<select name="post_status">
				<option value="draft"<?php if ($this->_tpl_vars['data']['post_status'] == 'draft'): ?>selected<?php endif; ?>>Draft</option>
				<option value="published" <?php if ($this->_tpl_vars['data']['post_status'] == 'published'): ?>selected<?php endif; ?>>Published</option>
				<option value="removed" <?php if ($this->_tpl_vars['data']['post_status'] == 'removed'): ?>selected<?php endif; ?>>Removed</option>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td colspan="2"><input type="submit" name="publish" value="Save changes" style="float:right;" /></td>
	</tr>
</table>
</form>