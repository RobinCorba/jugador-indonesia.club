<?php /* Smarty version 2.6.3, created on 2012-08-29 21:06:50
         compiled from ourpeople_overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'ourpeople_overview.tpl', 26, false),)), $this); ?>
<h1>Page Management</h1>
<p>An overview of all representatives.</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=ourpeople_new">New representative</a>
</div>
<?php if ($this->_tpl_vars['representatives'] == ""): ?>
	<p id="warning">Warning, no representatives are available on the website.</p>
<?php else: ?>
	<table id="grid" class="display dataTable">
		<thead>
			<tr>
				<td class="no_sort">#</td>
				<td class="no_sort">Picture</td>
				<td class="no_sort">Full Name</td>
				<td class="no_sort">Job Position</td>
				<td class="no_sort">Edited on</td>
				<td class="no_sort">Created on</td>
				<td class="no_sort">&nbsp;</td>
			</tr>
		</thead>
		<tbody class="ui-sortable">
	<?php if (count($_from = (array)($this->_tpl_vars['representatives']))):
    foreach ($_from as $this->_tpl_vars['person']):
?>
		<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
 data-position="<?php echo $this->_tpl_vars['person']['person_order']; ?>
" id="<?php echo $this->_tpl_vars['person']['person_order']; ?>
">
			<td><?php echo $this->_tpl_vars['person']['person_order']; ?>
</td>
			<td>
				<?php if ($this->_tpl_vars['person']['person_image_name'] == ""): ?>
					No picture of <?php echo $this->_tpl_vars['person']['person_name']; ?>
.
				<?php else: ?>
					<img src="<?php echo $this->_tpl_vars['site_dir']; ?>
media/images/representatives/thumb_<?php echo $this->_tpl_vars['person']['person_image_name']; ?>
" alt="Picture of <?php echo $this->_tpl_vars['person']['person_name']; ?>
" title="Picture of <?php echo $this->_tpl_vars['person']['person_name']; ?>
" />
				<?php endif; ?>
			</td>
			<td><?php echo $this->_tpl_vars['person']['person_name']; ?>
</td>
			<td><?php echo $this->_tpl_vars['person']['person_function']; ?>
</td>
			<td><?php echo $this->_tpl_vars['person']['person_date_edited_real']; ?>
</td>
			<td><?php echo $this->_tpl_vars['person']['person_date_added_real']; ?>
</td>
			<td>
				<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=ourpeople_edit&id=<?php echo $this->_tpl_vars['person']['person_id']; ?>
">Edit</a> | 
				<a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['person']['person_name']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=ourpeople_overview&del=<?php echo $this->_tpl_vars['person']['person_id']; ?>
');">Delete</a>
				<?php if ($this->_tpl_vars['page']['total_subs'] != 0): ?> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=page_sub_overview&id=<?php echo $this->_tpl_vars['page']['id']; ?>
">Manage subpages</a><?php endif; ?>
			</td>
		</tr>
	<?php endforeach; unset($_from); endif; ?>
		</tbody>
	</table>
<?php endif; ?>