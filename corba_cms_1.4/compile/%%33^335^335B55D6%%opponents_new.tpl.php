<?php /* Smarty version 2.6.3, created on 2016-07-17 15:53:25
         compiled from opponents_new.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>Add opponent</h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post">
	<input type="hidden" name="opponent_id" maxlength="4" <?php if (isset ( $this->_tpl_vars['opponent'] )): ?>value="<?php echo $this->_tpl_vars['opponent']['opponent_id']; ?>
"<?php endif; ?> />
	<table class="details">
		<tbody>
			<tr>
				<td>Opponent name</td>
				<td><input type="text" name="opponent_name" maxlength="128" placeholder="Name of the opponent team" <?php if (isset ( $this->_tpl_vars['opponent'] )): ?>value="<?php echo $this->_tpl_vars['opponent']['opponent_name']; ?>
"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td>Opponent color</td>
				<td><input type="text" name="opponent_color" maxlength="64" placeholder="Color of the opponent team's jersey" <?php if (isset ( $this->_tpl_vars['opponent'] )): ?>value="<?php echo $this->_tpl_vars['opponent']['opponent_color']; ?>
"<?php endif; ?> /></td>
			</tr>
			<tr>
				<td coslpan="2"><input type="submit" value="Save" /></td>
			</tr>
		</tbody>
	</table>
</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>