<?php /* Smarty version 2.6.3, created on 2015-10-31 20:40:09
         compiled from user_overview.tpl */ ?>
<h1>All accounts</h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=user_new">Create new user</a>
</div>
<?php if ($this->_tpl_vars['users'] != ""): ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Admin name</td>
		<td class="title" style="width:150px;">Account type</td>
		<td class="title" style="width:150px;">Registered</td>
		<td class="title" style="width:200px;">Last login</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php unset($this->_sections['this']);
$this->_sections['this']['name'] = 'this';
$this->_sections['this']['loop'] = is_array($_loop=$this->_tpl_vars['users']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['this']['show'] = true;
$this->_sections['this']['max'] = $this->_sections['this']['loop'];
$this->_sections['this']['step'] = 1;
$this->_sections['this']['start'] = $this->_sections['this']['step'] > 0 ? 0 : $this->_sections['this']['loop']-1;
if ($this->_sections['this']['show']) {
    $this->_sections['this']['total'] = $this->_sections['this']['loop'];
    if ($this->_sections['this']['total'] == 0)
        $this->_sections['this']['show'] = false;
} else
    $this->_sections['this']['total'] = 0;
if ($this->_sections['this']['show']):

            for ($this->_sections['this']['index'] = $this->_sections['this']['start'], $this->_sections['this']['iteration'] = 1;
                 $this->_sections['this']['iteration'] <= $this->_sections['this']['total'];
                 $this->_sections['this']['index'] += $this->_sections['this']['step'], $this->_sections['this']['iteration']++):
$this->_sections['this']['rownum'] = $this->_sections['this']['iteration'];
$this->_sections['this']['index_prev'] = $this->_sections['this']['index'] - $this->_sections['this']['step'];
$this->_sections['this']['index_next'] = $this->_sections['this']['index'] + $this->_sections['this']['step'];
$this->_sections['this']['first']      = ($this->_sections['this']['iteration'] == 1);
$this->_sections['this']['last']       = ($this->_sections['this']['iteration'] == $this->_sections['this']['total']);
?>
	<?php if ((1 & $this->_sections['this']['iteration'])): ?>
	<tr class="odd">
	<?php else: ?>
	<tr>
	<?php endif; ?> 
		<td><?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_name_real']; ?>
</td>
		<td><?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_type']; ?>
</td>
		<td><?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_date_created_real']; ?>
</td>
		<td><?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_last_login_real']; ?>
 hrs)</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=user_edit&id=<?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_id']; ?>
">Edit</a> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=user_overview&reset=<?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_id']; ?>
">Reset password</a><?php if ($this->_tpl_vars['user_type'] == 'superadmin'): ?> | <a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_name_real']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=user_overview&del=<?php echo $this->_tpl_vars['users'][$this->_sections['this']['index']]['admin_id']; ?>
')">Delete</a><?php endif; ?></td>
	</tr>
<?php endfor; endif; ?>
	</tbody>
</table>
<?php else: ?>
	<p>No users are available. <em>(this should not be possible, please contact Robin Corba)</em></p>
<?php endif; ?>