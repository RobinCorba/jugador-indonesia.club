<?php
/* Smarty version 3.1.30, created on 2016-10-09 02:58:22
  from "/var/www/jugador-indonesia.club/admin/templates/site_config.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f94fde6aef41_58883115',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '36199c70c3efbfee0ac51c8ba1a81f52854cba2f' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/site_config.tpl',
      1 => 1475909522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57f94fde6aef41_58883115 (Smarty_Internal_Template $_smarty_tpl) {
?>
<h1>Site configuration</h1>
<p>Maintain the most important parts of the website</p>
<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['warning']->value) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['message']->value) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=site_config">
<table id="details" style="width:800px;">
	<tr>
		<td style="width:150px;">Site root URL</td>
		<td>
			<input type="text" name="site_url" value="<?php echo $_smarty_tpl->tpl_vars['config_data']->value['site_url'];?>
" maxlength="100" style="width:350px;"/><br />
			<em>Domain name where the website is hosted. <strong>Don't change unless you know what you're doing!</strong></em>
		</td>
	</tr>
	<tr class="odd">
		<td style="width:150px;">Site root folder</td>
		<td>
			<input type="text" name="site_root" value="<?php echo $_smarty_tpl->tpl_vars['config_data']->value['site_root'];?>
" maxlength="100" style="width:350px;"/><br />
			<em>Main folder of the website on the server. <strong>Don't change unless you know what you're doing!</strong></em>
		</td>
	</tr>
	<tr>
		<td style="width:150px;">Site title</td>
		<td>
			<input type="text" name="site_title" value="<?php echo $_smarty_tpl->tpl_vars['config_data']->value['site_title'];?>
" maxlength="50" style="width:200px;"/><br />
			<em>This is always displayed behind any page specific title</em>
		</td>
	</tr>
	<tr class="odd">
		<td style="width:150px;">Site global key words</td>
		<td>
			<input type="text" name="site_keywords" value="<?php echo $_smarty_tpl->tpl_vars['config_data']->value['site_keywords'];?>
" maxlength="96" style="width:300px;"/><br />
			<em>These are stored with page specific key words for optimized search engine results. Seperate with a comma ','</em></td>
	</tr>
	<tr>
		<td style="width:150px;">Webmaster name</td>
		<td><input type="text" name="site_webmaster_name" value="<?php echo $_smarty_tpl->tpl_vars['config_data']->value['site_webmaster_name'];?>
" maxlength="48" style="width:100px;"/></td>
	</tr>
	<tr class="odd">
		<td style="width:150px;">Webmaster<br />e-mail address</td>
		<td>
			<input type="text" name="site_webmaster_email" value="<?php echo $_smarty_tpl->tpl_vars['config_data']->value['site_webmaster_email'];?>
" maxlength="96" style="width:300px;"/><br />
			<em>Used for CMS generated e-mails (example: webmaster@publicis.com)</em></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Save changes" style="float:right;" /></td>
	</tr>
</table>
</form><?php }
}
