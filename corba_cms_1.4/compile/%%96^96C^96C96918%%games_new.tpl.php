<?php /* Smarty version 2.6.3, created on 2016-07-17 15:53:40
         compiled from games_new.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>New game results</h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post">
	<input type="hidden" name="game_id" maxlength="4" <?php if (isset ( $this->_tpl_vars['game'] )): ?>value="<?php echo $this->_tpl_vars['game']['game_id']; ?>
"<?php endif; ?> />
	<table class="details">
		<tbody>
			<tr>
				<td>Date</td>
				<td>
					<input type="text" name="game_date_day" maxlength="2" placeholder="DD" value="<?php if (isset ( $this->_tpl_vars['game'] )):  echo $this->_tpl_vars['game']['game_date_day'];  else:  echo $this->_tpl_vars['this_day'];  endif; ?>" style="width:50px;" />
					<input type="text" name="game_date_month" maxlength="2" placeholder="MM" value="<?php if (isset ( $this->_tpl_vars['game'] )):  echo $this->_tpl_vars['game']['game_date_month'];  else:  echo $this->_tpl_vars['this_month'];  endif; ?>" style="width:50px;" />
					<input type="text" name="game_date_year" maxlength="4" placeholder="YYYY" value="<?php if (isset ( $this->_tpl_vars['game'] )):  echo $this->_tpl_vars['game']['game_date_year'];  else:  echo $this->_tpl_vars['this_year'];  endif; ?>" style="width:80px;" />
				</td>
			</tr>
			<tr>
				<td>Opponent</td>
				<td>
					<select name="game_opponent">
					<?php if (count($_from = (array)($this->_tpl_vars['opponents']))):
    foreach ($_from as $this->_tpl_vars['o']):
?>
						<option value="<?php echo $this->_tpl_vars['o']['opponent_id']; ?>
" <?php if (isset ( $this->_tpl_vars['game'] ) && $this->_tpl_vars['game']['game_opponent'] == $this->_tpl_vars['o']['opponent_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['o']['opponent_name']; ?>
</option>
					<?php endforeach; unset($_from); endif; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Score</td>
				<td><input type="text" maxlength="64" name="game_score" placeholder="Final score" <?php if (isset ( $this->_tpl_vars['game'] )): ?>value="<?php echo $this->_tpl_vars['game']['game_score']; ?>
"<?php endif; ?> /></td>
			</tr>
            <tr>
				<td>Result</td>
				<td>
					<select name="game_result">
						<option value="won" <?php if (isset ( $this->_tpl_vars['game'] ) && $this->_tpl_vars['game']['game_result'] == 'won'): ?>selected<?php endif; ?>>Win</option>
						<option value="drew" <?php if (isset ( $this->_tpl_vars['game'] ) && $this->_tpl_vars['game']['game_result'] == 'drew'): ?>selected<?php endif; ?>>Draw</option>
						<option value="lost" <?php if (isset ( $this->_tpl_vars['game'] ) && $this->_tpl_vars['game']['game_result'] == 'lost'): ?>selected<?php endif; ?>>Loss</option>
					</select>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top; padding-right:25px;">Goals and <br />clean sheets </td>
				<td>
					
					<table id="goal_overview">
						<thead>
							<tr style="font-weight:bold;">
								<td>Player name (no.)</td>
								<td>Pos.</td>
								<td style="padding-right:15px;">Total goals</td>
								<td>Clean sheet?</td>
							</tr>
						</thead>
						<tbody>
						<?php if (count($_from = (array)($this->_tpl_vars['players']))):
    foreach ($_from as $this->_tpl_vars['p']):
?>
							<tr>
								<td style="padding-right:15px;"><?php echo $this->_tpl_vars['p']['player_name'];  if ($this->_tpl_vars['p']['player_no'] != 0): ?> (<?php echo $this->_tpl_vars['p']['player_no']; ?>
)<?php endif; ?></td>
								<td style="padding-right:15px;"><?php echo $this->_tpl_vars['p']['player_card_position']; ?>
</td>
								<td style="padding-right:15px;"><input type="text" name="goals[<?php echo $this->_tpl_vars['p']['player_id']; ?>
]" maxlength="1" value="<?php if (isset ( $this->_tpl_vars['game']['goals'][$this->_tpl_vars['p']['player_id']] )):  echo $this->_tpl_vars['game']['goals'][$this->_tpl_vars['p']['player_id']];  else: ?>0<?php endif; ?>" style="width:12px; text-align:center;" /></td>
                                <td>
                                    <select name="clean_sheets[<?php echo $this->_tpl_vars['p']['player_id']; ?>
]">
                                        <option value="yes"<?php if (isset ( $this->_tpl_vars['game']['clean_sheets'][$this->_tpl_vars['p']['player_id']] )): ?>selected<?php endif; ?>>Yes</option>
                                        <option value="no"<?php if (! isset ( $this->_tpl_vars['game']['clean_sheets'][$this->_tpl_vars['p']['player_id']] )): ?>selected<?php endif; ?>>No</option>
                                    </select>
                                </td>
							</tr>
						<?php endforeach; unset($_from); endif; ?>
						</tbody>
					</table>
					
				</td>
			</tr>
			<tr>
				<td coslpan="2"><input type="submit" value="Save" /></td>
			</tr>
		</tbody>
	</table>
</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>