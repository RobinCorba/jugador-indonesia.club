<?php
/* Smarty version 3.1.30, created on 2016-10-15 20:54:45
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/admin/templates/new-user.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58023525541843_80297247',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2922709c2aa47f4a90b1aa3364ff8d3bec0b25b7' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/admin/templates/new-user.tpl',
      1 => 1476539644,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_58023525541843_80297247 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Create new account</h1>
<?php if ($_smarty_tpl->tpl_vars['user_type']->value != "superadmin") {?>
	<p id="error">Whoopsy daisy, you aren't allowed to open this page.</p>
<?php } else { ?>
	<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }?>
	<?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
	<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
admin/overview" />
		<table id="details">
			<tr>
				<td>Username (to login)</td>
				<td><input type="text" name="admin_name" maxlength="20" value="" style="width:120px;" placeholder="Username" /></td>
			</tr>
            <tr>
				<td>Password (to login)</td>
				<td><input type="password" name="admin_pass" autocomplete="off" style="width:140px;" /></td>
			</tr>
            <tr>
				<td>Full name</td>
				<td><input type="text" name="admin_name_real" maxlength="32" value="" style="width:200px;" placeholder="Full name" /></td>
			</tr>
			
			
			<tr>
				<td>E-mail address<br /><em>Optional</em></td>
				<td><input type="text" name="admin_email" autocomplete="off" maxlength="128" style="width:200px;" placeholder="E-mail address" /></td>
			</tr>
			<tr>
				<td>Account type</td>
				<td>
					<select name="admin_type">
						<option value="admin">Standard admin</option>
						<option value="superadmin">Super administrator</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:10px;"><input type="submit" name="save_type" value="Add user" style="float:right;"/></td>
			</tr>
		</table>
	</form>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
