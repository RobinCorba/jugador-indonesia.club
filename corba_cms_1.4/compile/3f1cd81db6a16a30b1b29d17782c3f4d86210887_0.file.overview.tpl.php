<?php
/* Smarty version 3.1.30, created on 2016-10-16 00:01:29
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/football/templates/overview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_580260e95acb44_35148623',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3f1cd81db6a16a30b1b29d17782c3f4d86210887' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/football/templates/overview.tpl',
      1 => 1476550427,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:datatable.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_580260e95acb44_35148623 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:datatable.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Footbal Management</h1>
<p>Manage your games, opponents, goals, clean sheets, and more!</p>
<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<div id="actions">
	<a id="new" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/new-game">Add game</a>
    <a id="new" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/new-opponent">Add opponent</a>
    <a id="new" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/new-player">Add player</a>
</div>

<h2>Game History</h2>
<?php if (empty($_smarty_tpl->tpl_vars['games']->value)) {?>
<p id="warning">No games registered.</p>
<?php } else { ?>
<table class="grid display">
    <thead>
    <tr>
        <td class="title" style="width:200px;">Date</td>
        <td class="title" style="width:200px;">Opponent</td>
        <td class="title" style="width:200px;">Score</td>
        <td class="title" style="width:200px;">Result</td>
        <td class="title" style="width:100px;">&nbsp;</td>
    </tr>
    </thead>
    <tbody>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['games']->value, 'g');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['g']->value) {
?>
        <td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_date_year'];?>
-<?php echo $_smarty_tpl->tpl_vars['g']->value['game_date_month'];?>
-<?php echo $_smarty_tpl->tpl_vars['g']->value['game_date_day'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_opponent']['name'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_score'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_result'];?>
</td>
        <td><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/edit-game/<?php echo $_smarty_tpl->tpl_vars['g']->value['game_id'];?>
">Edit game</a></td>
    </tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </tbody>
</table>
<?php }?>

<h2>Players</h2>
<?php if (empty($_smarty_tpl->tpl_vars['players']->value)) {?>
<p id="warning">No players registered.</p>
<?php } else { ?>
<table class="grid display">
    <thead>
        <tr>
            <td class="title" style="width:200px;">Name</td>
            <td class="title" style="width:200px;">Full name</td>
            <td class="title" style="width:200px;">Number</td>
            <td class="title" style="width:200px;">Position</td>
            <td class="title" style="width:200px;">Status</td>
            <td class="title" style="width:100px;">&nbsp;</td>
        </tr>
    </thead>
    <tbody>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['players']->value, 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
        <td class="table_pic" style="background-image:url('<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
media/images/player_cards/<?php echo $_smarty_tpl->tpl_vars['p']->value['card_picture'];?>
');"><?php echo $_smarty_tpl->tpl_vars['p']->value['display_name'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['p']->value['name'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['p']->value['no'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['p']->value['card_position'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['p']->value['status'];?>
</td>
        <td><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/edit-player/<?php echo $_smarty_tpl->tpl_vars['p']->value['id'];?>
">Edit</a></td>
    </tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </tbody>
</table>
<?php }?>

<h2>Opponents</h2>
<?php if (empty($_smarty_tpl->tpl_vars['opponents']->value)) {?>
<p id="warning">No opponents registered.</p>
<?php } else { ?>
<table class="grid display">
    <thead>
    <tr>
        <td class="title" style="width:200px;">Opponent name</td>
        <td class="title" style="width:200px;">Jersey colour</td>
        <td class="title" style="width:100px;">&nbsp;</td>
    </tr>
    </thead>
    <tbody>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['opponents']->value, 'o');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['o']->value) {
?>
        <td><?php echo $_smarty_tpl->tpl_vars['o']->value['name'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['o']->value['color'];?>
</td>
        <td><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/edit-opponent/<?php echo $_smarty_tpl->tpl_vars['o']->value['id'];?>
">Edit</a></td>
    </tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

    </tbody>
</table>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
