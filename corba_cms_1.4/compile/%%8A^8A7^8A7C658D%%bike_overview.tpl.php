<?php /* Smarty version 2.6.3, created on 2013-02-18 02:14:20
         compiled from bike_overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'bike_overview.tpl', 24, false),)), $this); ?>
<h1>Motorcycle Management</h1>
<p>An overview of all motorcycles.</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=bike_new">New motorcycle</a>
</div>
<?php if ($this->_tpl_vars['motorcycles'] == ""): ?>
	<p id="warning">Warning, no motorcycles are available.</p>
<?php else: ?>
	<table id="grid" class="display">
		<thead>
		<tr>
			<td>Motor name</td>
			<td>Brand name</td>
			<td>Added</td>
			<td>Price</td>
			<td class="no_sort">&nbsp;</td>
		</tr>
		</thead>
		<tbody>
	<?php if (count($_from = (array)($this->_tpl_vars['motorcycles']))):
    foreach ($_from as $this->_tpl_vars['bike']):
?>
		<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
			<td><?php echo $this->_tpl_vars['bike']['motor_name']; ?>
</td>
			<td><?php echo $this->_tpl_vars['bike']['motor_brand_name']; ?>
</td>
			<td><?php echo $this->_tpl_vars['bike']['motor_date_added']; ?>
</td>
			<td><?php echo $this->_tpl_vars['bike']['motor_currency']; ?>
 <?php echo $this->_tpl_vars['bike']['motor_price']; ?>
</td>
			<td>
				<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=bike_edit&id=<?php echo $this->_tpl_vars['bike']['motor_id']; ?>
">Edit</a> | 
				<a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['bike']['motor_name']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=bike_overview&del=<?php echo $this->_tpl_vars['bike']['motor_id']; ?>
');">Delete</a>
				<?php if ($this->_tpl_vars['page']['total_subs'] != 0): ?> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=page_sub_overview&id=<?php echo $this->_tpl_vars['page']['id']; ?>
">Manage subpages</a><?php endif; ?>
			</td>
		</tr>
	<?php endforeach; unset($_from); endif; ?>
		</tbody>
	</table>
<?php endif; ?>