<?php
/* Smarty version 3.1.30, created on 2016-10-09 12:02:15
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/games/templates/overview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f9cf572ac689_00398475',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f988027abc17ee8a3d81ca5de17f2e64c8476cb' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/games/templates/overview.tpl',
      1 => 1475989334,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:datatable.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f9cf572ac689_00398475 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:datatable.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Game History</h1>
<p>All recorded games.</p>
<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<div id="actions">
	<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
games.new">Add game results</a>
</div>
<?php if (!empty($_smarty_tpl->tpl_vars['games']->value)) {?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Date</td>
		<td class="title" style="width:200px;">Opponent</td>
		<td class="title" style="width:200px;">Score</td>
		<td class="title" style="width:200px;">Result</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['games']->value), 'g');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['g']->value) {
?>
		<td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_date_year'];?>
-<?php echo $_smarty_tpl->tpl_vars['g']->value['game_date_month'];?>
-<?php echo $_smarty_tpl->tpl_vars['g']->value['game_date_day'];?>
</td>
		<td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_opponent']['opponent_name'];?>
</td>
		<td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_score'];?>
</td>
		<td><?php echo $_smarty_tpl->tpl_vars['g']->value['game_result'];?>
</td>
		<td><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=game_edit&id=<?php echo $_smarty_tpl->tpl_vars['g']->value['game_id'];?>
">Edit</a></td>
	</tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	</tbody>
</table>
<?php } else { ?>
	<p>No games available.</p>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
