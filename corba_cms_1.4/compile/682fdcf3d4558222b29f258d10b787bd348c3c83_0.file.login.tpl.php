<?php
/* Smarty version 3.1.30, created on 2016-10-08 13:47:14
  from "/var/www/jugador-indonesia.club/admin/templates/login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f896721fae23_30925001',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '682fdcf3d4558222b29f258d10b787bd348c3c83' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/login.tpl',
      1 => 1475909232,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57f896721fae23_30925001 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Administration login</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="<?php if ($_smarty_tpl->tpl_vars['seo_desc']->value) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;?>
, <?php }?>Publicis S.E. Asia" />
		<meta name="keywords" content="Publicis, Modem, South East Asia, Indonesia, Singapore, Thailand, Phillipines" />
		<meta name="robots" content="follow,index" />
		<meta name="copyright" content="Robin Corba 2012-2016" />
		
		<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
favicon.ico" />
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
templates/css/style_login.css" />
	</head>
	<body>
		<img id="header_login" src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
templates/img/corba_cms.jpg" alt="Corba CMS version April '13" />
		<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
home">
			<div id="login_panel">
				<h1>Login</h1>
				<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }?>
				<?php if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }?>
				<?php if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
				<table>
					<tr>
						<td>Username</td>
						<td><input type="text" name="user_name" style="width:160px;" /></td>
					</tr>
					<tr>
						<td>Password</td>
						<td><input type="password" name="user_pass" style="width:160px;" /></td>
					</tr>
					<tr>
						<td colspan="2" id="submit"><input type="submit"  value="Login" /></td>
					</tr>
				</table>
			</div>
		</form>
		<div id="copyright">
			<a>Developed by Robin Corba 2010 - 2016 &copy;</a>
		</div>
	</body>
</html><?php }
}
