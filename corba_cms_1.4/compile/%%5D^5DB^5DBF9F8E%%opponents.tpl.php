<?php /* Smarty version 2.6.3, created on 2016-07-17 15:52:54
         compiled from opponents.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>Opponent teams</h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=opponents_new">Add opponent</a>
</div>
<?php if ($this->_tpl_vars['opponents'] != ""): ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Name</td>
		<td class="title" style="width:200px;">Color</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['opponents']))):
    foreach ($_from as $this->_tpl_vars['o']):
?>
		<td><?php echo $this->_tpl_vars['o']['opponent_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['o']['opponent_color']; ?>
</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=opponents_edit&id=<?php echo $this->_tpl_vars['o']['opponent_id']; ?>
">Edit</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<?php else: ?>
	<p>No opponents available.</p>
<?php endif;  $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>