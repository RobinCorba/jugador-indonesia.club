<?php /* Smarty version 2.6.3, created on 2013-11-10 12:19:19
         compiled from product_overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'product_overview.tpl', 23, false),)), $this); ?>
<h1>Products overview</h1>
<p>Manage all products</p>
<div id="actions">
	<img src="<?php echo $this->_tpl_vars['base_dir']; ?>
templates/img/icons/drink.png" alt="" /> <a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_new">New product</a>
	<img src="<?php echo $this->_tpl_vars['base_dir']; ?>
templates/img/icons/package.png" alt="" /> <a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_categories_overview">Manage categories</a>
</div>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>

<?php if (! empty ( $this->_tpl_vars['products'] )): ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:300px;">Category</td>
		<td style="width:200px;">Product Name</td>
		<td style="width:200px;">Price</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['products']))):
    foreach ($_from as $this->_tpl_vars['c']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td><?php echo $this->_tpl_vars['c']['category_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['c']['product_name']; ?>
</td>
		<td>Rp. <?php echo $this->_tpl_vars['c']['product_price']; ?>
</td>
		<td>
			<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_edit&id=<?php echo $this->_tpl_vars['c']['product_id']; ?>
">Edit</a> | 
			<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_overview&del=<?php echo $this->_tpl_vars['c']['product_id']; ?>
" onclick="return confirm('Are you sure you want to delete <?php echo $this->_tpl_vars['c']['product_name']; ?>
?');">Delete</a>
		</td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<?php else: ?>
	<p id="warning">No products exist yet</p>
<?php endif; ?>