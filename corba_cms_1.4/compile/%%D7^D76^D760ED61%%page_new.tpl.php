<?php /* Smarty version 2.6.3, created on 2013-04-14 22:47:07
         compiled from page_new.tpl */ ?>
<h1>New page</h1>
<p>Create a new page</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=page_new">
<table id="details" style="width:800px; border-bottom:0px;">
	<tr class="odd">
		<td style="width:200px;">Page title</td>
		<td>
			<input type="text" name="page_title" maxlength="64" value="<?php echo $this->_tpl_vars['page']['page_title']; ?>
" style="width:450px;" /><br />
			<em>Will also be shown in the browser tab and search engines.</em>
		</td>
	</tr>
	<tr>
		<td>Navigation title</td>
		<td>
			<input type="text" name="page_nav_title" maxlength="48" value="<?php echo $this->_tpl_vars['page']['page_nav_title']; ?>
" style="width:100px;" /><br />
			<em>This name will be showed in the navigation panel to link to the page.</em>
		</td>
	</tr>
	<tr class="odd">
		<td>Parent page</td>
		<td>
			<select name="page_parent_id">
				<option value="0" <?php if ($this->_tpl_vars['page']['page_parent_id'] == 0): ?>selected<?php endif; ?>>No parent page</option>
				<?php if (count($_from = (array)($this->_tpl_vars['pages']))):
    foreach ($_from as $this->_tpl_vars['parent_page']):
?>
				<?php if ($this->_tpl_vars['parent_page']['id'] != $this->_tpl_vars['page']['id']): ?><option value="<?php echo $this->_tpl_vars['parent_page']['id']; ?>
" <?php if ($this->_tpl_vars['page']['page_parent_id'] == $this->_tpl_vars['parent_page']['id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['parent_page']['page_title']; ?>
</option><?php endif; ?>
				<?php endforeach; unset($_from); endif; ?>
			</select><br />
			<em>Selecting a parent page will make this a subpage</em>
		</td>
	</tr>
	<tr>
		<td>Use a script?</td>
		<td>
			<select name="page_script" id="page_script" onchange="javascript:checkCustomPage();">
				<?php if (count($_from = (array)($this->_tpl_vars['scripts']))):
    foreach ($_from as $this->_tpl_vars['s']):
?>
				<option value="<?php echo $this->_tpl_vars['s']['script']; ?>
" <?php if ($this->_tpl_vars['page']['page_script'] == ($this->_tpl_vars['s']).".script"): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['s']['name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
				<option value="" <?php if ($this->_tpl_vars['page']['page_script'] == ""): ?>selected<?php endif; ?>>No page script..</option>
			</select><br />
			<em>Scripts are special pages like a registration form, photo gallery etc. They don't need text content</em>
		</td>
	</tr>
	<tr class="odd">
		<td style="width:200px;">Nice URL (optional)</td>
		<td>
			<?php echo $this->_tpl_vars['site_dir']; ?>
<input type="text" name="page_url" maxlength="10" value="<?php echo $this->_tpl_vars['page']['page_url']; ?>
" style="width:65px;" /><br />
			<em>A good looking link will make this page easier to access</em>
		</td>
	</tr>
	<tr>
		<td>Description (optional)</td>
		<td colspan="2">
			<input type="text" name="page_desc" style="width:500px;" value="<?php echo $this->_tpl_vars['page']['page_desc']; ?>
"/><br />
			<em>This can be used by search engines to display search results.</em>
		</td>
	</tr>
	<tr class="odd">
		<td>Keywords (optional)</td>
		<td colspan="2">
			<input type="text" name="page_tags" maxlength="256" style="width:400px;" value="<?php echo $this->_tpl_vars['page']['page_tags']; ?>
" /><br />
			<em>This will improve search results, separate keywords with a comma ','</em>
		</td>
	</tr>
</table>
<div id="custom_page">
	<table id="details" style="width:800px; border-top:0px; border-bottom:0px;">
		<tr class="odd">
			<td colspan="2">
				<textarea name="page_content" style="width:775px; height:600px;"><?php echo $this->_tpl_vars['page']['page_content']; ?>
</textarea>
			</td>
		</tr>
	</table>
</div>
<table id="details" style="width:800px; border-top:0px;">
	<tr>
		<td><input type="submit" value="Save new page" style="float:right;"/></td>
	</tr>
</table>
</form>