<?php /* Smarty version 2.6.3, created on 2012-07-04 16:30:42
         compiled from products.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'products.tpl', 20, false),)), $this); ?>
<h1>Products</h1>
<p>Manage Products for the Pizza Hut Indonesia website</p>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_new">New Products</a>
</div>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:300px;">Preview</td>
		<td style="width:200px;">Product Name</td>
		<td style="width:200px;">Category</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['products']))):
    foreach ($_from as $this->_tpl_vars['row']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td><img src="<?php echo $this->_tpl_vars['site_dir']; ?>
media/images/thumb_<?php echo $this->_tpl_vars['row']['img_url']; ?>
" /></td>
		<td><?php echo $this->_tpl_vars['row']['prod_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['row']['prodcat_name'];  if ($this->_tpl_vars['row']['subcat_name'] != ''): ?> [<?php echo $this->_tpl_vars['row']['subcat_name']; ?>
]<?php endif; ?></td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_edit&id=<?php echo $this->_tpl_vars['row']['prod_id']; ?>
">Edit</a> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=products&del=<?php echo $this->_tpl_vars['row']['prod_id']; ?>
" onclick="return sure()">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>