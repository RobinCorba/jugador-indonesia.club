<?php /* Smarty version 2.6.3, created on 2013-04-12 00:08:19
         compiled from site_config.tpl */ ?>
<h1>Site configuration</h1>
<p>Maintain the most important parts of the website</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=site_config">
<table id="details" style="width:800px;">
	<tr>
		<td style="width:150px;">Site root URL</td>
		<td>
			<input type="text" name="site_url" value="<?php echo $this->_tpl_vars['config_data']['site_url']; ?>
" maxlength="100" style="width:350px;"/><br />
			<em>Domain name where the website is hosted. <strong>Don't change unless you know what you're doing!</strong></em>
		</td>
	</tr>
	<tr class="odd">
		<td style="width:150px;">Site root folder</td>
		<td>
			<input type="text" name="site_root" value="<?php echo $this->_tpl_vars['config_data']['site_root']; ?>
" maxlength="100" style="width:350px;"/><br />
			<em>Main folder of the website on the server. <strong>Don't change unless you know what you're doing!</strong></em>
		</td>
	</tr>
	<tr>
		<td style="width:150px;">Site title</td>
		<td>
			<input type="text" name="site_title" value="<?php echo $this->_tpl_vars['config_data']['site_title']; ?>
" maxlength="50" style="width:200px;"/><br />
			<em>This is always displayed behind any page specific title</em>
		</td>
	</tr>
	<tr class="odd">
		<td style="width:150px;">Site global key words</td>
		<td>
			<input type="text" name="site_keywords" value="<?php echo $this->_tpl_vars['config_data']['site_keywords']; ?>
" maxlength="96" style="width:300px;"/><br />
			<em>These are stored with page specific key words for optimized search engine results. Seperate with a comma ','</em></td>
	</tr>
	<tr>
		<td style="width:150px;">Webmaster name</td>
		<td><input type="text" name="site_webmaster_name" value="<?php echo $this->_tpl_vars['config_data']['site_webmaster_name']; ?>
" maxlength="48" style="width:100px;"/></td>
	</tr>
	<tr class="odd">
		<td style="width:150px;">Webmaster<br />e-mail address</td>
		<td>
			<input type="text" name="site_webmaster_email" value="<?php echo $this->_tpl_vars['config_data']['site_webmaster_email']; ?>
" maxlength="96" style="width:300px;"/><br />
			<em>Used for CMS generated e-mails (example: webmaster@publicis.com)</em></td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Save changes" style="float:right;" /></td>
	</tr>
</table>
</form>