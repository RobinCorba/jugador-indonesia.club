<?php
/* Smarty version 3.1.30, created on 2016-10-09 02:58:07
  from "/var/www/jugador-indonesia.club/admin/templates/opponents_new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f94fcfe34308_12154743',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6efacabf6591432ab6b49abd0176ee75264889d8' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/opponents_new.tpl',
      1 => 1475909522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f94fcfe34308_12154743 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Add opponent</h1>
<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['warning']->value) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['message']->value) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<form method="post">
	<input type="hidden" name="opponent_id" maxlength="4" <?php if (isset($_smarty_tpl->tpl_vars['opponent']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['opponent']->value['opponent_id'];?>
"<?php }?> />
	<table class="details">
		<tbody>
			<tr>
				<td>Opponent name</td>
				<td><input type="text" name="opponent_name" maxlength="128" placeholder="Name of the opponent team" <?php if (isset($_smarty_tpl->tpl_vars['opponent']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['opponent']->value['opponent_name'];?>
"<?php }?> /></td>
			</tr>
			<tr>
				<td>Opponent color</td>
				<td><input type="text" name="opponent_color" maxlength="64" placeholder="Color of the opponent team's jersey" <?php if (isset($_smarty_tpl->tpl_vars['opponent']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['opponent']->value['opponent_color'];?>
"<?php }?> /></td>
			</tr>
			<tr>
				<td coslpan="2"><input type="submit" value="Save" /></td>
			</tr>
		</tbody>
	</table>
</form>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
