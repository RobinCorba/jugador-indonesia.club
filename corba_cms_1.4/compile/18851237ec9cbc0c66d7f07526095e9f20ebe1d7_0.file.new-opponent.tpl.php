<?php
/* Smarty version 3.1.30, created on 2016-10-15 15:03:52
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/football/templates/new-opponent.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5801e2e8206db6_83009450',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '18851237ec9cbc0c66d7f07526095e9f20ebe1d7' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/football/templates/new-opponent.tpl',
      1 => 1475997231,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5801e2e8206db6_83009450 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Add Opponent</h1>
<p>Save a new opponent.</p>
<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football/overview">
	<input type="hidden" name="opponent_id" maxlength="4" <?php if (isset($_smarty_tpl->tpl_vars['opponent']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['opponent']->value['id'];?>
"<?php }?> />
	<table class="details">
		<tbody>
			<tr>
				<td>Opponent name</td>
				<td><input type="text" name="opponent_name" maxlength="128" placeholder="Name of the opponent team" <?php if (isset($_smarty_tpl->tpl_vars['opponent']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['opponent']->value['name'];?>
"<?php }?> /></td>
			</tr>
			<tr>
				<td style="padding-right:25px;">Opponent jersey colour</td>
				<td><input type="text" name="opponent_color" maxlength="64" placeholder="Jersey colour" <?php if (isset($_smarty_tpl->tpl_vars['opponent']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['opponent']->value['color'];?>
"<?php }?> /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" name="save_type" value="Add opponent" style="float:right; margin-top:10px;" /></td>
			</tr>
		</tbody>
	</table>
</form>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
