<?php
/* Smarty version 3.1.30, created on 2016-10-08 13:49:19
  from "/var/www/jugador-indonesia.club/admin_may_2015/modules/news/templates/overview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f896ef7571b9_52417725',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c78b06e11b9990c359cd76fd503af312c13d4598' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin_may_2015/modules/news/templates/overview.tpl',
      1 => 1468252598,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:datatable.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f896ef7571b9_52417725 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_function_cycle')) require_once '/var/www/jugador-indonesia.club/admin_may_2015/library/smarty_3.1.30/plugins/function.cycle.php';
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:datatable.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<h1>News Management</h1>
<p>An overview of all articles.</p>
<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<div id="actions">
	<a id="new" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news/new">New article</a>
</div>
<?php if ($_smarty_tpl->tpl_vars['articles']->value == '') {?>
	<p>No articles written yet.</p>
<?php } else { ?>
	<table id="grid" class="display">
		<thead>
		<tr>
			<td>Article title</td>
			<td>Type</td>
			<td>Published</td>
			<td>Status</td>
			<td>&nbsp;</td>
		</tr>
		</thead>
		<tbody>
	<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['articles']->value, 'article');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['article']->value) {
?>
		<tr <?php echo smarty_function_cycle(array('values'=>',class="odd"'),$_smarty_tpl);?>
>
			<td style="width:500px;"><?php echo $_smarty_tpl->tpl_vars['article']->value['post_title_shortened'];?>
</td>
			<td style="width:80px;"><?php echo $_smarty_tpl->tpl_vars['article']->value['post_type'];?>
</td>
			<td style="width:220px;"><?php echo $_smarty_tpl->tpl_vars['article']->value['post_date_published_converted'];?>
 <?php echo $_smarty_tpl->tpl_vars['article']->value['post_time_published_real'];?>
 hrs</td>
			<td style="width:60px;"><?php echo $_smarty_tpl->tpl_vars['article']->value['post_status'];?>
</td>
			<td style="width:80px;"><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news/edit/<?php echo $_smarty_tpl->tpl_vars['article']->value['post_id'];?>
">Edit</a> | <a href="javascript:confirmDelete('<?php echo $_smarty_tpl->tpl_vars['article']->value['post_title'];?>
','<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news/overview/delete/<?php echo $_smarty_tpl->tpl_vars['article']->value['post_id'];?>
')">Delete</a></td>
		</tr>
	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</tbody>
	</table>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
