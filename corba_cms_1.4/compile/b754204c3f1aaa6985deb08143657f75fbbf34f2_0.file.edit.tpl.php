<?php
/* Smarty version 3.1.30, created on 2016-10-09 01:17:12
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/news/templates/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f938286b4d62_89198426',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b754204c3f1aaa6985deb08143657f75fbbf34f2' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/news/templates/edit.tpl',
      1 => 1475950624,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f938286b4d62_89198426 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/tinymce/tiny_mce.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
// TinyMCE
tinyMCE.init
({
    mode:                           "textareas",
    theme:                          "advanced",
    plugins:                        "paste",
    theme_advanced_buttons1:        "bold, italic, underline, strikethrough, sub, sup, justifyleft, justifycenter, justifyright, justifyfull",
    theme_advanced_buttons2:        "numlist, bullist, outdent, indent, link, fontsizeselect",
    theme_advanced_buttons3:        "cut, copy, paste, code",
    /*content_css:                  "templates/style_tinymce.css",*/
    relative_urls:                  false,
    remove_script_host:             false,
    paste_auto_cleanup_on_paste:    true,
    paste_preprocess:
    function(pl,o)
    {
        // Content string containing the HTML from the clipboard
        o.content = o.content;
    },
    paste_postprocess:
    function(pl,o)
    {
        // Content DOM node containing the DOM structure of the clipboard
        o.node.innerHTML = o.node.innerHTML;
    }
});
<?php echo '</script'; ?>
>

<h1>Edit article</h1>
<p>Make changes to an article of the news section</p>
<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<form enctype="multipart/form-data" method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news/overview">
<input type="hidden" name="article_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['id'];?>
" />
<table id="details" style="width:800px;">
	<tr>
		<td>Article title</td>
		<td>
			<input type="text" name="article_title" maxlength="128" style="width:350px;" <?php if (!empty($_smarty_tpl->tpl_vars['data']->value['title'])) {?>value="<?php echo $_smarty_tpl->tpl_vars['data']->value['title'];?>
"<?php }?> /><br />
			<em>Appears on top of the page, browser tab and search engines.</em>
		</td>
	</tr>
	<tr>
		<td>Picture</td>
		<td>
            <img id="img_upload_preview_post" src="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
media/images/news/thumb_600_<?php echo $_smarty_tpl->tpl_vars['data']->value['picture'];?>
" alt="" /></div>
			<input id="img_upload_post" name="article_picture" type="file" accept="image/*" /><br />
			<em>Minimum size 1200 x 800 pixels (max. 1 MB)</em>
		</td>
	</tr>
	<tr>
		<td colspan="2">Article content</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="article_content" style="width:800px; height:600px;"><?php if (!empty($_smarty_tpl->tpl_vars['data']->value['content'])) {
echo $_smarty_tpl->tpl_vars['data']->value['content'];
}?></textarea>
		</td>
	</tr>
	<tr>
		<td>Summary (or description)</td>
		<td>
			<input type="text" name="article_description" style="width:500px;" value="<?php if (!empty($_smarty_tpl->tpl_vars['data']->value['description'])) {
echo $_smarty_tpl->tpl_vars['data']->value['description'];
}?>" /><br />
			<em>A paragraph; This will help search engines (Google, Bing etc.) to find this article.</em>
		</td>
	</tr>
	<tr>
		<td>Tags</td>
		<td>
			<input type="text" name="article_keywords" maxlength="256" style="width:400px;" value="<?php if (!empty($_smarty_tpl->tpl_vars['data']->value['tags'])) {
echo $_smarty_tpl->tpl_vars['data']->value['tags'];
}?>" /><br />
			<em>Separate tags with a comma ','. This will improve search results and connecting to related articles</em>
		</td>
	</tr> 
	<tr>
		<td colspan="2"><input type="submit" name="save_type" value="Save changes" style="float:right;" /></td>
	</tr>
</table>
</form>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
