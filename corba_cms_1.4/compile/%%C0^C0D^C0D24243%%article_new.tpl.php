<?php /* Smarty version 2.6.3, created on 2016-04-09 17:53:42
         compiled from article_new.tpl */ ?>
<h1>Create new article</h1>
<p>Write a new article for the news section</p>
<?php if (! empty ( $this->_tpl_vars['error'] )): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if (! empty ( $this->_tpl_vars['warning'] )): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if (! empty ( $this->_tpl_vars['message'] )): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form enctype="multipart/form-data" method="post" action="<?php echo $this->_tpl_vars['base_url']; ?>
?p=article_new">
<table id="details" style="width:800px;">
	<tr class="odd">
		<td style="width:220px;">Category</td>
		<td>
			<select name="article_category">
				<?php if (count($_from = (array)$this->_tpl_vars['categories'])):
    foreach ($_from as $this->_tpl_vars['category']):
?>
				<option value="<?php echo $this->_tpl_vars['category']['category_id']; ?>
" <?php if (! empty ( $this->_tpl_vars['data']['category_id'] ) && $this->_tpl_vars['data']['category_id'] == $this->_tpl_vars['category']['category_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['category']['category_title']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Article title</td>
		<td>
			<input type="text" name="article_title" maxlength="128" style="width:350px;" <?php if (! empty ( $this->_tpl_vars['data']['article_title'] )): ?>value="<?php echo $this->_tpl_vars['data']['article_title']; ?>
"<?php endif; ?> /><br />
			<em>Appears on top of the page, browser tab and search engines.</em>
		</td>
	</tr>
	<tr class="odd">
		<td>Picture</td>
		<td>
			<input name="article_picture" type="file" accept="image/*" /><br />
			<em>Minimum size 340 x 280 pixels (max. 2MB)</em>
		</td>
	</tr>
	<tr>
		<td colspan="2">Article content</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="article_content" style="width:800px; height:600px;"><?php if (! empty ( $this->_tpl_vars['data']['article_content'] )):  echo $this->_tpl_vars['data']['article_content'];  endif; ?></textarea>
		</td>
	</tr>
	<tr class="odd">
		<td>Summary (or description)</td>
		<td>
			<input type="text" name="article_description" style="width:500px;" value="<?php if (! empty ( $this->_tpl_vars['data']['article_description'] )):  echo $this->_tpl_vars['data']['article_description'];  endif; ?>" /><br />
			<em>A paragraph; This will help search engines (Google, Bing etc.) to find this article.</em>
		</td>
	</tr>
	<tr>
		<td>Tags</td>
		<td>
			<input type="text" name="article_keywords" maxlength="256" style="width:400px;" value="<?php if (! empty ( $this->_tpl_vars['data']['article_keywords'] )):  echo $this->_tpl_vars['data']['article_keywords'];  endif; ?>" /><br />
			<em>Separate tags with a comma ','. This will improve search results and connecting to related articles</em>
		</td>
	</tr> 
	<tr class="odd">
		<td><input type="submit" name="draft" value="Save draft" /></td>
		<td><input type="submit" name="publish" value="Publish article" style="float:right;" /></td>
	</tr>
</table>
</form>