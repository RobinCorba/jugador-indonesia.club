<?php /* Smarty version 2.6.3, created on 2016-08-27 16:45:44
         compiled from players_new.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1><?php if (isset ( $this->_tpl_vars['edit'] ) && $this->_tpl_vars['edit'] == 'yes'): ?>Edit <?php echo $this->_tpl_vars['player']['player_name'];  else: ?> Add new player<?php endif; ?></h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<link rel="stylesheet" type="text/css" href="../css/cards.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<form method="post" enctype='multipart/form-data'>
	<input type="hidden" name="player_id" maxlength="4" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_id']; ?>
"<?php endif; ?> />
	<table class="details">
		<tbody>
			<tr>
				<td>Full Name</td>
				<td><input type="text" name="player_name" maxlength="128" placeholder="Name of the player" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_name']; ?>
"<?php endif; ?> /></td>
                <td rowspan="6" style="vertical-align:top;">
                    Card
                    <div class="player_card <?php echo $this->_tpl_vars['player']['player_status']; ?>
">
                        <div class="number"><?php echo $this->_tpl_vars['player']['player_no']; ?>
</div>
                        <div class="position"><input type="text" name="player_card_position" maxlength="5" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_card_position']; ?>
"<?php endif; ?> style="background:none; font-size:18px; text-align:center; width:50px;" /></div>
                        <div class="name"><?php echo $this->_tpl_vars['player']['player_display_name']; ?>
</div>
                        <div class="stats" id="stat-1">
                            <span>Lorem</span>
                            <span>Ipsum</span>
                        </div>
                        <div class="stats" id="stat-2">
                            <span>Lorem</span>
                            <span>Ipsum</span>
                        </div>
                        <div class="stats" id="stat-3">
                            <span>Lorem</span>
                            <span>Ipsum</span>
                        </div>
                        <div class="stats-footer"><?php echo $this->_tpl_vars['player']['player_status_readable']; ?>
</div>
                        <div id="img_card_upload_preview" class="picture" style="background-image:url('../media/images/player_cards/<?php if (isset ( $this->_tpl_vars['player'] )):  echo $this->_tpl_vars['player']['player_card_picture'];  else: ?>default_card_picture.png<?php endif; ?>');"></div>
                    </div>
                </td>
			</tr>
            <tr>
                <td>Display Name</td>
				<td><input type="text" name="player_display_name" maxlength="128" placeholder="Displated name of the player" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_display_name']; ?>
"<?php endif; ?> /></td>
            </tr>
			<tr>
				<td>No.</td>
				<td><input type="text" name="player_no" maxlength="4" placeholder="Number of the player" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_no']; ?>
"<?php endif; ?> style="width:20px; text-align:center;" /></td>
			</tr>
			<tr>
				<td>Position(s)</td>
				<td>
					<select name="player_positions[]" multiple style="height:224px;">
					<?php if (count($_from = (array)($this->_tpl_vars['positions']))):
    foreach ($_from as $this->_tpl_vars['p']):
?>
						<option value="<?php echo $this->_tpl_vars['p']['position_id']; ?>
"
						<?php if (count($_from = (array)$this->_tpl_vars['player']['player_positions'])):
    foreach ($_from as $this->_tpl_vars['pos']):
?>
						<?php if ($this->_tpl_vars['pos']['position_id'] == $this->_tpl_vars['p']['position_id']): ?>selected<?php endif; ?>
						<?php endforeach; unset($_from); endif; ?>
						><?php echo $this->_tpl_vars['p']['position_code']; ?>
</option>
					<?php endforeach; unset($_from); endif; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
					<select name="player_status">
                        <option value="former" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_status'] == 'former'): ?>selected<?php endif; ?>>Former player</option>
                        <option value="inactive" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_status'] == 'inactive'): ?>selected<?php endif; ?>>Inactive player</option>
						<option value="new" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_status'] == 'new'): ?>selected<?php endif; ?>>New player</option>
						<option value="active" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_status'] == 'active'): ?>selected<?php endif; ?>>Active player</option>
						<option value="top" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_status'] == 'top'): ?>selected<?php endif; ?>>Top player</option>
                        <option value="legendary" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_status'] == 'legendary'): ?>selected<?php endif; ?>>Legendary player</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Playing since</td>
				<td>
					<select name="player_since_month">
						<option value="01" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '01'): ?>selected<?php endif; ?>>Jan.</option>
						<option value="02" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '02'): ?>selected<?php endif; ?>>Feb.</option>
						<option value="03" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '03'): ?>selected<?php endif; ?>>Mar.</option>
						<option value="04" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '04'): ?>selected<?php endif; ?>>Apr.</option>
						<option value="05" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '05'): ?>selected<?php endif; ?>>May.</option>
						<option value="06" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '06'): ?>selected<?php endif; ?>>Jun.</option>
						<option value="07" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '07'): ?>selected<?php endif; ?>>Jul.</option>
						<option value="08" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '08'): ?>selected<?php endif; ?>>Aug.</option>
						<option value="09" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '09'): ?>selected<?php endif; ?>>Sep.</option>
						<option value="10" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '10'): ?>selected<?php endif; ?>>Oct.</option>
						<option value="11" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '11'): ?>selected<?php endif; ?>>Nov.</option>
						<option value="12" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_since_month'] == '12'): ?>selected<?php endif; ?>>Dec.</option>
					</select>
					<input type="text" maxlength="4" name="player_since_year" placeholder="Year" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_since_year']; ?>
"<?php endif; ?> style="text-align:center; width:40px;" />
				</td>
			</tr>
            <tr>
				<td>Retired on (if former player)</td>
				<td>
					<select name="player_stopped_month">
						<option value="01" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '01'): ?>selected<?php endif; ?>>Jan.</option>
						<option value="02" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '02'): ?>selected<?php endif; ?>>Feb.</option>
						<option value="03" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '03'): ?>selected<?php endif; ?>>Mar.</option>
						<option value="04" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '04'): ?>selected<?php endif; ?>>Apr.</option>
						<option value="05" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '05'): ?>selected<?php endif; ?>>May.</option>
						<option value="06" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '06'): ?>selected<?php endif; ?>>Jun.</option>
						<option value="07" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '07'): ?>selected<?php endif; ?>>Jul.</option>
						<option value="08" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '08'): ?>selected<?php endif; ?>>Aug.</option>
						<option value="09" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '09'): ?>selected<?php endif; ?>>Sep.</option>
						<option value="10" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '10'): ?>selected<?php endif; ?>>Oct.</option>
						<option value="11" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '11'): ?>selected<?php endif; ?>>Nov.</option>
						<option value="12" <?php if (isset ( $this->_tpl_vars['player'] ) && $this->_tpl_vars['player']['player_stopped_month'] == '12'): ?>selected<?php endif; ?>>Dec.</option>
					</select>
					<input type="text" maxlength="4" name="player_stopped_year" placeholder="YYYY" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_stopped_year']; ?>
"<?php endif; ?> style="text-align:center; width:40px;" />
				</td>
			</tr>
			<tr>
				<td>Picture</td>
				<td>
					<img id="img_upload_preview" src="<?php if (isset ( $this->_tpl_vars['player'] )):  echo $this->_tpl_vars['site_dir']; ?>
media/images/players/thumb_180_<?php echo $this->_tpl_vars['player']['player_picture'];  endif; ?>" alt="No preview" class="profile_picture" />
					<input type="file" name="player_picture" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_picture']; ?>
"<?php endif; ?> id="img_upload" />
				</td>
                <td rowspan="2">
                    Card Picture
					<input type="file" name="player_card_picture" <?php if (isset ( $this->_tpl_vars['player'] )): ?>value="<?php echo $this->_tpl_vars['player']['player_card_picture']; ?>
"<?php endif; ?> id="img_card_upload" />
                </td>
			</tr>
			<tr>
				<td coslpan="2"><input type="submit" value="Save <?php if (isset ( $this->_tpl_vars['edit'] )): ?>changes<?php else: ?>new player<?php endif; ?>" style="float:right;" /></td>
			</tr>
		</tbody>
	</table>
</form>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>