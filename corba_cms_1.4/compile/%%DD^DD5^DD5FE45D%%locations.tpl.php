<?php /* Smarty version 2.6.3, created on 2012-07-04 16:30:45
         compiled from locations.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'locations.tpl', 21, false),)), $this); ?>
<h1>Locations</h1>
<p>Manage locations for the Pizza Hut Indonesia website</p>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_new">New restaurant</a> <a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_city_new">New city/region</a> <a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_province_new">New province</a> 
</div>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<h2>All restaurants</h2>
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:300px;">Restaurant name</td>
		<td style="width:200px;">City / Region</td>
		<td style="width:200px;">Province</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['restos']))):
    foreach ($_from as $this->_tpl_vars['resto']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td><?php echo $this->_tpl_vars['resto']['name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['resto']['city']; ?>
</td>
		<td><?php echo $this->_tpl_vars['resto']['province']; ?>
</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_edit&id=<?php echo $this->_tpl_vars['resto']['id']; ?>
">Edit</a> | <a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['resto']['name']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations&del=<?php echo $this->_tpl_vars['resto']['id']; ?>
');">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<h2>Cities & regions</h2>
<table id="overview">
	<tr>
		<td>City / Region</td>
		<td>Province</td>
		<td>&nbsp;</td>
	</tr>
<?php if (count($_from = (array)$this->_tpl_vars['cities'])):
    foreach ($_from as $this->_tpl_vars['city']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td style="font-weight:bold;"><?php echo $this->_tpl_vars['city']['city_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['city']['prov_name']; ?>
 </td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_city_edit&id=<?php echo $this->_tpl_vars['city']['city_id']; ?>
">Edit</a> | <a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['city']['city_name']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_city_del&id=<?php echo $this->_tpl_vars['city']['city_id']; ?>
');">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
</table>
<p><em>* If city/region doesn't have a restaurant yet, it will not be displayed on the website.</em></p>
<h2>Provinces</h2>
<table id="overview">
	<tr>
		<td>Province</td>
		<td>&nbsp;</td>
	</tr>
<?php if (count($_from = (array)$this->_tpl_vars['provinces'])):
    foreach ($_from as $this->_tpl_vars['prov']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td style="font-weight:bold;"><?php echo $this->_tpl_vars['prov']['prov_name']; ?>
</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_province_edit&id=<?php echo $this->_tpl_vars['prov']['prov_id']; ?>
">Edit</a> | <a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['prov']['prov_name']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=locations_province_del&id=<?php echo $this->_tpl_vars['prov']['prov_id']; ?>
');">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
</table>