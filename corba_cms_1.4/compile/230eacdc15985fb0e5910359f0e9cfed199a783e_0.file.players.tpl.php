<?php
/* Smarty version 3.1.30, created on 2016-10-08 13:49:18
  from "/var/www/jugador-indonesia.club/admin/templates/players.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f896eec47083_23352089',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '230eacdc15985fb0e5910359f0e9cfed199a783e' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/players.tpl',
      1 => 1468252555,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f896eec47083_23352089 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Team Roster</h1>
<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['warning']->value) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['message']->value) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<div id="actions">
	<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=players_new">Add player</a>
</div>
<?php if ($_smarty_tpl->tpl_vars['players']->value != '') {?>
<table id="grid" class="display">
	<thead>
	<tr>
        <td class="title" style="width:100px;">Photo</td>
		<td class="title">Name</td>
		<td class="title">#</td>
		<td class="title">Position</td>
		<td class="title">Status</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['players']->value), 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
		<td>
            <img src="<?php echo $_smarty_tpl->tpl_vars['site_dir']->value;?>
media/images/players/thumb_120_<?php echo $_smarty_tpl->tpl_vars['p']->value['player_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['p']->value['player_name'];?>
" class="profile_picture icon" style="float:left; margin-right:5px;" />
            <img src="<?php echo $_smarty_tpl->tpl_vars['site_dir']->value;?>
media/images/player_cards/<?php echo $_smarty_tpl->tpl_vars['p']->value['player_card_picture'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['p']->value['player_name'];?>
" style="width:50px; height:auto;" />
        </td>
        <td>
            <span style="font-size:16px;"><?php echo $_smarty_tpl->tpl_vars['p']->value['player_name'];?>
</span><br />
            <em><?php echo $_smarty_tpl->tpl_vars['p']->value['player_display_name'];?>
</em>
        </td>
		<td><?php echo $_smarty_tpl->tpl_vars['p']->value['player_no'];?>
</td>
		<td>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['p']->value['player_positions'], 'position');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['position']->value) {
?>
			<?php echo $_smarty_tpl->tpl_vars['position']->value['position_code'];?>
,
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

		</td>
		<td><?php echo $_smarty_tpl->tpl_vars['p']->value['player_status'];?>
</td>
		<td><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=players_edit&id=<?php echo $_smarty_tpl->tpl_vars['p']->value['player_id'];?>
">Edit</a></td>
	</tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	</tbody>
</table>
<?php } else { ?>
	<p>No players are available.</p>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
