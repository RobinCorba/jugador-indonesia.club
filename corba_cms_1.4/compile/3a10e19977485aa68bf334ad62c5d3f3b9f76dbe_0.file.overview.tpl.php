<?php
/* Smarty version 3.1.30, created on 2016-10-15 20:34:38
  from "/var/www/jugador-indonesia.club/corba_cms_1.4/modules/admin/templates/overview.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5802306e93e627_81993427',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3a10e19977485aa68bf334ad62c5d3f3b9f76dbe' => 
    array (
      0 => '/var/www/jugador-indonesia.club/corba_cms_1.4/modules/admin/templates/overview.tpl',
      1 => 1476538474,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:datatable.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5802306e93e627_81993427 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<?php $_smarty_tpl->_subTemplateRender("file:datatable.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>All accounts</h1>
<?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (isset($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (isset($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<div id="actions">
	<a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
admin/new-user">Create new user</a>
</div>
<?php if (empty($_smarty_tpl->tpl_vars['users']->value)) {?>
    <p id="warning">No CMS users registered</p><p id="error">Contact your system administrator</p>
<?php } else { ?>
    <table class="grid display">
        <thead>
        <tr>
            <td class="title" style="width:200px;">Admin name</td>
            <td class="title" style="width:150px;">Account type</td>
            <td class="title" style="width:150px;">Registered</td>
            <td class="title" style="width:200px;">Last login</td>
            <td class="title" style="width:100px;">&nbsp;</td>
        </tr>
        </thead>
        <tbody>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, 'u');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['u']->value) {
?>
            <tr>
                <td><?php echo $_smarty_tpl->tpl_vars['u']->value['real_name'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['u']->value['type'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['u']->value['date_created'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['u']->value['last_login_date'];?>
 (<?php echo $_smarty_tpl->tpl_vars['u']->value['last_login_time'];?>
 hrs)</td>
                <td><a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
edit-user/<?php echo $_smarty_tpl->tpl_vars['u']->value['admin_id'];?>
">Edit</a> | <a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
admin/reset/<?php echo $_smarty_tpl->tpl_vars['u']->value['admin_id'];?>
">Reset password</a><?php if ($_smarty_tpl->tpl_vars['user_type']->value == "superadmin") {?> | <a href="javascript:confirmDelete('<?php echo $_smarty_tpl->tpl_vars['u']->value['admin_name_real'];?>
','<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
admin/reset<?php echo $_smarty_tpl->tpl_vars['u']->value['admin_id'];?>
')">Delete</a><?php }?></td>
            </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

        </tbody>
    </table>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
