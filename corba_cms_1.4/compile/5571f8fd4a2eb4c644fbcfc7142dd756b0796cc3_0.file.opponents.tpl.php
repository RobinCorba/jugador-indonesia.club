<?php
/* Smarty version 3.1.30, created on 2016-10-09 02:58:06
  from "/var/www/jugador-indonesia.club/admin/templates/opponents.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f94fce4241c0_07874706',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5571f8fd4a2eb4c644fbcfc7142dd756b0796cc3' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/opponents.tpl',
      1 => 1475909522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f94fce4241c0_07874706 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>Opponent teams</h1>
<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['warning']->value) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['message']->value) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<div id="actions">
	<a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=opponents_new">Add opponent</a>
</div>
<?php if ($_smarty_tpl->tpl_vars['opponents']->value != '') {?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Name</td>
		<td class="title" style="width:200px;">Color</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['opponents']->value), 'o');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['o']->value) {
?>
		<td><?php echo $_smarty_tpl->tpl_vars['o']->value['opponent_name'];?>
</td>
		<td><?php echo $_smarty_tpl->tpl_vars['o']->value['opponent_color'];?>
</td>
		<td><a href="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=opponents_edit&id=<?php echo $_smarty_tpl->tpl_vars['o']->value['opponent_id'];?>
">Edit</a></td>
	</tr>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

	</tbody>
</table>
<?php } else { ?>
	<p>No opponents available.</p>
<?php }
$_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
