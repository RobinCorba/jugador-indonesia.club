<?php /* Smarty version 2.6.3, created on 2013-02-18 02:51:40
         compiled from bike_new.tpl */ ?>
<h1>New motorcycle</h1>
<p>Add a new motorcycle.</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=bike_new" enctype="multipart/form-data">
<table id="details" style="width:800px;">
	<tr>
		<td>Bike name</td>
		<td>
			<input type="text" name="motor_name" maxlength="64" value="<?php echo $this->_tpl_vars['data']['motor_name']; ?>
" style="width:200px;" /> <em>Max. 64 characters.</em>
		</td>
	</tr>
	<tr class="odd">
		<td style="width:200px;">Brand</td>
		<td>
			<select name="motor_brand_id">
				<?php if (count($_from = (array)($this->_tpl_vars['brands']))):
    foreach ($_from as $this->_tpl_vars['brand']):
?>
				<option value="<?php echo $this->_tpl_vars['brand']['brand_id']; ?>
" <?php if ($this->_tpl_vars['data']['motor_brand_id'] == $this->_tpl_vars['brand']['brand_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['brand']['brand_name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Picture</td>
		<td>
			<?php if ($this->_tpl_vars['data']['motor_img_name'] != ""): ?><img src="<?php echo $this->_tpl_vars['site_dir']; ?>
media/images/bikes/thumb_<?php echo $this->_tpl_vars['data']['motor_img_name']; ?>
" alt="Picture of the <?php echo $this->_tpl_vars['data']['motor_name']; ?>
" title="Picture of the <?php echo $this->_tpl_vars['data']['motor_name']; ?>
" /><br /><?php endif; ?>
			<input type="file" name="motor_img_upload" /><br />
			<em>Picture has to be a .jpg file and max. 2 MB in size.</em>
		</td>
	</tr>
	<tr class="odd">
		<td>Type</td>
		<td>
			<select name="motor_type_id">
				<?php if (count($_from = (array)($this->_tpl_vars['types']))):
    foreach ($_from as $this->_tpl_vars['type']):
?>
				<option value="<?php echo $this->_tpl_vars['type']['type_id']; ?>
" <?php if ($this->_tpl_vars['data']['motor_type_id'] == $this->_tpl_vars['type']['type_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['type']['type_name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Subtype (optional)</td>
		<td>
			<select name="motor_subtype_id">
				<option value="0" <?php if ($this->_tpl_vars['data']['motor_subtype_id'] == 0): ?>selected<?php endif; ?>>No subtype</option>
				<?php if (count($_from = (array)($this->_tpl_vars['subtypes']))):
    foreach ($_from as $this->_tpl_vars['subtype']):
?>
				<option value="<?php echo $this->_tpl_vars['subtype']['subtype_id']; ?>
" <?php if ($this->_tpl_vars['data']['motor_subtype_id'] == $this->_tpl_vars['subtype']['subtype_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['subtype']['subtype_name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td>CC</td>
		<td>
			<input type="text" name="motor_cc" maxlength="5" value="<?php echo $this->_tpl_vars['data']['motor_cc']; ?>
" style="width:50px;" />
		</td>
	</tr>
	<tr>
		<td>Price</td>
		<td>
			<input type="text" name="motor_price" maxlength="12" value="<?php echo $this->_tpl_vars['data']['motor_price']; ?>
" style="width:90px; text-align:right;" />
			<select name="motor_currency">
				<option value="IDR" <?php if ($this->_tpl_vars['data']['motor_currency'] == 'IDR'): ?>selected<?php endif; ?>>IDR</option>
				<option value="USD" <?php if ($this->_tpl_vars['data']['motor_currency'] == 'USD'): ?>selected<?php endif; ?>>USD</option>
				<option value="EUR" <?php if ($this->_tpl_vars['data']['motor_currency'] == 'EUR'): ?>selected<?php endif; ?>>EUR</option>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td>Description</td>
		<td>
			<textarea name="motor_description" style="width:500px; height:200px;"><?php echo $this->_tpl_vars['data']['motor_description']; ?>
</textarea>
		</td>
	</tr>
	<tr>
		<td>Official page (optional)</td>
		<td>
			<input type="text" name="motor_url" maxlength="128" value="<?php echo $this->_tpl_vars['data']['motor_url']; ?>
" style="width:300px;" /><br />
			<em>URL of the official website where this motorcycle is highlighted.</em>
		</td>
	</tr>
	<tr class="odd">
		<td>Mark as new</td>
		<td>
			<input type="checkbox" name="motor_mark_new" value="true" <?php if ($this->_tpl_vars['data']['motor_mark_new'] == 'true'): ?>checked<?php endif; ?> />
			<em>This will display a 'new' tag on the bike</em>
		</td>
	</tr>
	<tr>
		<td>Mark as promo</td>
		<td>
			<input type="checkbox" name="motor_mark_promo" value="true" <?php if ($this->_tpl_vars['data']['motor_mark_promo'] == 'true'): ?>checked<?php endif; ?>>
			<em>This will display a 'promo' tag on the bike (when price has dropped)</em>
		</td>
	</tr>
	<tr class="odd">
		<td colspan="2"><input type="submit" value="Save changes" style="float:right;"/></td>
	</tr>
</table>
</form>