<?php /* Smarty version 2.6.3, created on 2013-04-12 00:07:45
         compiled from api_overview.tpl */ ?>
<div id="header">Corba CMS - API documentation (version April 12th, 2013, generated on <?php echo $this->_tpl_vars['today']; ?>
)</div>
<div id="container">
	<div id="nav">
	<h2>Library</h2>
		<?php unset($this->_sections['this']);
$this->_sections['this']['name'] = 'this';
$this->_sections['this']['loop'] = is_array($_loop=($this->_tpl_vars['library'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['this']['show'] = true;
$this->_sections['this']['max'] = $this->_sections['this']['loop'];
$this->_sections['this']['step'] = 1;
$this->_sections['this']['start'] = $this->_sections['this']['step'] > 0 ? 0 : $this->_sections['this']['loop']-1;
if ($this->_sections['this']['show']) {
    $this->_sections['this']['total'] = $this->_sections['this']['loop'];
    if ($this->_sections['this']['total'] == 0)
        $this->_sections['this']['show'] = false;
} else
    $this->_sections['this']['total'] = 0;
if ($this->_sections['this']['show']):

            for ($this->_sections['this']['index'] = $this->_sections['this']['start'], $this->_sections['this']['iteration'] = 1;
                 $this->_sections['this']['iteration'] <= $this->_sections['this']['total'];
                 $this->_sections['this']['index'] += $this->_sections['this']['step'], $this->_sections['this']['iteration']++):
$this->_sections['this']['rownum'] = $this->_sections['this']['iteration'];
$this->_sections['this']['index_prev'] = $this->_sections['this']['index'] - $this->_sections['this']['step'];
$this->_sections['this']['index_next'] = $this->_sections['this']['index'] + $this->_sections['this']['step'];
$this->_sections['this']['first']      = ($this->_sections['this']['iteration'] == 1);
$this->_sections['this']['last']       = ($this->_sections['this']['iteration'] == $this->_sections['this']['total']);
?>
			<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=api_overview&class=<?php echo $this->_tpl_vars['library'][$this->_sections['this']['index']]; ?>
"><?php echo $this->_tpl_vars['library'][$this->_sections['this']['index']]; ?>
</a><br />
		<?php endfor; endif; ?>
	</div>
	<div id="class">
		<?php if ($this->_tpl_vars['class_name'] != ""): ?>
			<h1><?php echo $this->_tpl_vars['class_name']; ?>
 class</h1>
			<?php if (count($_from = (array)($this->_tpl_vars['functions']))):
    foreach ($_from as $this->_tpl_vars['this']):
?>
				<hr />
				<h2>Function <?php echo $this->_tpl_vars['this']['name_full']; ?>
)</h2>
				<?php if ($this->_tpl_vars['this']['desc'] != ""): ?>
					<span class="desc"><?php echo $this->_tpl_vars['this']['desc']; ?>
</span>
				<?php else: ?>
					<span class="desc"><em>Missing documentation</em></span>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['this']['return_type'] != ""): ?>
					<strong>Returns</strong><br />
					<span class="return">
						<a class="type"><?php echo $this->_tpl_vars['this']['return_type']; ?>
</a>	<a class="desc"><?php echo $this->_tpl_vars['this']['return_desc']; ?>
</a>
					</span>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['this']['params'] != ""): ?>
					<strong>Input variables</strong><br />
					<?php if (count($_from = (array)$this->_tpl_vars['this']['params'])):
    foreach ($_from as $this->_tpl_vars['param']):
?>
						<span class="param">
							<a class="type"><?php echo $this->_tpl_vars['param']['type']; ?>
</a> <?php echo $this->_tpl_vars['param']['name']; ?>
<br />
							<a class="desc"><?php echo $this->_tpl_vars['param']['desc']; ?>
</a>
						</span>
					<?php endforeach; unset($_from); endif; ?>
				<?php else: ?>
				
				<?php endif; ?>
			<?php endforeach; unset($_from); endif; ?>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['error']):  echo $this->_tpl_vars['error'];  endif; ?>
	</div>
</div>