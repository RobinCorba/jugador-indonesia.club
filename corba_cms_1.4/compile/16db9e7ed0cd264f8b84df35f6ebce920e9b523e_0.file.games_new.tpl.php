<?php
/* Smarty version 3.1.30, created on 2016-10-09 02:58:12
  from "/var/www/jugador-indonesia.club/admin/templates/games_new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f94fd4740ca8_66090772',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '16db9e7ed0cd264f8b84df35f6ebce920e9b523e' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/games_new.tpl',
      1 => 1475909522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_57f94fd4740ca8_66090772 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<h1>New game results</h1>
<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['warning']->value) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if ($_smarty_tpl->tpl_vars['message']->value) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<form method="post">
	<input type="hidden" name="game_id" maxlength="4" <?php if (isset($_smarty_tpl->tpl_vars['game']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['game']->value['game_id'];?>
"<?php }?> />
	<table class="details">
		<tbody>
			<tr>
				<td>Date</td>
				<td>
					<input type="text" name="game_date_day" maxlength="2" placeholder="DD" value="<?php if (isset($_smarty_tpl->tpl_vars['game']->value)) {
echo $_smarty_tpl->tpl_vars['game']->value['game_date_day'];
} else {
echo $_smarty_tpl->tpl_vars['this_day']->value;
}?>" style="width:50px;" />
					<input type="text" name="game_date_month" maxlength="2" placeholder="MM" value="<?php if (isset($_smarty_tpl->tpl_vars['game']->value)) {
echo $_smarty_tpl->tpl_vars['game']->value['game_date_month'];
} else {
echo $_smarty_tpl->tpl_vars['this_month']->value;
}?>" style="width:50px;" />
					<input type="text" name="game_date_year" maxlength="4" placeholder="YYYY" value="<?php if (isset($_smarty_tpl->tpl_vars['game']->value)) {
echo $_smarty_tpl->tpl_vars['game']->value['game_date_year'];
} else {
echo $_smarty_tpl->tpl_vars['this_year']->value;
}?>" style="width:80px;" />
				</td>
			</tr>
			<tr>
				<td>Opponent</td>
				<td>
					<select name="game_opponent">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['opponents']->value), 'o');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['o']->value) {
?>
						<option value="<?php echo $_smarty_tpl->tpl_vars['o']->value['opponent_id'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['game']->value) && $_smarty_tpl->tpl_vars['game']->value['game_opponent'] == $_smarty_tpl->tpl_vars['o']->value['opponent_id']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['o']->value['opponent_name'];?>
</option>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

					</select>
				</td>
			</tr>
			<tr>
				<td>Score</td>
				<td><input type="text" maxlength="64" name="game_score" placeholder="Final score" <?php if (isset($_smarty_tpl->tpl_vars['game']->value)) {?>value="<?php echo $_smarty_tpl->tpl_vars['game']->value['game_score'];?>
"<?php }?> /></td>
			</tr>
            <tr>
				<td>Result</td>
				<td>
					<select name="game_result">
						<option value="won" <?php if (isset($_smarty_tpl->tpl_vars['game']->value) && $_smarty_tpl->tpl_vars['game']->value['game_result'] == "won") {?>selected<?php }?>>Win</option>
						<option value="drew" <?php if (isset($_smarty_tpl->tpl_vars['game']->value) && $_smarty_tpl->tpl_vars['game']->value['game_result'] == "drew") {?>selected<?php }?>>Draw</option>
						<option value="lost" <?php if (isset($_smarty_tpl->tpl_vars['game']->value) && $_smarty_tpl->tpl_vars['game']->value['game_result'] == "lost") {?>selected<?php }?>>Loss</option>
					</select>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top; padding-right:25px;">Goals and <br />clean sheets </td>
				<td>
					
					<table id="goal_overview">
						<thead>
							<tr style="font-weight:bold;">
								<td>Player name (no.)</td>
								<td>Pos.</td>
								<td style="padding-right:15px;">Total goals</td>
								<td>Clean sheet?</td>
							</tr>
						</thead>
						<tbody>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, ((string)$_smarty_tpl->tpl_vars['players']->value), 'p');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['p']->value) {
?>
							<tr>
								<td style="padding-right:15px;"><?php echo $_smarty_tpl->tpl_vars['p']->value['player_name'];
if ($_smarty_tpl->tpl_vars['p']->value['player_no'] != 0) {?> (<?php echo $_smarty_tpl->tpl_vars['p']->value['player_no'];?>
)<?php }?></td>
								<td style="padding-right:15px;"><?php echo $_smarty_tpl->tpl_vars['p']->value['player_card_position'];?>
</td>
								<td style="padding-right:15px;"><input type="text" name="goals[<?php echo $_smarty_tpl->tpl_vars['p']->value['player_id'];?>
]" maxlength="1" value="<?php if (isset($_smarty_tpl->tpl_vars['game']->value['goals'][$_smarty_tpl->tpl_vars['p']->value['player_id']])) {
echo $_smarty_tpl->tpl_vars['game']->value['goals'][$_smarty_tpl->tpl_vars['p']->value['player_id']];
} else { ?>0<?php }?>" style="width:12px; text-align:center;" /></td>
                                <td>
                                    <select name="clean_sheets[<?php echo $_smarty_tpl->tpl_vars['p']->value['player_id'];?>
]">
                                        <option value="yes"<?php if (isset($_smarty_tpl->tpl_vars['game']->value['clean_sheets'][$_smarty_tpl->tpl_vars['p']->value['player_id']])) {?>selected<?php }?>>Yes</option>
                                        <option value="no"<?php if (!isset($_smarty_tpl->tpl_vars['game']->value['clean_sheets'][$_smarty_tpl->tpl_vars['p']->value['player_id']])) {?>selected<?php }?>>No</option>
                                    </select>
                                </td>
							</tr>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

						</tbody>
					</table>
					
				</td>
			</tr>
			<tr>
				<td coslpan="2"><input type="submit" value="Save" /></td>
			</tr>
		</tbody>
	</table>
</form>
<?php $_smarty_tpl->_subTemplateRender("file:footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
