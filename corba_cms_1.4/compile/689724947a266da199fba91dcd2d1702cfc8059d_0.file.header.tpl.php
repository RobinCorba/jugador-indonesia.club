<?php
/* Smarty version 3.1.30, created on 2016-10-15 21:40:11
  from "/var/www/jugador-indonesia.club/admin/templates/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_58023fcb49a0c9_47395205',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '689724947a266da199fba91dcd2d1702cfc8059d' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/header.tpl',
      1 => 1476542403,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_58023fcb49a0c9_47395205 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php if (isset($_smarty_tpl->tpl_vars['seo_title']->value)) {
echo $_smarty_tpl->tpl_vars['seo_title']->value;
} else { ?>Administration<?php }?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="<?php if (isset($_smarty_tpl->tpl_vars['seo_desc']->value)) {
echo $_smarty_tpl->tpl_vars['seo_desc']->value;
}?>" />
		<meta name="keywords" content="Corba CMS<?php if (isset($_smarty_tpl->tpl_vars['cms']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['cms']->value['release_version'];?>
 (<?php echo $_smarty_tpl->tpl_vars['cms']->value['release_month'];?>
 <?php echo $_smarty_tpl->tpl_vars['cms']->value['release_year'];?>
)<?php }?>" />
		<meta name="robots" content="nofollow, noindex" />
		<meta name="copyright" content="Robin Corba<?php if (isset($_smarty_tpl->tpl_vars['cms']->value)) {?> 2010-<?php echo $_smarty_tpl->tpl_vars['cms']->value['release_year'];
}?>" />
		
        <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
templates/css/style.css" />
		<link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
templates/img/favicon.ico" />
        <?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="//code.jquery.com/jquery-2.2.3.min.js"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/class.ajax.js"><?php echo '</script'; ?>
>
	</head>
	<body>
        <div id="header"><img src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
templates/img/client_logo.jpg" alt="" /></div>
        <div id="container">
            <div id="nav">
                <a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
news">News</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
football">Football</a>
                <a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
?p=statistics">Statistics</a>
                <?php if (isset($_smarty_tpl->tpl_vars['user_type']->value) && $_smarty_tpl->tpl_vars['user_type']->value == "superadmin") {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
admin">Administration</a>
                <?php }?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
logout">Logout</a>
            </div>
            <div id="content"><?php }
}
