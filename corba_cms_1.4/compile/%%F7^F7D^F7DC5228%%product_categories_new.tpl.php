<?php /* Smarty version 2.6.3, created on 2013-10-12 22:05:02
         compiled from product_categories_new.tpl */ ?>
<h1>New product category</h1>
<p>Add a new category for your product line</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_categories_overview">
<table id="details" style="width:800px;">
	<tr>
		<td>Category name</td>
		<td>
			<input type="text" name="prodcat_name" maxlength="48" style="width:200px;" value="<?php echo $this->_tpl_vars['data']['article_title']; ?>
" /><br />
			<em>Max. 48 characters.</em>
		</td>
	</tr>
	<?php if (! empty ( $this->_tpl_vars['cats'] )): ?>
	<tr class="odd">
		<td>Add new category after</td>
		<td>
			<select name="product_order">
				<?php if (count($_from = (array)($this->_tpl_vars['cats']))):
    foreach ($_from as $this->_tpl_vars['c']):
?>
				<option value="<?php echo $this->_tpl_vars['c']['prodcat_id']; ?>
"><?php echo $this->_tpl_vars['c']['prodcat_name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<?php endif; ?>
	<tr class="odd">
		<td colspan="2"><input type="submit" value="Save category" style="float:right;" /></td>
	</tr>
</table>
</form>