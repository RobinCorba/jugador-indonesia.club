<?php
/* Smarty version 3.1.30, created on 2016-10-09 14:20:58
  from "/var/www/jugador-indonesia.club/admin/templates/datatable.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f9efdaa01784_91403408',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8ef547264fc90035bc9cb09effed66aaa9cf2a48' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/datatable.tpl',
      1 => 1475997657,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57f9efdaa01784_91403408 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- jQuery dataTables -->
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/grid/js/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/grid/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/grid/js/jquery.dataTables.dragndrop.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/grid/js/jquery-ui.js"><?php echo '</script'; ?>
>
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/grid/css/demo_table_jui.css">
<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
js/grid/css/jquery-ui-1.8.4.custom.css">

<?php echo '<script'; ?>
 type="text/javascript">
$(document).ready
(
    function()
    {
        $(".loading").hide();
        $('.grid').dataTable
        ({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers"
        });
    }
);
<?php echo '</script'; ?>
>
<?php }
}
