<?php /* Smarty version 2.6.3, created on 2013-02-24 21:23:03
         compiled from dealer_overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'dealer_overview.tpl', 23, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>Dealership Management</h1>
<p>An overview of all dealerships.</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=dealer_new">Add dealership</a>
</div>
<?php if ($this->_tpl_vars['dealers'] == ""): ?>
	<p id="warning">Warning, no dealerships are available.</p>
<?php else: ?>
	<table id="grid" class="display">
		<thead>
		<tr>
			<td>Dealership name</td>
			<td>Dealer brand</td>
			<td class="no_sort">&nbsp;</td>
		</tr>
		</thead>
		<tbody>
	<?php if (count($_from = (array)($this->_tpl_vars['dealers']))):
    foreach ($_from as $this->_tpl_vars['d']):
?>
		<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
			<td><?php echo $this->_tpl_vars['d']['dealer_name']; ?>
</td>
			<td><?php echo $this->_tpl_vars['d']['brand_name']; ?>
</td>
			<td>
				<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=dealer_edit&id=<?php echo $this->_tpl_vars['d']['dealer_id']; ?>
">Edit</a> | 
				<a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['d']['dealer_name']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=dealer_overview&del=<?php echo $this->_tpl_vars['d']['dealer_id']; ?>
');">Delete</a>
			</td>
		</tr>
	<?php endforeach; unset($_from); endif; ?>
		</tbody>
	</table>
<?php endif; ?>