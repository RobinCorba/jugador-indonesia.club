<?php
/* Smarty version 3.1.30, created on 2016-10-15 20:49:27
  from "/var/www/jugador-indonesia.club/admin/templates/user_new.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_580233e7bcb719_60389915',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bf76e99686fe191a51cdfa4b0892e0b4b2f2ca3d' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/user_new.tpl',
      1 => 1475909522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_580233e7bcb719_60389915 (Smarty_Internal_Template $_smarty_tpl) {
?>
<h1>Create new account</h1>
<?php if ($_smarty_tpl->tpl_vars['user_type']->value != "superadmin") {?>
	<p id="error">Whoopsy daisy, you aren't allowed to open this page.</p>
<?php } else { ?>
	<?php if ($_smarty_tpl->tpl_vars['error']->value) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['warning']->value) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }?>
	<?php if ($_smarty_tpl->tpl_vars['message']->value) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
	<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
?p=user_new" />
		<table id="details">
			<tr class="odd">
				<td>Username</td>
				<td><input type="text" name="user_name_new" maxlength="30" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['user_name'];?>
" style="width:150px;" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="password" name="user_pass_new" autocomplete="off" style="width:120px;" /></td>
			</tr>
			<tr class="odd">
				<td>Real name</td>
				<td><input type="text" name="user_name_real" maxlength="50" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['user_name_real'];?>
" style="width:180px;" /></td>
			</tr>
			<tr>
				<td>E-mail address (optional)</td>
				<td><input type="text" name="user_email" autocomplete="off" maxlength="128" style="width:200px;" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['user_email'];?>
" /></td>
			</tr>
			<tr class="odd">
				<td>Account type</td>
				<td>
					<select name="user_type">
						<option value="admin" <?php if ($_smarty_tpl->tpl_vars['data']->value['user_type'] == "admin") {?>selected<?php }?>>Standard admin</option>
						<option value="superadmin" <?php if ($_smarty_tpl->tpl_vars['data']->value['user_type'] == "superadmin") {?>selected<?php }?>>Super administrator</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:10px;"><input type="submit" value="Save changes" style="float:right;"/></td>
			</tr>
		</table>
	</form>
<?php }
}
}
