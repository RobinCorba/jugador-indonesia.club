<?php /* Smarty version 2.6.3, created on 2013-04-12 00:03:23
         compiled from page_overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'page_overview.tpl', 23, false),)), $this); ?>
<h1>Page Management</h1>
<p>An overview of all pages.</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<div id="actions">
	<a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=page_new">New page</a>
</div>
<?php if ($this->_tpl_vars['pages'] == ""): ?>
	<p id="warning">Warning, no pages are available.</p>
<?php else: ?>
	<table id="grid" class="display dataTable">
		<thead>
		<tr>
			<td class="no_sort">#</td>
			<td class="no_sort">Page title</td>
			<td class="no_sort">Last edit</td>
			<td class="no_sort">&nbsp;</td>
		</tr>
		</thead>
		<tbody class="ui-sortable">
	<?php if (count($_from = (array)($this->_tpl_vars['pages']))):
    foreach ($_from as $this->_tpl_vars['page']):
?>
		<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
 data-position="<?php echo $this->_tpl_vars['page']['id']; ?>
" id="<?php echo $this->_tpl_vars['page']['id']; ?>
">
			<td><?php echo $this->_tpl_vars['page']['page_order']; ?>
</td>
			<td><?php echo $this->_tpl_vars['page']['page_title']; ?>
</td>
			<td><?php echo $this->_tpl_vars['page']['date_edited_real']; ?>
 hrs)</td>
			<td>
				<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=page_edit&id=<?php echo $this->_tpl_vars['page']['id']; ?>
">Edit</a> | 
				<a href="javascript:confirmDelete('<?php echo $this->_tpl_vars['page']['page_title']; ?>
','<?php echo $this->_tpl_vars['base_dir']; ?>
?p=page_overview&del=<?php echo $this->_tpl_vars['page']['id']; ?>
');">Delete</a>
				<?php if ($this->_tpl_vars['page']['total_subs'] != 0): ?> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=page_sub_overview&id=<?php echo $this->_tpl_vars['page']['id']; ?>
">Manage subpages</a><?php endif; ?>
			</td>
		</tr>
	<?php endforeach; unset($_from); endif; ?>
		</tbody>
	</table>
<?php endif; ?>