<?php /* Smarty version 2.6.3, created on 2012-07-09 14:39:31
         compiled from banner_edit.tpl */ ?>
<h1>Update Banner</h1>
<p>Update banner information.</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=banner_edit" enctype="multipart/form-data">
<table id="details" style="width:800px;">
	<tr>
		<td style="width:220px;">Banner Type</td>
		<td>
			<select name="banner_type" style="width:300px;" id="banner_type">
				 <option value="sliders"<?php if ($this->_tpl_vars['banners']['ban_type'] == 'sliders'): ?> selected="selected"<?php endif; ?>>Slider</option>
                 <option value="banners"<?php if ($this->_tpl_vars['banners']['ban_type'] == 'banners'): ?> selected="selected"<?php endif; ?>>Banner</option>
			</select>
            
		</td>
	</tr>
	<tr class="odd">
		<td style="width:220px;">Banner name</td>
		<td>
			<input name="ban_name" type="text" id="ban_name" style="width:200px;" value="<?php echo $this->_tpl_vars['banners']['ban_name']; ?>
" maxlength="128" />
		</td>
	</tr>
	<tr>
		<td>Banner URL</td>
		<td>
			<input name="ban_url" type="text" id="ban_url" style="width:500px;" value="<?php echo $this->_tpl_vars['banners']['ban_url']; ?>
" maxlength="256" />
		</td>
	</tr>
	<tr class="odd">
		<td style="width:220px;">Publish</td>
		<td>
        <input type="radio" name="publish" id="publish" value="1" <?php if ($this->_tpl_vars['banners']['published'] == '1'): ?>checked="checked"<?php endif; ?> /> Yes
        <input type="radio" name="publish" id="publish" value="0" <?php if ($this->_tpl_vars['banners']['published'] == '0'): ?>checked="checked"<?php endif; ?> /> Draft
        </td>
	</tr>
	<tr class="odd">
	  <td valign="top" style="width:220px;">Image URL</td>
	  <td>
       
      <img src="<?php echo $this->_tpl_vars['site_dir']; ?>
media/<?php echo $this->_tpl_vars['banners']['ban_type']; ?>
/<?php echo $this->_tpl_vars['banners']['ban_image']; ?>
" />
      
      </td>
    </tr>
	<tr class="odd">
		<td style="width:220px;"></td>
		<td><a href="javascript:showForm('edit1');" class="box-link">[change image]</a><br />
      <div id="edit1" style="display: none;">
      <label class="file-upload"> <span>Upload file</span>
	    <input type="file" name="file" id="file" />
	    </label>
	    
      </div></td>
	</tr>
	<tr>
		<td colspan="2">
        <input type="hidden" name="old_image" id="old_image" value="<?php echo $this->_tpl_vars['banners']['ban_image']; ?>
" />
        <input type="hidden" name="ban_id" id="ban_id" value="<?php echo $this->_tpl_vars['banners']['ban_id']; ?>
" />
        <input type="hidden" name="action" id="action" value="update" />
        <input type="submit" value="Edit product" style="float:right;"/></td>
	</tr>
</table>
</form>