<?php /* Smarty version 2.6.3, created on 2016-01-06 12:24:44
         compiled from user_new.tpl */ ?>
<h1>Create new account</h1>
<?php if ($this->_tpl_vars['user_type'] != 'superadmin'): ?>
	<p id="error">Whoopsy daisy, you aren't allowed to open this page.</p>
<?php else: ?>
	<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
	<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
	<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
	<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=user_new" />
		<table id="details">
			<tr class="odd">
				<td>Username</td>
				<td><input type="text" name="user_name_new" maxlength="30" value="<?php echo $this->_tpl_vars['data']['user_name']; ?>
" style="width:150px;" /></td>
			</tr>
			<tr>
				<td>Password</td>
				<td><input type="password" name="user_pass_new" autocomplete="off" style="width:120px;" /></td>
			</tr>
			<tr class="odd">
				<td>Real name</td>
				<td><input type="text" name="user_name_real" maxlength="50" value="<?php echo $this->_tpl_vars['data']['user_name_real']; ?>
" style="width:180px;" /></td>
			</tr>
			<tr>
				<td>E-mail address (optional)</td>
				<td><input type="text" name="user_email" autocomplete="off" maxlength="128" style="width:200px;" value="<?php echo $this->_tpl_vars['data']['user_email']; ?>
" /></td>
			</tr>
			<tr class="odd">
				<td>Account type</td>
				<td>
					<select name="user_type">
						<option value="admin" <?php if ($this->_tpl_vars['data']['user_type'] == 'admin'): ?>selected<?php endif; ?>>Standard admin</option>
						<option value="superadmin" <?php if ($this->_tpl_vars['data']['user_type'] == 'superadmin'): ?>selected<?php endif; ?>>Super administrator</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:10px;"><input type="submit" value="Save changes" style="float:right;"/></td>
			</tr>
		</table>
	</form>
<?php endif; ?>