<?php /* Smarty version 2.6.3, created on 2013-11-10 13:29:57
         compiled from product_edit.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>Edit product</h1>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<form method="post" action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_edit" enctype="multipart/form-data">
<input type="hidden" name="product_id" value="<?php echo $this->_tpl_vars['data']['product_id']; ?>
" />
<table id="details" style="width:800px;">
	<tr class="odd">
		<td style="width:220px;">Product picture</td>
		<td><input type="file" name="uploaded_img" /></td>
    </tr>
	<tr>
		<td style="width:220px;">Category</td>
		<td>
			<select name="prodcat_id" style="width:200px;">
				<?php if (count($_from = (array)($this->_tpl_vars['cats']))):
    foreach ($_from as $this->_tpl_vars['c']):
?>
					<option value="<?php echo $this->_tpl_vars['c']['prodcat_id']; ?>
" <?php if ($this->_tpl_vars['data']['product_category_id'] == $this->_tpl_vars['c']['prodcat_id']): ?>selected<?php endif; ?>><?php echo $this->_tpl_vars['c']['prodcat_name']; ?>
</option>
				<?php endforeach; unset($_from); endif; ?>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td style="width:220px;">Product name</td>
		<td>
			<input name="product_name" type="text" id="product_name" style="width:200px;" value="<?php echo $this->_tpl_vars['data']['product_name']; ?>
" maxlength="64" /><br />
			<em>Max. 64 characters.</em>
		</td>
	</tr>
	<tr>
		<td>Description</td>
		<td>
			<input name="product_description" type="text" id="product_description" style="width:500px;" value="<?php echo $this->_tpl_vars['data']['product_desc']; ?>
" maxlength="256" />
		</td>
	</tr>
	<tr class="odd">
		<td>Price</td>
		<td>
			Rp. <input name="product_price" type="text" style="width:65px;" value="<?php echo $this->_tpl_vars['data']['product_price']; ?>
" maxlength="12" /><br />
			<em>Max. 12 characters.</em>
		</td>
	</tr>
	<tr>
		<td colspan="2"><input type="submit" value="Save changes" style="float:right;"/></td>
	</tr>
</table>
</form>