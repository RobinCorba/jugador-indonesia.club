<?php /* Smarty version 2.6.3, created on 2012-07-04 16:31:12
         compiled from categories.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'categories.tpl', 65, false),)), $this); ?>
<h1>Products Categories</h1>
<p>Manage Products Categories for the Pizza Hut Indonesia website</p>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif; ?>
<?php if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif; ?>
<ul class="tabs">
        <!--<li><a href="#" <?php echo $this->_tpl_vars['actTab1']; ?>
 rel="tabs1">Upload new work</a></li>-->
		<li><a href="#" <?php echo $this->_tpl_vars['actTab1']; ?>
 rel="tabs1">Add New Category</a></li>
        <li><a href="#" <?php echo $this->_tpl_vars['actTab2']; ?>
 rel="tabs2">Add New Subcategory</a></li>
    </ul>
 <div class="tab-content" id="tabs1">
 <form action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=categories" method="post" enctype="multipart/form-data">
 			New Category
	        <table width="300" border="0">
	          <tr>
	            <td><input name="cat_name" type="text" id="cat_name" size="50" value="<?php echo $this->_tpl_vars['SELECTEDCATEGORY']; ?>
" /></td>
              </tr>
	          <tr>
	            <td>
                <input type="hidden" name="actTab" id="actTab" value="tab1" />
                <input type="submit" name="button" id="button" value="Submit" /></td>
              </tr>
            </table>
          </form>
 </div>   
 <div class="tab-content" id="tabs2">
 <form action="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=categories" method="post" enctype="multipart/form-data">
 			
	        <table width="300" border="0">
            <tr>
	            <td>
               Category
                </td>
              </tr>
            <tr>
	            <td>
                <select name="prodcat_id">
                	<?php if (count($_from = (array)($this->_tpl_vars['categories']))):
    foreach ($_from as $this->_tpl_vars['row']):
?>
                		<option value="<?php echo $this->_tpl_vars['row']['prodcat_id']; ?>
"><?php echo $this->_tpl_vars['row']['prodcat_name']; ?>
</option>
                    <?php endforeach; unset($_from); endif; ?>    
                </select>
                </td>
              </tr>
	          <tr>
	            <td>New Subcategory<br /><input name="subcat_name" type="text" id="subcat_name" size="50" value="<?php echo $this->_tpl_vars['SELECTEDCATEGORY']; ?>
" /></td>
              </tr>
	          <tr>
	            <td>
                <input type="hidden" name="actTab" id="actTab" value="tab2" />
                <input type="submit" name="button" id="button" value="Submit" /></td>
              </tr>
            </table>
          </form>
 </div><br />
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:300px;">Categories Name</td>
		<td style="width:200px;">Subcategory</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['categories']))):
    foreach ($_from as $this->_tpl_vars['row']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
 valign="top">
		<td><?php echo $this->_tpl_vars['row']['prodcat_name']; ?>
</td>
		<td>  
        <ul>      
                <?php if (count($_from = (array)($this->_tpl_vars['subcat']))):
    foreach ($_from as $this->_tpl_vars['sub']):
?>
                    <?php if ($this->_tpl_vars['row']['prodcat_id'] == $this->_tpl_vars['sub']['prodcat_id']): ?>
                        <li><?php echo $this->_tpl_vars['sub']['subcat_name']; ?>
</li>               
                    <?php endif; ?>
                <?php endforeach; unset($_from); endif; ?>
        </ul>           
        </td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=categories&catid=<?php echo $this->_tpl_vars['row']['prodcat_id']; ?>
">Edit</a> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=categories&delcat=<?php echo $this->_tpl_vars['row']['prodcat_id']; ?>
" onclick="return sure()">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<h3>Products Subcategories</h3>
<table id="grid" class="display" border="1">
	<thead>
	<tr bgcolor="#CCCCCC">
		<td style="width:300px;"><strong>SubCategories Name</strong></td>
		<td style="width:200px;"><strong>Category</strong></td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)($this->_tpl_vars['subcat']))):
    foreach ($_from as $this->_tpl_vars['row']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td><?php echo $this->_tpl_vars['row']['subcat_name']; ?>
</td>
		<td><?php echo $this->_tpl_vars['row']['prodcat_name']; ?>
</td>
		<td><a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=categories&amp;subcatid=<?php echo $this->_tpl_vars['row']['subcat_id']; ?>
">Edit</a> | <a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=categories&amp;delsubcat=<?php echo $this->_tpl_vars['row']['subcat_id']; ?>
" onclick="return sure()">Delete</a></td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>