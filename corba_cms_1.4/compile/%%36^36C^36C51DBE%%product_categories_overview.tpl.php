<?php /* Smarty version 2.6.3, created on 2013-10-12 17:46:09
         compiled from product_categories_overview.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'product_categories_overview.tpl', 22, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<h1>Product categories</h1>
<p>Manage all product categories</p>
<div id="actions">
	<img src="<?php echo $this->_tpl_vars['base_dir']; ?>
templates/img/icons/package_add.png" alt="" /> <a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_categories_new">New category</a>
	<img src="<?php echo $this->_tpl_vars['base_dir']; ?>
templates/img/icons/drink.png" alt="" /> <a id="new" href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_overview">Manage products</a>
</div>
<?php if ($this->_tpl_vars['error']): ?><p id="error"><?php echo $this->_tpl_vars['error']; ?>
</p><?php endif;  if ($this->_tpl_vars['warning']): ?><p id="warning"><?php echo $this->_tpl_vars['warning']; ?>
</p><?php endif;  if ($this->_tpl_vars['message']): ?><p id="message"><?php echo $this->_tpl_vars['message']; ?>
</p><?php endif;  if ($this->_tpl_vars['cats']): ?>
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:200px;">Order</td>
		<td style="width:200px;">Category name</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
<?php if (count($_from = (array)$this->_tpl_vars['cats'])):
    foreach ($_from as $this->_tpl_vars['c']):
?>
	<tr <?php echo smarty_function_cycle(array('values' => ',class="odd"'), $this);?>
>
		<td><?php echo $this->_tpl_vars['c']['prodcat_order']; ?>
</td>
		<td><?php echo $this->_tpl_vars['c']['prodcat_name']; ?>
</td>
		<td>
			<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_categories_edit&id=<?php echo $this->_tpl_vars['c']['prodcat_id']; ?>
">Edit</a> | 
			<a href="<?php echo $this->_tpl_vars['base_dir']; ?>
?p=product_categories_overview&del=<?php echo $this->_tpl_vars['c']['prodcat_id']; ?>
&n=<?php echo $this->_tpl_vars['c']['prodcat_name']; ?>
" onclick="return confirm('Are you sure you want to delete <?php echo $this->_tpl_vars['c']['prodcat_name']; ?>
?')">Delete</a>
		</td>
	</tr>
<?php endforeach; unset($_from); endif; ?>
	</tbody>
</table>
<?php else: ?>
	<p id="warning">No categories available yet</p>
<?php endif; ?>