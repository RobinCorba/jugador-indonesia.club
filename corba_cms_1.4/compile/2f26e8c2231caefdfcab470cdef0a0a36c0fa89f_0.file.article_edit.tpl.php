<?php
/* Smarty version 3.1.30, created on 2016-10-09 00:40:24
  from "/var/www/jugador-indonesia.club/admin/templates/article_edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_57f92f88a25a41_97629888',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2f26e8c2231caefdfcab470cdef0a0a36c0fa89f' => 
    array (
      0 => '/var/www/jugador-indonesia.club/admin/templates/article_edit.tpl',
      1 => 1475909522,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_57f92f88a25a41_97629888 (Smarty_Internal_Template $_smarty_tpl) {
?>
<h1>Edit article</h1>
<p>Make changes to an article of the news section</p>
<?php if (!empty($_smarty_tpl->tpl_vars['error']->value)) {?><p id="error"><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['warning']->value)) {?><p id="warning"><?php echo $_smarty_tpl->tpl_vars['warning']->value;?>
</p><?php }
if (!empty($_smarty_tpl->tpl_vars['message']->value)) {?><p id="message"><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</p><?php }?>
<form method="post" action="<?php echo $_smarty_tpl->tpl_vars['base_url']->value;?>
?p=article_edit&id=<?php echo $_smarty_tpl->tpl_vars['data']->value['post_id'];?>
">
<input type="hidden" name="post_id" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['post_id'];?>
" />
<table id="details" style="width:800px;">
	<tr class="odd">
		<td colspan="2" style="font-weight:normal; font-size:11px;">
			<input type="text" name="post_title" maxlength="128" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['post_title'];?>
" id="preview_title" /><br />
			<input type="text" name="post_desc" maxlength="256" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['post_desc'];?>
" id="preview_summary" /><br />
			<textarea name="post_content" id="post_content"><?php echo $_smarty_tpl->tpl_vars['data']->value['post_content'];?>
</textarea>
		</td>
	</tr>
	<tr>
		<td>Article type</td>
		<td>
			<select name="post_type">
				<option value="personal" <?php if ($_smarty_tpl->tpl_vars['data']->value['post_type'] == "personal") {?>selected<?php }?>>Personal story</option>
				<option value="business"<?php if ($_smarty_tpl->tpl_vars['data']->value['post_type'] == "business") {?>selected<?php }?>>Business</option>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td>Tags</td>
		<td>
			<input type="text" name="post_tags" maxlength="128" style="width:400px;" value="<?php echo $_smarty_tpl->tpl_vars['data']->value['post_tags'];?>
" /><br />
			<em>Separate tags with a comma ','. This will improve search results and connecting to related articles</em>
		</td>
	</tr> 
	<tr>
		<td>Article status</td>
		<td>
			<select name="post_status">
				<option value="draft"<?php if ($_smarty_tpl->tpl_vars['data']->value['post_status'] == "draft") {?>selected<?php }?>>Draft</option>
				<option value="published" <?php if ($_smarty_tpl->tpl_vars['data']->value['post_status'] == "published") {?>selected<?php }?>>Published</option>
				<option value="removed" <?php if ($_smarty_tpl->tpl_vars['data']->value['post_status'] == "removed") {?>selected<?php }?>>Removed</option>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td colspan="2"><input type="submit" name="publish" value="Save changes" style="float:right;" /></td>
	</tr>
</table>
</form><?php }
}
