<?php
	
	if(!empty($_POST))
	{
		$_POST = $class_security->makeSafeArray($_POST);
		$result = $class_football->addGame($_POST);
		if($result[0] === true)
		{
			$smarty->assign("message", "Game results ".$_POST["game_score"]." (".$_POST["game_result"].") has been added.");
			include("games.php");
			exit();
		}
		else
		{
			$smarty->assign("error", "Error occurred: ".$result[1]);
			$smarty->assign("game", $_POST);
		}
	}
	$smarty->assign("players", $class_football->retrievePlayers(false));
	$smarty->assign("opponents", $class_football->retrieveOpponents());
	$smarty->display("games_new.tpl");
	
?>