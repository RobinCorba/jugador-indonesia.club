<?php
	
	if(!empty($_POST))
	{
		$data["user_id"]					= $class_security->makeSafeNumber($_POST["user_id"]);
		$data["user_name_real"]	= $class_security->makeSafeString($_POST["user_name_real"]);
		$data["user_name"]			= $class_security->makeSafeString($_POST["user_name_new"]);
		$data["user_type"]				= $class_security->makeSafeString($_POST["user_type"]);
		$data["user_email"]			= $class_security->makeSafeString($_POST["user_email"]);
		if($_POST["user_pass_new"] != "")
		{
			$data["user_pass"]			= $class_security->encryptStringWithSalt($_POST["user_pass_new"], $class_security->getSalt($data["user_name"]));
		}
		$check_mail 				= true;
		if($data["user_email"] != "")
		{
			$check_mail = $class_security->checkEmail($data["user_email"]);
		}
		if($check_mail == true)
		{
			if($data["user_name_real"] != "" && $data["user_name"] != "")
			{
				if($class_user->editUser($data))
				{
					$smarty->assign("message", "Saved changes.");
					require_once("user_overview.php");
					exit();
				}
				else
				{
					$smarty->assign("error", "Whoopsy Daisy, could not save changes.");
				}
			}
			else
			{
				$smarty->assign("data",	$data);
				$smarty->assign("warning","Please fill in all required fields.");
				$smarty->display("header.tpl");
				$smarty->display("user_new.tpl");
			}
		}
		else
		{
			$smarty->assign("data",	$data);
			$smarty->assign("warning","The given e-mail address is invalid.");
			$smarty->display("header.tpl");
			$smarty->display("user_new.tpl");
		}
	}
	$smarty->assign("data", $class_user->retrieveUserInfo($class_security->makeSafeString($_GET["id"])));
	$smarty->display("header.tpl");
	$smarty->display("user_edit.tpl");
	
?>