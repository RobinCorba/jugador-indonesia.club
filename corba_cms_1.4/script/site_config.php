<?php
	
	if(!empty($_POST))
	{
		$config_data["site_url"]							= $class_security->makeSafeString($_POST["site_url"]);
		$config_data["site_root"]							= $class_security->makeSafeString($_POST["site_root"]);
		$config_data["site_title"]							= $class_security->makeSafeString($_POST["site_title"]);
		$config_data["site_keywords"]				= $class_security->makeSafeString($_POST["site_keywords"]);
		$config_data["site_webmaster_name"]		= $class_security->makeSafeString($_POST["site_webmaster_name"]);
		$config_data["site_webmaster_email"]		= $class_security->makeSafeString($_POST["site_webmaster_email"]);
		$config_data["site_interface"]					= "standard";
		$check_mail 											= true;
		if($config_data["site_webmaster_email"] != "")
		{
			$check_mail = $class_security->checkEmail($config_data["site_webmaster_email"]);
		}
		// Start processing the data
		if(	$config_data["site_url"]	== "" ||
			$config_data["site_root"]	== "")
		{
			$smarty->assign("error", "Site URL and root folder cannot be empty!");
			$smarty->assign("config_data", $config_data);
		}
		elseif($check_mail == false)
		{
			$smarty->assign("error", "The given e-mail address is invalid.");
			$smarty->assign("config_data", $config_data);
		}
		else
		{
			if($class_common->saveSiteConfiguration($config_data))
			{
				$smarty->assign("message", "Site configuration settings saved.");
			}
			else
			{
				$smarty->assign("error", "There was an error saving changes to the site configuration.");
			}
		}
		$smarty->assign("config_data", $config_data);
		
		// Templates
		$smarty->assign("seo_title", "Site configuration of " . OFFICE_NAME);
		$smarty->assign("seo_desc", "Manage the site configuration of " . OFFICE_NAME);
		$smarty->display("header.tpl");
		$smarty->display("site_config.tpl");
		
	}
	else
	{
		$config_data = $class_common->retrieveSiteConfig();
		$smarty->assign("config_data", $config_data);
		
		// Templates
		$smarty->assign("seo_title", "Site configuration of " . OFFICE_NAME);
		$smarty->assign("seo_desc", "Manage the site configuration of " . OFFICE_NAME);
		$smarty->display("header.tpl");
		$smarty->display("site_config.tpl");
	}
	
?>