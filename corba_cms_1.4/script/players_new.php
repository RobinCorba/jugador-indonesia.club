<?php
	
	if(!empty($_POST))
	{
		$_POST = $class_security->makeSafeArray($_POST);
        include("library/class.image.php");
		$class_image = new Image();
		if(!empty($_FILES["player_picture"]))
		{
			$thumb_sizes = array(array(180,240),array(120,160));
			$result = $class_image->uploadImageWithThumbnails($_FILES["player_picture"], $_POST["player_name"], $root."media/images/players/", $thumb_sizes, 91);
			if($result[0] === true)
			{
				$_POST["player_picture"] = $result[1];
			}
		}
        if(!empty($_FILES["player_card_picture"]))
		{
            $thumb_sizes = array(array(120,120));
			$result = $class_image->uploadImageWithThumbnails($_FILES["player_card_picture"], $_POST["player_name"], $root."media/images/player_cards/", $thumb_sizes, 91);
			if($result[0] === true)
			{
				$_POST["player_card_picture"] = $result[1];
			}
		}
		$result = $class_football->addPlayer($_POST);
		if($result[0] === true)
		{
			
			$smarty->assign("message", "Player ".$_POST["player_name"]." has been added to the roster");
			include("players.php");
			exit();
		}
		else
		{
			$smarty->assign("error", "Error occurred: ".$result[1]);
			$smarty->assign("player", $_POST);
		}
	}
	$smarty->assign("positions", $class_football->retrievePositions());
	$smarty->display("players_new.tpl");
	
?>