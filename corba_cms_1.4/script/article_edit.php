<?php
	
    include("library/class.news.php");
    $class_news = new News();
    include("library/class.image.php");
    $class_image = new Image();
    define("DIR_NEWS","media/images/news/");
    
	if(!empty($_POST))
	{
		$data["post_id"]					= $class_security->makeSafeNumber($_POST["post_id"]);
		$data["post_type"]				= $class_security->makeSafeString($_POST["post_type"]);
		$data["post_title"]				= $class_security->makeSafeString($_POST["post_title"]);
		$data["post_content"]			= addslashes($_POST["post_content"]);
		$data["post_desc"]			= $class_security->makeSafeString($_POST["post_desc"]);
		$data["post_tags"]				= trim(strtolower($class_security->makeSafeString($_POST["post_tags"])));
		$data["post_status"]			= $class_security->makeSafeString($_POST["post_status"]);
		if(substr($data["post_tags"], -1) == ",")
		{
			$data["post_tags"] = substr($data["post_tags"], 0, -1);
		}
		// Check if all mandatory fields are filled
		$warning = "";
		if($data["post_title"] == "")
		{
			$warning .= "This post needs a title.<br />";
		}
		elseif($data["post_content"] == "")
		{
			$warning .= "This post needs some content.<br />";
		}
		elseif($data["post_desc"] == "")
		{
			$warning .= "This article needs a description.<br />";
		}
		elseif($data["post_tags"] == "")
		{
			$warning .= "This article needs some keywords.<br />";
		}
		if($warning !== "")
		{
			$smarty->assign("warning", $warning);
			$smarty->assign("data", $data);
		}
		elseif($class_news->editArticle($data, $root))
		{
			// Process the changes
			$smarty->assign("message", "Saved changes to '$data[post_title]'.");
			require_once("article_overview.php");
			exit();
		}
		else
		{
			$smarty->assign("error","An error occured, no changes saved to '$data[post_title]'.");
			$smarty->assign("data", $data);
		}
	}
	
	// Show the template
	if(empty($data))
	{
		$smarty->assign("data", $class_news->retrieveArticle($class_security->makeSafeNumber($_GET["id"])));
	}
	$smarty->assign("seo_title", "Edit article for " . OFFICE_NAME);
	$smarty->assign("seo_desc", "Edit an article for " . OFFICE_NAME);
	$smarty->display("header.tpl");
	$smarty->display("article_edit.tpl");
	
?>