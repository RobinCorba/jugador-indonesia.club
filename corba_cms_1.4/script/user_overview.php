<?php
	
	if(!empty($_GET["del"]))
	{
		if($class_user->deleteUser($class_security->makeSafeNumber($_GET["del"])))
		{
			$smarty->assign("message","User has been deleted.");
		}
		else
		{
			$smarty->assign("error","Whoopsy Daisy, could not delete this user.");
		}
	}
	if(!empty($_GET["reset"]))
	{
		$new_pass = $class_user->generatePassword();
		if($class_user->savePassword($class_security->makeSafeNumber($_GET["reset"]), $class_security->encryptString($new_pass)))
		{
			$smarty->assign("message","Password has been reset! New password is: $new_pass");
		}
		else
		{
			$smarty->assign("error","Could not reset the password.");
		}
	}
	$smarty->assign("users",$class_user->retrieveAllUsers(0,0));
	$smarty->display("header.tpl");
	$smarty->display("user_overview.tpl");
?>