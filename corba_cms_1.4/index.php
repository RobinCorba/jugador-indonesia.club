<?php

	session_start();
	require_once("admin_config.php");
	
	if(!empty($_POST["user_name"]) && !empty($_POST["user_pass"]))
	{
        // User is logging in
		$data["user_name"]	= $class_security->makeSafeString($_POST["user_name"]);
		$data["user_pass"]	= $class_security->encryptStringWithSalt($_POST["user_pass"], $class_security->getSalt($_POST["user_name"]));
		$user_data = $class_session->checkLogin($data);
		if($user_data != false)
		{
			$class_session->startSession($user_data, $class_security->createUniqueSessionKey($user_data["admin_name"]));
			$smarty->assign("message", "You logged in successfully. Your last login was at $user_data[admin_last_login_real] hrs)");
            $p = "home";
		}
		else
		{
			$smarty->assign("error", "Username and password combination is wrong.");
		}
	}
    
    // Check the active session
    if($class_session->checkSession())
    {
        if(isset($_SESSION["admin_type"]))
        {
            $smarty->assign("user_type", $_SESSION["admin_type"]);
        }
    }
    else
    {
        // Session failed
    }
    
	if(empty($_GET["p"]) && empty($_GET["m"]))
    {
        if($class_session->checkSession())
        {
            header("Location: /admin/home");
        }
        else
        {
            header("Location: /admin/login");
        }
    }
    
    if(!empty($_GET["m"]))
    {
        // Retrieve the module
        if(empty($_GET["p"]))
        {
            if(file_exists("modules/".$_GET["m"]."/index.php"))
            {
                require_once("modules/".$_GET["m"]."/index.php");
            }
            else
            {
                $smarty->display("404.tpl");
            }
        }
        else
        {
            if(file_exists("modules/".$_GET["m"]."/".$_GET["p"].".php"))
            {
                require_once("modules/".$_GET["m"]."/".$_GET["p"].".php");
            }
            else
            {
                $smarty->display("404.tpl");
            }
        }
    }
    else
    {
        if(empty($_GET["p"]))
        {
            if($class_session->checkSession())
            {
                $p = "home";
            }
            else
            {   
                $p = "login";
            }
        }
        elseif(empty($p))
        {
            $p = $_GET["p"];
        }
        $smarty->assign("p", $p);
        if($p == "logout")
        {
            $admin_id = "";
            if(!empty($_SESSION["admin_idd"]))
            {
                $admin_id = $class_security->makeSafeNumber($_SESSION["admin_id"]);
            }
            if($class_session->terminateSession($admin_id))
            {
                $smarty->assign("message","You have been logged out.");
            }
            $smarty->display("login.tpl");
        }
        elseif($class_session->checkSession())
        {
            if(file_exists("script/".$p.".php"))
            {
                require_once("script/".$p.".php");
            }
            elseif(file_exists("templates/".$p.".tpl"))
            {
                $smarty->display($p.".tpl");
            }
            else
            {
                $smarty->display("404.tpl");
            }
        }
        else
        {
            if($p != "" && $p != "logout" && $p != "login" && $p != "home")
            {
                $smarty->assign("warning","You are not logged in as an administrator.");
            }
            $smarty->display("login.tpl");
        }
    }
    
?>