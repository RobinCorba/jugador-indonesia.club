<?php
	
	// PHP error handling
	ini_set('display_errors', true);
	ERROR_REPORTING(E_ALL);
	//ini_set('display_errors', false);
	//ERROR_REPORTING(NULL);
	date_default_timezone_set('Asia/Jakarta');
    
    // Define constants
	define("URL_ROOT",      "http://localhost/jugador-indonesia.club/");
	define("DIR_ROOT",      "C:/Users/Robin Corba/Sites/jugador-indonesia.club/");
    define("DIR_MODULE",    DIR_ROOT."admin/modules/");
    
    
    define("DIR_IMG_NEWS",          DIR_ROOT."media/images/news/");
	
	// PHP libraries
    require_once("library/class.database.php");
	require_once("./admin/library/class.statistics.php");
	require_once("./admin/library/class.modules.php");
	$class_stats = new Statistics();
    $class_news = Modules::load("news");
	
    
	// Basic settings
	$url				= "http://www.jugador-indonesia.club/";
	$root				= "/var/www/html/jugador-indonesia.club/";
	
	// Smarty setup
	include 'smarty/Smarty.class.php';
	$smarty = new Smarty();
	$smarty->template_dir		= $root . "templates/";
	$smarty->compile_dir		= $root . "compile/";
	$smarty->caching 		    = 0;
	$smarty->debugging 			= false;
    
	// Miscellaneous setup
	$smarty->assign("temp_dir",	    $smarty->template_dir);
	$smarty->assign("base_dir",	    $url);
	$smarty->assign("site_title",	"Jugador CF");
	$smarty->assign("site_desc",	"");
	$smarty->assign("site_tags",	"");

?>