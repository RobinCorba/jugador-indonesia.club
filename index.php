<?php

	session_start();
	require_once("config.php");
	$p      = "home";
	$stats  = array();
	$news   = array();
	$stats["games"] = $class_stats->retrieveGameStatistics();
	$news           = $class_news->retrieveAllArticles();
	$smarty->assign("stats",    $stats);
	$smarty->assign("news",     $news);
	$smarty->display("home.tpl");
	
?>