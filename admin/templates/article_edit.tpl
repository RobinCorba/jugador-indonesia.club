<h1>Edit article</h1>
<p>Make changes to an article of the news section</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<form method="post" action="{$base_url}?p=article_edit&id={$data.post_id}">
<input type="hidden" name="post_id" value="{$data.post_id}" />
<table id="details" style="width:800px;">
	<tr class="odd">
		<td colspan="2" style="font-weight:normal; font-size:11px;">
			<input type="text" name="post_title" maxlength="128" value="{$data.post_title}" id="preview_title" /><br />
			<input type="text" name="post_desc" maxlength="256" value="{$data.post_desc}" id="preview_summary" /><br />
			<textarea name="post_content" id="post_content">{$data.post_content}</textarea>
		</td>
	</tr>
	<tr>
		<td>Article type</td>
		<td>
			<select name="post_type">
				<option value="personal" {if $data.post_type == "personal"}selected{/if}>Personal story</option>
				<option value="business"{if $data.post_type == "business"}selected{/if}>Business</option>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td>Tags</td>
		<td>
			<input type="text" name="post_tags" maxlength="128" style="width:400px;" value="{$data.post_tags}" /><br />
			<em>Separate tags with a comma ','. This will improve search results and connecting to related articles</em>
		</td>
	</tr> 
	<tr>
		<td>Article status</td>
		<td>
			<select name="post_status">
				<option value="draft"{if $data.post_status == "draft"}selected{/if}>Draft</option>
				<option value="published" {if $data.post_status == "published"}selected{/if}>Published</option>
				<option value="removed" {if $data.post_status == "removed"}selected{/if}>Removed</option>
			</select>
		</td>
	</tr>
	<tr class="odd">
		<td colspan="2"><input type="submit" name="publish" value="Save changes" style="float:right;" /></td>
	</tr>
</table>
</form>