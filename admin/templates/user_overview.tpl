<h1>All accounts</h1>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a href="{$base_dir}?p=user_new">Create new user</a>
</div>
{if $users != ""}
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Admin name</td>
		<td class="title" style="width:150px;">Account type</td>
		<td class="title" style="width:150px;">Registered</td>
		<td class="title" style="width:200px;">Last login</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
{section name=this loop=$users}
	{if $smarty.section.this.iteration is odd}
	<tr class="odd">
	{else}
	<tr>
	{/if} 
		<td>{$users[this].admin_name_real}</td>
		<td>{$users[this].admin_type}</td>
		<td>{$users[this].admin_date_created_real}</td>
		<td>{$users[this].admin_last_login_real} hrs)</td>
		<td><a href="{$base_dir}?p=user_edit&id={$users[this].admin_id}">Edit</a> | <a href="{$base_dir}?p=user_overview&reset={$users[this].admin_id}">Reset password</a>{if $user_type == "superadmin"} | <a href="javascript:confirmDelete('{$users[this].admin_name_real}','{$base_dir}?p=user_overview&del={$users[this].admin_id}')">Delete</a>{/if}</td>
	</tr>
{/section}
	</tbody>
</table>
{else}
	<p>No users are available. <em>(this should not be possible, please contact Robin Corba)</em></p>
{/if}