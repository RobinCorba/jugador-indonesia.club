<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>{if $seo_title != ""}{$seo_title}{else}Administration{/if}</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="{if $seo_desc}{$seo_desc}, {/if}Jugador CF" />
		<meta name="keywords" content="Corba CMS" />
		<meta name="robots" content="nofollow, noindex" />
		<meta name="copyright" content="Robin Corba 2012-2016" />
		
		{if $p == "api_overview"}
			<link rel="stylesheet" type="text/css" href="{$base_url}templates/css/style_api.css" />
		{else}
			<link rel="stylesheet" type="text/css" href="{$base_url}templates/css/style.css" />
		{/if}
		<link rel="shortcut icon" href="{$base_url}favicon.ico" />
        <!--script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-2.2.3.min.js"></script-->
		<script type="text/javascript" src="{$base_url}js/class.ajax.js"></script>
	</head>
	<body>
		{if $p != "api_overview"}
			<div id="header"><img src="{$base_url}templates/img/theboumas_logo.jpg" alt="Corba CMS built for www.theboumas.net" /></div>
			<div id="container">
				<div id="nav">
					<a href="{$base_url}news">News</a>
					<a href="{$base_url}?p=players">Players</a>
					<a href="{$base_url}?p=opponents">Opponents</a>
					<a href="{$base_url}?p=games">Games</a>
					<a href="{$base_url}?p=statistics">Statistics</a>
					{if $user_type == "superadmin"}
					<a href="{$base_url}?p=user_overview">CMS Users</a>
					<a href="{$base_url}?p=site_config">Site configuration</a>
					<a href="{$base_url}?p=api_overview">API Documentation</a>
					{/if}
					<a href="{$base_url}logout">Logout</a>
				</div>
				<div id="content">
			{/if}