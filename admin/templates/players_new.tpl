{include file="header.tpl"}
<h1>{if isset($edit) && $edit == "yes"}Edit {$player.player_name}{else} Add new player{/if}</h1>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
<link rel="stylesheet" type="text/css" href="../css/cards.css">
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<form method="post" enctype='multipart/form-data'>
	<input type="hidden" name="player_id" maxlength="4" {if isset($player)}value="{$player.player_id}"{/if} />
	<table class="details">
		<tbody>
			<tr>
				<td>Full Name</td>
				<td><input type="text" name="player_name" maxlength="128" placeholder="Name of the player" {if isset($player)}value="{$player.player_name}"{/if} /></td>
                <td rowspan="6" style="vertical-align:top;">
                    Card
                    <div class="player_card {$player.player_status}">
                        <div class="number">{$player.player_no}</div>
                        <div class="position"><input type="text" name="player_card_position" maxlength="5" {if isset($player)}value="{$player.player_card_position}"{/if} style="background:none; font-size:18px; text-align:center; width:50px;" /></div>
                        <div class="name">{$player.player_display_name}</div>
                        <div class="stats" id="stat-1">
                            <span>Lorem</span>
                            <span>Ipsum</span>
                        </div>
                        <div class="stats" id="stat-2">
                            <span>Lorem</span>
                            <span>Ipsum</span>
                        </div>
                        <div class="stats" id="stat-3">
                            <span>Lorem</span>
                            <span>Ipsum</span>
                        </div>
                        <div class="stats-footer">{$player.player_status_readable}</div>
                        <div id="img_card_upload_preview" class="picture" style="background-image:url('../media/images/player_cards/{if isset($player)}{$player.player_card_picture}{else}default_card_picture.png{/if}');"></div>
                    </div>
                </td>
			</tr>
            <tr>
                <td>Display Name</td>
				<td><input type="text" name="player_display_name" maxlength="128" placeholder="Displated name of the player" {if isset($player)}value="{$player.player_display_name}"{/if} /></td>
            </tr>
			<tr>
				<td>No.</td>
				<td><input type="text" name="player_no" maxlength="4" placeholder="Number of the player" {if isset($player)}value="{$player.player_no}"{/if} style="width:20px; text-align:center;" /></td>
			</tr>
			<tr>
				<td>Position(s)</td>
				<td>
					<select name="player_positions[]" multiple style="height:224px;">
					{foreach from="$positions" item="p"}
						<option value="{$p.position_id}"
						{foreach from=$player.player_positions item="pos"}
						{if $pos.position_id == $p.position_id}selected{/if}
						{/foreach}
						>{$p.position_code}</option>
					{/foreach}
					</select>
				</td>
			</tr>
			<tr>
				<td>Status</td>
				<td>
					<select name="player_status">
                        <option value="former" {if isset($player) && $player.player_status == "former"}selected{/if}>Former player</option>
                        <option value="inactive" {if isset($player) && $player.player_status == "inactive"}selected{/if}>Inactive player</option>
						<option value="new" {if isset($player) && $player.player_status == "new"}selected{/if}>New player</option>
						<option value="active" {if isset($player) && $player.player_status == "active"}selected{/if}>Active player</option>
						<option value="top" {if isset($player) && $player.player_status == "top"}selected{/if}>Top player</option>
                        <option value="legendary" {if isset($player) && $player.player_status == "legendary"}selected{/if}>Legendary player</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Playing since</td>
				<td>
					<select name="player_since_month">
						<option value="01" {if isset($player) && $player.player_since_month == "01"}selected{/if}>Jan.</option>
						<option value="02" {if isset($player) && $player.player_since_month == "02"}selected{/if}>Feb.</option>
						<option value="03" {if isset($player) && $player.player_since_month == "03"}selected{/if}>Mar.</option>
						<option value="04" {if isset($player) && $player.player_since_month == "04"}selected{/if}>Apr.</option>
						<option value="05" {if isset($player) && $player.player_since_month == "05"}selected{/if}>May.</option>
						<option value="06" {if isset($player) && $player.player_since_month == "06"}selected{/if}>Jun.</option>
						<option value="07" {if isset($player) && $player.player_since_month == "07"}selected{/if}>Jul.</option>
						<option value="08" {if isset($player) && $player.player_since_month == "08"}selected{/if}>Aug.</option>
						<option value="09" {if isset($player) && $player.player_since_month == "09"}selected{/if}>Sep.</option>
						<option value="10" {if isset($player) && $player.player_since_month == "10"}selected{/if}>Oct.</option>
						<option value="11" {if isset($player) && $player.player_since_month == "11"}selected{/if}>Nov.</option>
						<option value="12" {if isset($player) && $player.player_since_month == "12"}selected{/if}>Dec.</option>
					</select>
					<input type="text" maxlength="4" name="player_since_year" placeholder="Year" {if isset($player)}value="{$player.player_since_year}"{/if} style="text-align:center; width:40px;" />
				</td>
			</tr>
            <tr>
				<td>Retired on (if former player)</td>
				<td>
					<select name="player_stopped_month">
						<option value="01" {if isset($player) && $player.player_stopped_month == "01"}selected{/if}>Jan.</option>
						<option value="02" {if isset($player) && $player.player_stopped_month == "02"}selected{/if}>Feb.</option>
						<option value="03" {if isset($player) && $player.player_stopped_month == "03"}selected{/if}>Mar.</option>
						<option value="04" {if isset($player) && $player.player_stopped_month == "04"}selected{/if}>Apr.</option>
						<option value="05" {if isset($player) && $player.player_stopped_month == "05"}selected{/if}>May.</option>
						<option value="06" {if isset($player) && $player.player_stopped_month == "06"}selected{/if}>Jun.</option>
						<option value="07" {if isset($player) && $player.player_stopped_month == "07"}selected{/if}>Jul.</option>
						<option value="08" {if isset($player) && $player.player_stopped_month == "08"}selected{/if}>Aug.</option>
						<option value="09" {if isset($player) && $player.player_stopped_month == "09"}selected{/if}>Sep.</option>
						<option value="10" {if isset($player) && $player.player_stopped_month == "10"}selected{/if}>Oct.</option>
						<option value="11" {if isset($player) && $player.player_stopped_month == "11"}selected{/if}>Nov.</option>
						<option value="12" {if isset($player) && $player.player_stopped_month == "12"}selected{/if}>Dec.</option>
					</select>
					<input type="text" maxlength="4" name="player_stopped_year" placeholder="YYYY" {if isset($player)}value="{$player.player_stopped_year}"{/if} style="text-align:center; width:40px;" />
				</td>
			</tr>
			<tr>
				<td>Picture</td>
				<td>
					<img id="img_upload_preview" src="{if isset($player)}{$site_dir}media/images/players/thumb_180_{$player.player_picture}{/if}" alt="No preview" class="profile_picture" />
					<input type="file" name="player_picture" {if isset($player)}value="{$player.player_picture}"{/if} id="img_upload" />
				</td>
                <td rowspan="2">
                    Card Picture
					<input type="file" name="player_card_picture" {if isset($player)}value="{$player.player_card_picture}"{/if} id="img_card_upload" />
                </td>
			</tr>
			<tr>
				<td coslpan="2"><input type="submit" value="Save {if isset($edit)}changes{else}new player{/if}" style="float:right;" /></td>
			</tr>
		</tbody>
	</table>
</form>
{include file="footer.tpl"}