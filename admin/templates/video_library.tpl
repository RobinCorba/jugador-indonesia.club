<h1>Video Library</h1>
<p>Manage all Video here.</p>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
{literal}
<style>
/*fieldset{display:none};*/
</style>
<script type="text/javascript">
	function toggleSet(rad){
		var type = rad.value;
		for(var k=0;elm=rad.form.elements[k];k++)
			if(elm.className=='item')
			elm.style.display = elm.id==type? 'inline':'';
		
	}
	function sure(){
		if(confirm('Are you sure want to delete?'))
			return true
		return false	
	}
</script>
{/literal}
<form action="{$base_dir}?p=video_library" method="post" enctype="multipart/form-data">
 <!--Choose : <select name="selector" onchange="toggleSet(this)" >
  <option value="">- select -</option> 
  <option value="1"> Upload video </option>
  <option value="2"> Upload video link </option>
 </select>
  <div style="margin:10px">&nbsp;</div>
<fieldset style="width:500px;" id="1" class="item">
	<legend>Upload new Video</legend>
	<table>
		<tr>
			<td><input type="file" name="file" style="width:220px;" /></td>
			<td>SWF (Max. 5 MB)</td>
		</tr>
		<tr>
			<td><input type="text" name="file_name" maxlength="50" style="width:200px;"/></td>
			<td>File name (optional)</td>
		</tr>
		<tr>
			<td colspan="2">
            <input type="hidden" name="action1" value="post_file" />
            <input type="submit" name="submit" value="Upload new video" /></td>
		</tr>
	</table>
</fieldset>-->
<fieldset style="width:500px;" id="2" class="item">
	<legend>Upload new Video</legend>
	<table>
		<tr>
			<td><input type="text" name="file_link" style="width:220px;" id="file_link" /></td>
			<td>eg : http://www.youtube.com/watch?v=xxx </td>
		</tr>
        <tr>
			<td><input type="text" name="file_name" maxlength="50" style="width:200px;"/></td>
			<td>Video title</td>
		</tr>
			<tr>
			  <td colspan="2">Descriptions</td>
	  </tr>
			<tr>
			  <td colspan="2"><input type="text" name="file_desc" style="width:400px;" id="file_desc"></td>
	  </tr>
			<td colspan="2">
            <input type="hidden" name="action2" value="post_file_link" />
            <input type="submit" name="submit" value="Upload new video" /></td>
		</tr>
	</table>
</fieldset>
</form>
<h2>All Video</h2>
<div style="float:right;margin-right:12px;">* copy and paste the Video Code to article/page post. Ex : <strong>[youtube=36GJL51y7Cs]</strong></div>
<table id="grid" class="display">
	<thead>
    <tr style="background:#CCC;">
		<td style="width:250px;">Title</td>
        <td style="width:150px;">Description</td>
        <td style="width:150px;">Video Code</td>
		<td style="width:500px;">Youtube URL</td>
		<td style="width:150px;">Uploaded on</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
    </thead>
    <tbody>
{foreach from="$video" item="video"}
   
	<tr {cycle values=',class="odd"'}>
		<td>{$video.vid_name}</td>
        <td>{$video.vid_description}</td>
        <td>[youtube={$video.vid_brackets}]</td>
		<td><a href="{$video.vid_url}" target="_blank">{$video.vid_url}</a></td>
		<td>{$video.date_uploaded_real} hrs</td>
		<td>
        <a href="{$base_dir}?p=video_library&edit={$video.vid_id}">Edit</a>&nbsp;&nbsp;
        <a href="{$base_dir}?p=video_library&del={$video.vid_id}" onclick="return sure()">Delete</a>
        </td>
	</tr>
   
{/foreach}
    </tbody>
</table>