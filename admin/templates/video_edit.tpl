<h1>Video Library</h1>
<p>Manage all Video here.</p>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
{literal}
<style>
/*fieldset{display:none};*/
</style>
<script type="text/javascript">
	function toggleSet(rad){
		var type = rad.value;
		for(var k=0;elm=rad.form.elements[k];k++)
			if(elm.className=='item')
			elm.style.display = elm.id==type? 'inline':'';
		
	}
	function sure(){
		if(confirm('Are you sure want to delete?'))
			return true
		return false	
	}
</script>
{/literal}
<form action="{$base_dir}?p=video_library" method="post" enctype="multipart/form-data">
 <!--Choose : <select name="selector" onchange="toggleSet(this)" >
  <option value="">- select -</option> 
  <option value="1"> Upload video </option>
  <option value="2"> Upload video link </option>
 </select>
  <div style="margin:10px">&nbsp;</div>
<fieldset style="width:500px;" id="1" class="item">
	<legend>Upload new Video</legend>
	<table>
		<tr>
			<td><input type="file" name="file" style="width:220px;" /></td>
			<td>SWF (Max. 5 MB)</td>
		</tr>
		<tr>
			<td><input type="text" name="file_name" maxlength="50" style="width:200px;"/></td>
			<td>File name (optional)</td>
		</tr>
		<tr>
			<td colspan="2">
            <input type="hidden" name="action1" value="post_file" />
            <input type="submit" name="submit" value="Upload new video" /></td>
		</tr>
	</table>
</fieldset>-->
<fieldset style="width:500px;" id="2" class="item">
	<legend>Edit Video</legend>
	<table>
		<tr>
			<td><input type="text" name="file_link" style="width:220px;" id="file_link" value="{$video.vid_url}" /></td>
			<td>eg : http://www.youtube.com/watch?v=xxx </td>
		</tr>
        <tr>
			<td><input type="text" name="file_name" maxlength="50" style="width:200px;" value="{$video.vid_name}" /></td>
			<td>Video title</td>
		</tr>
			<tr>
			  <td colspan="2">Descriptions</td>
	  </tr>
			<tr>
			  <td colspan="2"><input type="text" name="file_desc" style="width:200px;" id="file_desc" value="{$video.vid_description}"></td>
	  </tr>
			<td colspan="2">
            <input name="ori_id" type="hidden" value="{$video.vid_id}"/>
            <input type="hidden" name="action2" value="update" />
            <input type="submit" name="submit" value="Update video" /></td>
		</tr>
	</table>
</fieldset>
</form>
