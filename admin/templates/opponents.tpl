{include file="header.tpl"}
<h1>Opponent teams</h1>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a href="{$base_dir}?p=opponents_new">Add opponent</a>
</div>
{if $opponents != ""}
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Name</td>
		<td class="title" style="width:200px;">Color</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
{foreach from="$opponents" item="o"}
		<td>{$o.opponent_name}</td>
		<td>{$o.opponent_color}</td>
		<td><a href="{$base_dir}?p=opponents_edit&id={$o.opponent_id}">Edit</a></td>
	</tr>
{/foreach}
	</tbody>
</table>
{else}
	<p>No opponents available.</p>
{/if}
{include file="footer.tpl"}