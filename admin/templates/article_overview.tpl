<h1>Weblog Management</h1>
<p>An overview of all articles.</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a id="new" href="{$base_url}?p=article_new">New article</a>
</div>
{if $articles == ""}
	<p>No articles written yet.</p>
{else}
	<table id="grid" class="display">
		<thead>
		<tr>
			<td>Article title</td>
			<td>Type</td>
			<td>Published</td>
			<td>Status</td>
			<td>&nbsp;</td>
		</tr>
		</thead>
		<tbody>
	{foreach from=$articles item="article"}
		<tr {cycle values=',class="odd"'}>
			<td style="width:500px;">{$article.post_title_shortened}</td>
			<td style="width:80px;">{$article.post_type}</td>
			<td style="width:220px;">{$article.post_date_published_converted} {$article.post_time_published_real} hrs</td>
			<td style="width:60px;">{$article.post_status}</td>
			<td style="width:80px;"><a href="{$base_url}?p=article_edit&id={$article.post_id}">Edit</a> | <a href="javascript:confirmDelete('{$article.post_title}','{$base_url}?p=article_overview&del={$article.post_id}')">Delete</a></td>
		</tr>
	{/foreach}
		</tbody>
	</table>
{/if}