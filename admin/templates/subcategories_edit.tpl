<h1>Products Categories</h1>
<p>Manage Products Categories for the Pizza Hut Indonesia website</p>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
  
 <form action="{$base_dir}?p=categories" method="post" enctype="multipart/form-data">
 			
	        <table width="300" border="0">
            <tr>
	            <td>
               Category
                </td>
              </tr>
            <tr>
	            <td>
                <select name="prodcat_id">
                	<option value="0">- no category -</option>
                	{foreach from="$categories" item="row"}
                		<option value="{$row.prodcat_id}" {if $SELECTEDSUBCATEGORY.prodcat_id==$row.prodcat_id}selected="selected"{else}{/if}>{$row.prodcat_name}</option>
                    {/foreach}    
                </select>
                </td>
              </tr>
	          <tr>
	            <td>New Subcategory<br /><input name="subcat_name" type="text" id="subcat_name" size="50" value="{$SELECTEDSUBCATEGORY.subcat_name}" /></td>
              </tr>
	          <tr>
	            <td>
                <input type="hidden" name="subcat_id" id="subcat_id" value="{$SELECTEDSUBCATEGORY.subcat_id}" />
                <input type="hidden" name="action" id="action" value="editsubcat" />
                <input type="submit" name="button" id="button" value="Submit" /></td>
              </tr>
            </table>
          </form>
<br />
<table id="grid" class="display">
	<thead>
	<tr>
		<td style="width:300px;">Categories Name</td>
		<td style="width:200px;">Subcategory</td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
{foreach from="$categories" item="row"}
	<tr {cycle values=',class="odd"'}>
		<td valign="top">{$row.prodcat_name}</td>
		<td valign="top">
        <ul>  
        	        	  
                {foreach from="$subcat" item="sub"}
                    {if $row.prodcat_id == $sub.prodcat_id}
                        <li>{$sub.subcat_name}</li>               
                    {/if}
                {/foreach}   
                    
        </ul>
        </td>
		<td valign="top"><a href="{$base_dir}?p=categories&catid={$row.prodcat_id}">Edit</a> | <a href="{$base_dir}?p=categories&delcat={$row.prodcat_id}" onclick="return sure()">Delete</a></td>
	</tr>
{/foreach}
	</tbody>
</table>
<h3>Products Subcategories</h3>
<table id="grid" class="display" border="1">
	<thead>
	<tr bgcolor="#CCCCCC">
		<td style="width:300px;"><strong>SubCategories Name</strong></td>
		<td style="width:200px;"><strong>Category</strong></td>
		<td style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
{foreach from="$subcat" item="row"}
	<tr {cycle values=',class="odd"'}>
		<td>{$row.subcat_name}</td>
		<td>{$row.prodcat_name}</td>
		<td><a href="{$base_dir}?p=categories&amp;subcatid={$row.subcat_id}">Edit</a> | <a href="{$base_dir}?p=categories&amp;delsubcat={$row.subcat_id}" onclick="return sure()">Delete</a></td>
	</tr>
{/foreach}
	</tbody>
</table>