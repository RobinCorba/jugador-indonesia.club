{include file="header.tpl"}
<h1>All recorded games</h1>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a href="{$base_dir}?p=games_new">Add game results</a>
</div>
{if $games != ""}
<table id="grid" class="display">
	<thead>
	<tr>
		<td class="title" style="width:200px;">Date</td>
		<td class="title" style="width:200px;">Opponent</td>
		<td class="title" style="width:200px;">Score</td>
		<td class="title" style="width:200px;">Result</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
{foreach from="$games" item="g"}
		<td>{$g.game_date_year}-{$g.game_date_month}-{$g.game_date_day}</td>
		<td>{$g.game_opponent.opponent_name}</td>
		<td>{$g.game_score}</td>
		<td>{$g.game_result}</td>
		<td><a href="{$base_dir}?p=game_edit&id={$g.game_id}">Edit</a></td>
	</tr>
{/foreach}
	</tbody>
</table>
{else}
	<p>No games available.</p>
{/if}
{include file="footer.tpl"}