{include file="header.tpl"}
<h1>Team Roster</h1>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
<div id="actions">
	<a href="{$base_dir}?p=players_new">Add player</a>
</div>
{if $players != ""}
<table id="grid" class="display">
	<thead>
	<tr>
        <td class="title" style="width:100px;">Photo</td>
		<td class="title">Name</td>
		<td class="title">#</td>
		<td class="title">Position</td>
		<td class="title">Status</td>
		<td class="title" style="width:100px;">&nbsp;</td>
	</tr>
	</thead>
	<tbody>
{foreach from="$players" item="p"}
		<td>
            <img src="{$site_dir}media/images/players/thumb_120_{$p.player_picture}" alt="{$p.player_name}" class="profile_picture icon" style="float:left; margin-right:5px;" />
            <img src="{$site_dir}media/images/player_cards/{$p.player_card_picture}" alt="{$p.player_name}" style="width:50px; height:auto;" />
        </td>
        <td>
            <span style="font-size:16px;">{$p.player_name}</span><br />
            <em>{$p.player_display_name}</em>
        </td>
		<td>{$p.player_no}</td>
		<td>
		{foreach from=$p.player_positions item="position"}
			{$position.position_code},
		{/foreach}
		</td>
		<td>{$p.player_status}</td>
		<td><a href="{$base_dir}?p=players_edit&id={$p.player_id}">Edit</a></td>
	</tr>
{/foreach}
	</tbody>
</table>
{else}
	<p>No players are available.</p>
{/if}
{include file="footer.tpl"}