<div id="header">Corba CMS - API documentation (version April 12th, 2013, generated on {$today})</div>
<div id="container">
	<div id="nav">
	<h2>Library</h2>
		{section name="this" loop="$library"}
			<a href="{$base_dir}?p=api_overview&class={$library[this]}">{$library[this]}</a><br />
		{/section}
	</div>
	<div id="class">
		{if $class_name != ""}
			<h1>{$class_name} class</h1>
			{foreach from="$functions" item="this"}
				<hr />
				<h2>Function {$this.name_full})</h2>
				{if $this.desc != ""}
					<span class="desc">{$this.desc}</span>
				{else}
					<span class="desc"><em>Missing documentation</em></span>
				{/if}
				{if $this.return_type != ""}
					<strong>Returns</strong><br />
					<span class="return">
						<a class="type">{$this.return_type}</a>	<a class="desc">{$this.return_desc}</a>
					</span>
				{/if}
				{if $this.params != ""}
					<strong>Input variables</strong><br />
					{foreach from=$this.params item=param}
						<span class="param">
							<a class="type">{$param.type}</a> {$param.name}<br />
							<a class="desc">{$param.desc}</a>
						</span>
					{/foreach}
				{else}
				
				{/if}
			{/foreach}
		{/if}
		{if $error}{$error}{/if}
	</div>
</div>