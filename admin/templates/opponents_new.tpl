{include file="header.tpl"}
<h1>Add opponent</h1>
{if $error}<p id="error">{$error}</p>{/if}
{if $warning}<p id="warning">{$warning}</p>{/if}
{if $message}<p id="message">{$message}</p>{/if}
<form method="post">
	<input type="hidden" name="opponent_id" maxlength="4" {if isset($opponent)}value="{$opponent.opponent_id}"{/if} />
	<table class="details">
		<tbody>
			<tr>
				<td>Opponent name</td>
				<td><input type="text" name="opponent_name" maxlength="128" placeholder="Name of the opponent team" {if isset($opponent)}value="{$opponent.opponent_name}"{/if} /></td>
			</tr>
			<tr>
				<td>Opponent color</td>
				<td><input type="text" name="opponent_color" maxlength="64" placeholder="Color of the opponent team's jersey" {if isset($opponent)}value="{$opponent.opponent_color}"{/if} /></td>
			</tr>
			<tr>
				<td coslpan="2"><input type="submit" value="Save" /></td>
			</tr>
		</tbody>
	</table>
</form>
{include file="footer.tpl"}