<h1>Create new article</h1>
<p>Write a new article for the news section</p>
{if !empty($error)}<p id="error">{$error}</p>{/if}
{if !empty($warning)}<p id="warning">{$warning}</p>{/if}
{if !empty($message)}<p id="message">{$message}</p>{/if}
<form enctype="multipart/form-data" method="post" action="{$base_url}?p=article_new">
<table id="details" style="width:800px;">
	<tr class="odd">
		<td style="width:220px;">Category</td>
		<td>
			<select name="article_category">
				{foreach from=$categories item="category"}
				<option value="{$category.category_id}" {if !empty($data.category_id) && $data.category_id == $category.category_id}selected{/if}>{$category.category_title}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td>Article title</td>
		<td>
			<input type="text" name="article_title" maxlength="128" style="width:350px;" {if !empty($data.article_title)}value="{$data.article_title}"{/if} /><br />
			<em>Appears on top of the page, browser tab and search engines.</em>
		</td>
	</tr>
	<tr class="odd">
		<td>Picture</td>
		<td>
			<input name="article_picture" type="file" accept="image/*" /><br />
			<em>Minimum size 340 x 280 pixels (max. 2MB)</em>
		</td>
	</tr>
	<tr>
		<td colspan="2">Article content</td>
	</tr>
	<tr>
		<td colspan="2">
			<textarea name="article_content" style="width:800px; height:600px;">{if !empty($data.article_content)}{$data.article_content}{/if}</textarea>
		</td>
	</tr>
	<tr class="odd">
		<td>Summary (or description)</td>
		<td>
			<input type="text" name="article_description" style="width:500px;" value="{if !empty($data.article_description)}{$data.article_description}{/if}" /><br />
			<em>A paragraph; This will help search engines (Google, Bing etc.) to find this article.</em>
		</td>
	</tr>
	<tr>
		<td>Tags</td>
		<td>
			<input type="text" name="article_keywords" maxlength="256" style="width:400px;" value="{if !empty($data.article_keywords)}{$data.article_keywords}{/if}" /><br />
			<em>Separate tags with a comma ','. This will improve search results and connecting to related articles</em>
		</td>
	</tr> 
	<tr class="odd">
		<td><input type="submit" name="draft" value="Save draft" /></td>
		<td><input type="submit" name="publish" value="Publish article" style="float:right;" /></td>
	</tr>
</table>
</form>