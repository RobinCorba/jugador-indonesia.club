<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Administration login</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="description" content="{if $seo_desc}{$seo_desc}, {/if}Publicis S.E. Asia" />
		<meta name="keywords" content="Publicis, Modem, South East Asia, Indonesia, Singapore, Thailand, Phillipines" />
		<meta name="robots" content="follow,index" />
		<meta name="copyright" content="Robin Corba 2012-2016" />
		
		<link rel="shortcut icon" href="{$base_dir}favicon.ico" />
		<link rel="stylesheet" type="text/css" href="{$base_dir}templates/css/style_login.css" />
	</head>
	<body>
		<img id="header_login" src="{$base_dir}templates/img/corba_cms.jpg" alt="Corba CMS version April '13" />
        <div id="login_panel" style="width:350px;">
            <h1 style="color:#B00;">404: Page not found</h1>
            <p>Sorry, the page you are looking for is not available (anymore).</p>
        </div>
    </body>
</html>