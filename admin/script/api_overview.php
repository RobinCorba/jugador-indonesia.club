<?php
	
	// Loop through all .php classes
	if($handle = opendir("library/"))
	{
		while(($file_name = readdir($handle)) !== false)
		{
			$is_php_file = strpos($file_name, ".php");
			if($is_php_file !== false)
			{
				$file_base = explode(".php", $file_name);
				$all_classes[] = $file_base[0];
			}
		}
		closedir($handle);
		$smarty->assign("library", $all_classes);
	}

	// Load a specific class
	$folder		= "library/";
	$class_file	= $_GET["class"] . ".php";
	if($_GET["class"] == "")
	{
		$smarty->assign("error", "Welcome to the API reference documentation.");
	}
	elseif(!file_exists($folder . $class_file))
	{
		$smarty->assign("error", "Could not find the class.");
	}
	else
	{
		$myFile = $folder . $class_file;
		$fh = fopen($myFile, 'r');
		$theData = fread($fh, filesize($myFile));
		fclose($fh);
		$file = explode("<?php", (string) $theData);
		$funcs = explode("	function ", $file[1]);
		
		// Strip { and spaces from class name
		$class_raw		= explode("class ", $funcs[0]);
		$class_raw_2	= explode("{", $class_raw[1]);
		$class_name		= trim(preg_replace('/\s\s+\{/', ' ', $class_raw_2[0]));
		$smarty->assign("class_name", $class_name);
		
		// Loop all functions
		foreach($funcs as $key => $value)
		{
			// Skip the first (includes class name)
			if($key > 0)
			{
				// Function name
				$func_raw			= explode("(", $value);
				$func_name			= $func_raw[0];
				$function["name"]	= $func_name;
				// Function name w/ parameters
				$func_raw				= explode(")", $value);
				$func_name_full			= $func_raw[0];
				$function["name_full"]	= $func_name_full;
				unset($function["desc"]);
				unset($function["return_type"]);
				unset($function["return_desc"]);
				unset($function["params"]);
				
				// Retrieve comments
				$comm_start = strpos($file[1], "@function $func_name");
				if($comm_start !== false)
				{
					$comm_end		= strpos($file[1], "*/", $comm_start);
					$comm_section	= substr($file[1], $comm_start, ($comm_end - $comm_start));
					
					// Retrieve function description
					$func_desc_raw		= explode("@function $func_name ", $comm_section);
					$func_desc_raw_2	= explode("\n", $func_desc_raw[1]);
					$func_desc			= $func_desc_raw_2[0];
					$function["desc"]	= $func_desc;
					
					// Retrieve return value
					$func_return_raw		= explode("@return ", $comm_section);
					$func_return_raw_2		= explode("\n", $func_return_raw[1]);
					$func_return_raw_3		= explode(" ", $func_return_raw_2[0]);
					$return_type			= $func_return_raw_3[0];
					$return_desc_raw		= "";
					for($i = 1; $i < count($func_return_raw_3); $i++)
					{
						$return_desc_raw .= $func_return_raw_3[$i] . " ";
					}
					$return_desc_1			= explode("*", $return_desc_raw);
					$return_desc_2			= trim($return_desc_1[0]);
					$function["return_type"]= $return_type;
					$function["return_desc"]= $return_desc_2;
					
					
					// Retrieve all parameters
					$params_raw = explode("@param ", $comm_section);
					$params_available = false;
					foreach($params_raw as $key => $value)
					{
						if($key > 0)
						{
							$params_available = true;
							$param_raw	= explode(" ", $value);
							$param_type	= $param_raw[0];
							$param_name	= $param_raw[1];
							$param_desc_raw = "";
							for($i = 2; $i < count($param_raw); $i++)
							{
								$param_desc_raw .= $param_raw[$i] . " ";
							}
							$param_desc_raw_2 = explode("*", $param_desc_raw);
							$param_desc = $param_desc_raw_2[0];
							$param["name"]		= $param_name;
							$param["type"]		= $param_type;
							$param["desc"]		= $param_desc;
							$params[]			= $param;
						}
					}
					if($params_available)
					{
						$function["params"]	= $params;
						unset($params);
					}
				}
				$all_functions[] = $function;
			}
		}
		$smarty->assign("functions", $all_functions);
	}
	/* echo"<pre>";
	print_r($all_functions);
	echo"</pre>"; */
	$smarty->assign("today", date("j F Y"));
	$smarty->assign("seo_title", "API documentation");
	$smarty->assign("seo_desc", "Reference to the API");
	$smarty->display("header.tpl");
	$smarty->display("api_overview.tpl");

?>