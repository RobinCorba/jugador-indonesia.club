<?php
	if(!empty($_POST))
	{
	
		$data["user_name_real"]	= $class_security->makeSafeString($_POST["user_name_real"]);
		$data["user_name"]			= $class_security->makeSafeString($_POST["user_name_new"]);
		$data["user_pass"]			= $class_security->encryptStringWithSalt($_POST["user_pass_new"], $class_security->getSalt($data["user_name"]));
		$data["user_type"]				= $class_security->makeSafeString($_POST["user_type"]);
		$data["user_email"]			= $class_security->makeSafeString($_POST["user_email"]);
		$check_mail 						= true;
		if($data["user_email"] != "")
		{
			$check_mail = $class_security->checkEmail($data["user_email"]);
		}
		if($check_mail == true)
		{
			if($data["user_name_real"] != "" && $data["user_name"] != "" && $_POST["user_pass_new"] != "")
			{
				if($class_user->createNewUser($data))
				{
					$smarty->assign("message","Saved new user.");
					require_once("user_overview.php");
				}
				else
				{
					$smarty->assign("data",	$data);
					$smarty->assign("error","Could not save new user, please try again.");
					$smarty->display("header.tpl");
					$smarty->display("user_new.tpl");
				}
			}
			else
			{
				$smarty->assign("data",	$data);
				$smarty->assign("warning","Please fill in all required fields.");
				$smarty->display("header.tpl");
				$smarty->display("user_new.tpl");
			}
		}
		else
		{
			$smarty->assign("data",	$data);
			$smarty->assign("warning","The given e-mail address is invalid.");
			$smarty->display("header.tpl");
			$smarty->display("user_new.tpl");
		}
	}
	else
	{
		$smarty->display("header.tpl");
		$smarty->display("user_new.tpl");
	}
?>