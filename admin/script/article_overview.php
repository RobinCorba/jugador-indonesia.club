<?php
    
    Modules::load("news");
    
	if(isset($_GET["del"]))
	{
		if($class_news->deleteArticle($class_security->makeSafeNumber($_GET["del"])))
		{
			$smarty->assign("message","Article has been deleted.");
		}
		else
		{
			$smarty->assign("error","Could not delete article.");
		}
	}
	$smarty->assign("articles", News::retrieveAllArticles());
	$smarty->assign("seo_title", "News overview");
	$smarty->assign("seo_desc", "An overview of all news articles.");
	$smarty->display("header.tpl");
	$smarty->display("article_overview.tpl");
	
?>