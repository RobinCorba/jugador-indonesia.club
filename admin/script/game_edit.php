<?php
	
	if(!empty($_POST))
	{
		$_POST = $class_security->makeSafeArray($_POST);
		$result = $class_football->editGame($_POST);
		if($result[0] === true)
		{
			$smarty->assign("message", "Saved changes to game ".$_POST["game_score"]." (".$_POST["game_result"].").");
			include("games.php");
			exit();
		}
		else
		{
			$smarty->assign("error", "Error occurred: ".$result[1]);
			$smarty->assign("game", $_POST);
		}
	}
	$smarty->assign("game", $class_football->retrieveGame($class_security->makeSafeString($_GET["id"])));
	$smarty->assign("players", $class_football->retrievePlayers(false));
	$smarty->assign("opponents", $class_football->retrieveOpponents());
	$smarty->display("games_new.tpl");
	
?>