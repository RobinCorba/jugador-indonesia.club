<?php
	
	if(!empty($_POST))
	{
		$_POST = $class_security->makeSafeArray($_POST);
		$result = $class_football->editOpponent($_POST);
		if($result[0] === true)
		{
			$smarty->assign("message", "Saved changes to opponent team ".$_POST["opponent_name"].".");
			include("opponents.php");
			exit();
		}
		else
		{
			$smarty->assign("error", "Error occurred: ".$result[1]);
			$smarty->assign("opponent", $_POST);
		}
	}
	$smarty->assign("opponent", $class_football->retrieveOpponent($class_security->makeSafeString($_GET["id"])));
	$smarty->display("opponents_new.tpl");
	
?>