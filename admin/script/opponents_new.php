<?php
	
	if(!empty($_POST))
	{
		$_POST = $class_security->makeSafeArray($_POST);
		$result = $class_football->addOpponent($_POST);
		if($result[0] === true)
		{
			$smarty->assign("message", "Opponent team ".$_POST["opponent_name"]." has been added.");
			include("opponents.php");
			exit();
		}
		else
		{
			$smarty->assign("error", "Error occurred: ".$result[1]);
			$smarty->assign("opponent", $_POST);
		}
	}
	$smarty->display("opponents_new.tpl");
	
?>