<?php
    
    $class_news = Modules::load("news");
    
    include("library/class.image.php");
    $class_image = new Image();
    
	
	if(!empty($_POST["save_type"]))
	{
		$data["article_category"]		= $class_security->makeSafeString($_POST["article_category"]);
		$data["article_title"]			= $class_security->makeSafeString($_POST["article_title"]);
		$data["article_content"]		= addslashes($_POST["article_content"]);
		$data["article_description"]	= $class_security->makeSafeString($_POST["article_description"]);
		$data["article_keywords"]		= trim(strtolower($class_security->makeSafeString($_POST["article_keywords"])));
        
        // Remove the last comma from the list of keywords
		if(substr($data["article_keywords"], -1) == ",")
		{
			$data["article_keywords"] = substr($data["article_keywords"], 0, -1);
		}
        
        // Check whether this article needs to be saved as a draft or published
		if($_POST["save_type"] == "Save draft")
		{
			$data["article_status"]	= "draft";
		}
		elseif($_POST["save_type"] == "Publish article")
		{
			$data["article_status"]	= "published";
		}
        
		// Check if all mandatory fields are filled
		if($data["article_title"] == "")
		{
			$smarty->assign("warning", "This article needs a title.");
			$smarty->assign("data", $data);
		}
		elseif($data["article_content"] == "")
		{
			$smarty->assign("warning", "This article needs some content.");
			$smarty->assign("data", $data);
		}
		elseif($data["article_description"] == "")
		{
			$smarty->assign("warning", "This article needs a description.");
			$smarty->assign("data", $data);
		}
		elseif($data["article_keywords"] == "")
		{
			$smarty->assign("warning", "This article needs some keywords.");
			$smarty->assign("data", $data);
		}
		else
		{
			// All required fields filled in, process the picture
			if(!empty($_FILES["article_picture"]["name"]))
			{
				$thumb_sizes	= array(array(340, 280),array(200,200));
				$result			= $class_image->uploadImageWithThumbnails($_FILES["article_picture"], $data["article_title"], DIR_IMG_NEWS, $thumb_sizes);
				if($result[0] === false)
				{
					$smarty->assign("error", "An error occured when uploading the image: " . $result[1]);
				}
				else
				{
					$data["article_img_name"] = $result[1];
				}
			}
			else
			{
				$smarty->assign("warning", "No picture was uploaded, it's strongly advisable to upload a picture!");
				$data["article_img_name"] = "common_picture.jpg";
			}
			// Process the article
			if($class_news->createArticle($data))
			{
				if($data["article_status"] == "draft")
				{
					$smarty->assign("message","New article draft saved.");
				}
				elseif($data["article_status"] == "published")
				{
					$smarty->assign("message", "New article published!");
				}
			}
			else
			{
				$smarty->assign("error","Could not save new article: ".DB::getError());
			}
		}
	}
    
    if(isset($_GET["del"]))
	{
		if($class_news->deleteArticle($class_security->makeSafeNumber($_GET["del"])))
		{
			$smarty->assign("message","Article has been deleted.");
		}
		else
		{
			$smarty->assign("error","Could not delete article.");
		}
	}
	$smarty->assign("articles", $class_news->retrieveAllArticles());
	$smarty->assign("seo_title", "News overview");
	$smarty->assign("seo_desc", "An overview of all news articles.");
    $smarty->display(dirname(__FILE__)."/templates/overview.tpl");
    
?>