<?php

	ini_set('display_errors',true);
	ERROR_REPORTING(E_ALL);
	//ini_set('display_errors', false);
	//ERROR_REPORTING(NULL);
	
	// Timezone
	date_default_timezone_set('Asia/Jakarta');
    
    // Legacy
    $url				= "http://www.jugador-indonesia.club/";
	$root				= "/var/www/html/jugador-indonesia.club/";
    
    // Define constants
	define("URL_ROOT",      "http://www.jugador-indonesia.club/");
	define("DIR_ROOT",      "/var/www/html/jugador-indonesia.club/");
    define("DIR_MODULE",    DIR_ROOT."admin/modules/");
    
    
    define("DIR_IMG_NEWS",          DIR_ROOT."media/images/news/");
	
	// Load modules
    include(DIR_ROOT."admin/library/class.modules.php");
    
	
	// PHP static function library
	
	include("../library/class.database.php");
	// PHP libraries (from CMS)
	include("library/class.security.php");
	$class_security	= new Security();
	include("library/class.common.php");
	
	$class_common	= new Common();
	include("library/class.session.php");
	$class_session	= new Session();
	include("library/class.user.php");
	$class_user		= new User();
	include("library/class.football.php");
	$class_football		= new Football();
	
	// Smarty setup
	include '../smarty/Smarty.class.php';
	$smarty = new Smarty();
	$smarty->template_dir   = $root . "admin/templates/";
	$smarty->compile_dir    = $root . "admin/compile/";
	$smarty->cache_dir      = $root . "admin/cache/";
	$smarty->caching        = 0;
	$smarty->debugging      = false;
	$smarty->assign("temp_dir", $smarty->template_dir);
	$smarty->assign("base_url", $url . "admin/");
	$smarty->assign("site_dir", $url);
    
?>