onload=function()
{
	if (document.getElementsByClassName == undefined)
	{
		document.getElementsByClassName = function(className)
		{
			var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
			var allElements = document.getElementsByTagName("*");
			var results = [];

			var element;
			for (var i = 0; (element = allElements[i]) != null; i++)
			{
				var elementClass = element.className;
				if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
				{
					results.push(element);
				}
			}

			return results;
		}
	}
	
	$("#img_upload").change(function(){         readURL(this,"img_upload_preview", "img");	            });
	$("#img_card_upload").change(function(){    readURL(this,"img_card_upload_preview", "background");	});
}


function confirmDelete(text, url)
{
	if(text == "")
	{
		text = "this";
	}
	if(confirm("Are you sure you want to delete " + text + "?"))
	{
		window.location = url;
	}
}

// Image upload preview
function readURL(input, id_preview_element, preview_type)
{
    if (input.files && input.files[0])
	{
        var reader = new FileReader();

        reader.onload = function (e)
		{
            if(preview_type == "img")
            {
                $('#'+id_preview_element).attr('src', e.target.result);
            }
            else if(preview_type == "background")
            {
                $('#'+id_preview_element).css('background-image', "url('"+e.target.result+"')");
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}