<?php

class Football
{
	private $table_prefix = "jugador_";
	
	function addGoal($player_id, $game_id)
	{
		$result = DB::query("INSERT INTO ".$this->table_prefix."goals (goal_player_id, goal_game_id) VALUES ('".$player_id."', '".$game_id."');");
		$return_values[0] = $result;
		$return_values[1] = DB::getError();
		return $return_values;
	}
    
    function addCleanSheet($player_id, $game_id)
	{
		$result = DB::query("INSERT INTO ".$this->table_prefix."clean_sheets (clean_sheet_player_id, clean_sheet_game_id) VALUES ('".$player_id."', '".$game_id."');");
		$return_values[0] = $result;
		$return_values[1] = DB::getError();
		return $return_values;
	}
	
	function retrieveGoals($game_id)
	{
		$result = DB::query("SELECT * FROM ".$this->table_prefix."goals WHERE goal_game_id = '".$game_id."';");
		$return_values[0] = $result;
		$return_values[1] = DB::getError();
		if($result === false)
		{
			return $return_values;
		}
		else
		{
			$goals = array();
			while($goal = mysqli_fetch_assoc($result))
			{
				if(empty($goals[$goal["goal_player_id"]]))
				{
					$goals[$goal["goal_player_id"]] = 1;
				}
				else
				{
					$goals[$goal["goal_player_id"]] += 1;
				}
			}
			return $goals;
		}
	}
    
    function retrieveCleanSheets($game_id)
	{
		$result = DB::query("SELECT * FROM ".$this->table_prefix."clean_sheets WHERE clean_sheet_game_id = '".$game_id."' ORDER BY clean_sheet_player_id;");
		$return_values[0] = $result;
		$return_values[1] = DB::getError();
		if($result === false)
		{
			return $return_values;
		}
		else
		{
			$clean_sheets = array();
			while($clean_sheet = mysqli_fetch_assoc($result))
			{
				if(empty($clean_sheets[$clean_sheet["clean_sheet_player_id"]]))
				{
					$clean_sheets[$clean_sheet["clean_sheet_player_id"]] = 1;
				}
				else
				{
					$clean_sheets[$clean_sheet["clean_sheet_player_id"]] += 1;
				}
			}
			return $clean_sheets;
		}
	}
	
	function retrieveGame($game_id)
	{
		$result = DB::query("SELECT * FROM ".$this->table_prefix."games WHERE game_id = '".$game_id."';");
		$return_values[0] = $result;
		$return_values[1] = DB::getError();
		if($result !== false)
		{
			$game                   = mysqli_fetch_assoc($result);
			$game["goals"]          = $this->retrieveGoals($game["game_id"]);
			$game["clean_sheets"]   = $this->retrieveCleanSheets($game["game_id"]);
			return $game;
		}
		else
		{
			return $return_values;
		}
	}
	
	function editGame($data)
	{
		$result = DB::query("UPDATE ".$this->table_prefix."games SET
		game_date_day		= '".$data["game_date_day"]."',
		game_date_month	= '".$data["game_date_month"]."',
		game_date_year	= '".$data["game_date_year"]."',
		game_opponent		= '".$data["game_opponent"]."',
		game_score			= '".$data["game_score"]."',
		game_result			= '".$data["game_result"]."'
		WHERE game_id	= '".$data["game_id"]."';");
		$return_values[0] = $result;
		$return_values[1] = DB::getError();
		if($result !== false)
		{
			// Update goals too
			DB::query("DELETE FROM ".$this->table_prefix."goals WHERE goal_game_id = '".$data["game_id"]."';");
			foreach($data["goals"] as $player_id => $total_goals)
			{
				for($i = 0; $i < $total_goals; $i++)
				{
					$this->addGoal($player_id, $data["game_id"]);
				}
			}
            // Update clean sheets too
            DB::query("DELETE FROM ".$this->table_prefix."clean_sheets WHERE clean_sheet_game_id = '".$data["game_id"]."';");
            foreach($data["clean_sheets"] as $player_id => $got_clean_sheet)
			{
                if($got_clean_sheet == "yes")
                {
                    $this->addCleanSheet($player_id, $data["game_id"]);
                }
			}
			return $return_values;
		}
	}
	
	function addGame($data)
	{
		$result = DB::query("INSERT INTO ".$this->table_prefix."games (game_date_day, game_date_month, game_date_year, game_opponent, game_score, game_result) VALUES
		('".$data["game_date_day"]."', '".$data["game_date_month"]."', '".$data["game_date_year"]."', '".$data["game_opponent"]."', '".$data["game_score"]."', '".$data["game_result"]."');");
		$return_values[0]   = $result;
		$return_values[1]   = DB::getError();
		$new_game_id        = DB::getInsertID();
		if($result === true)
		{
            // Insert goals
			foreach($data["goals"] as $player_id => $total_goals)
			{
				for($i = 0; $i < $total_goals; $i++)
				{
					$this->addGoal($player_id, $new_game_id);
				}
			}
            // Insert clean sheets
            foreach($data["clean_sheets"] as $player_id => $got_clean_sheet)
			{
                if($got_clean_sheet == "yes")
                {
                    $this->addCleanSheet($player_id, $data["game_id"]);
                }
			}
		}
		return $return_values;
	}
	
	function retrieveGames()
	{
		$opponents = $this->retrieveOpponents();
		$d = DB::query("SELECT * FROM ".$this->table_prefix."games;");
		if($d === false)
		{
			$return_values[0] = false;
			$return_values[1] = DB::getError();
			return $return_values;
		}
		$games = array();
		while($game = mysqli_fetch_assoc($d))
		{
			$game["game_opponent"] = $opponents[$game["game_opponent"]];
			$games[] = $game;
		}
		return $games;
	}
	
	function retrieveOpponents()
	{
		$d = DB::query("SELECT * FROM ".$this->table_prefix."opponents;");
		if($d === false)
		{
			$return_values[0] = false;
			$return_values[1] = DB::getError();
			return $return_values;
		}
		$opponents = array();
		while($opponent = mysqli_fetch_assoc($d))
		{
			$opponents[$opponent["opponent_id"]] = $opponent;
		}
		return $opponents;
	}
	
	function addOpponent($data)
	{
		$d = DB::query("INSERT INTO ".$this->table_prefix."opponents (opponent_name, opponent_color) VALUES ('".$data["opponent_name"]."', '".$data["opponent_color"]."');");
		$return_values[0] = $d;
		$return_values[1] = DB::getError();
		return $return_values;
	}
	
	function editOpponent($data)
	{
		$d = DB::query("UPDATE ".$this->table_prefix."opponents SET
		opponent_name = '".$data["opponent_name"]."',
		opponent_color = '".$data["opponent_color"]."'
		WHERE opponent_id = '".$data["opponent_id"]."';");
		$return_values[0] = $d;
		$return_values[1] = DB::getError();
		return $return_values;
	}
	
	function retrieveOpponent($id)
	{
		$d = DB::query("SELECT * FROM ".$this->table_prefix."opponents WHERE opponent_id = '".$id."';");
		$return_values[0] = $d;
		$return_values[1] = DB::getError();
		return mysqli_fetch_assoc($d);
	}
	
	function retrievePlayer($id)
	{
		$positions = $this->retrievePositions();
		$d = DB::query("SELECT * FROM ".$this->table_prefix."players WHERE player_id = '".$id."';");
		if($d === false)
		{
			$return_values[0] = false;
			$return_values[1] = DB::getError();
			return $return_values;
		}
		$player = mysqli_fetch_assoc($d);
		$player["player_positions"] = json_decode($player["player_positions"]);
		foreach($player["player_positions"] as $key => $value)
		{
			$player["player_positions"][$key] = $positions[$value];
		}
        
        $player["player_status_readable"] = "Active player";
        switch($player["player_status"])
        {
            case "former":      $player["player_status_readable"] = "Former player";        break;
            case "inactive":    $player["player_status_readable"] = "Inactive player";      break;
            case "new":         $player["player_status_readable"] = "New player";           break;
            case "active":      $player["player_status_readable"] = "Active player";        break;
            case "top":         $player["player_status_readable"] = "Top player";           break;
            case "legendary":   $player["player_status_readable"] = "Legendary player";     break;
        }
		return $player;
	}
	
	function retrievePlayers($include_former_players = true)
	{
		$positions = $this->retrievePositions();
		$exclude_clause = "";
		if($include_former_players !== true)
		{
			$exclude_clause = "WHERE player_status != 'former'";
		}
        
		$d = DB::query("SELECT * FROM ".$this->table_prefix."players $exclude_clause ORDER BY player_name, player_no ASC;");
		if($d === false)
		{
			$return_values[0] = false;
			$return_values[1] = DB::getError();
			return $return_values;
		}
        
		$players = array();
		while($player = mysqli_fetch_assoc($d))
		{
			$player["player_positions"] = json_decode($player["player_positions"]);
			if(is_array($player["player_positions"]))
			{
				foreach($player["player_positions"] as $key => $value)
				{
					$player["player_positions"][$key] = $positions[$value];
				}
			}
            $player["player_status_readable"] = "Active player";
            switch($player["player_status"])
            {
                case "former":      $player["player_status_readable"] = "Former player";        break;
                case "inactive":    $player["player_status_readable"] = "Inactive player";      break;
                case "new":         $player["player_status_readable"] = "New player";           break;
                case "active":      $player["player_status_readable"] = "Active player";        break;
                case "top":         $player["player_status_readable"] = "Top player";           break;
                case "legendary":   $player["player_status_readable"] = "Legendary player";     break;
            }
			$players[$player["player_id"]] = $player;
		}
		return $players;
	}
	
	function addPlayer($p)
	{
		$result = DB::query("INSERT INTO ".$this->table_prefix."players (player_name,player_display_name,player_no,player_positions,player_status,player_since_month,player_since_year, player_stopped_month,player_stopped_year, player_card_position)
		VALUES ('".$p["player_name"]."', '".$p["player_display_name"]."','".$p["player_no"]."', '".json_encode($p["player_positions"])."', '".$p["player_status"]."', '".$p["player_since_month"]."', '".$p["player_since_year"]."', '".$p["player_stopped_month"]."', '".$p["player_stopped_year"]."', '".$p["player_card_position"]."');");
		$return_values[0]   = $result;
		$return_values[1]   = DB::getError();
        $new_player_id      = DB::getInsertID();
		if(isset($p["player_picture"]))
		{
			DB::query("UPDATE ".$this->table_prefix."players SET player_picture = '".$p["player_picture"]."' WHERE player_id = '".$new_player_id."';");
		}
        if(isset($p["player_card_picture"]))
		{
			DB::query("UPDATE ".$this->table_prefix."players SET player_card_picture = '".$p["player_card_picture"]."' WHERE player_id = '".$new_player_id."';");
		}
		return $return_values;
	}
	
	function editPlayer($p, $root)
	{
		$picture_clause = "";
		$picture_card_clause = "";
        
        // Update the player picture
		if(isset($p["player_picture"]))
		{
			$old_data = $this->retrievePlayer($p["player_id"]);
			if(!empty($old_data["player_picture"]) && $old_data["player_picture"] != "default_picture.jpg")
			{
				// Remove old picture
				$sizes = array(180,120);
				foreach($sizes as $size)
				{
					unlink($root."media/images/players/thumb_".$size."_".$old_data["player_picture"]);
				}
				unlink($root."media/images/players/".$old_data["player_picture"]);
			}
			$picture_clause = "player_picture = '".$p["player_picture"]."',";
		}
        
        // Update the player card picture
        if(isset($p["player_card_picture"]))
		{
			$old_data = $this->retrievePlayer($p["player_id"]);
			if(!empty($old_data["player_card_picture"]) && $old_data["player_card_picture"] != "default_card_picture.png")
			{
				// Remove old picture
				$sizes = array(120);
				foreach($sizes as $size)
				{
					unlink($root."media/images/player_cards/thumb_".$size."_".$old_data["player_card_picture"]);
				}
				unlink($root."media/images/player_cards/".$old_data["player_card_picture"]);
			}
			$picture_card_clause = "player_card_picture = '".$p["player_card_picture"]."',";
		}

		$result = DB::query("UPDATE ".$this->table_prefix."players SET
		player_name             = '".$p["player_name"]."',
		player_display_name     = '".$p["player_display_name"]."',
		player_no               = '".$p["player_no"]."',
		player_positions        = '".json_encode($p["player_positions"])."',
		player_card_position    = '".$p["player_card_position"]."',
		player_status           = '".$p["player_status"]."',
		player_since_month      = '".$p["player_since_month"]."',
		player_stopped_month    = '".$p["player_stopped_month"]."',
        player_stopped_year     = '".$p["player_stopped_year"]."',
		".$picture_clause.$picture_card_clause."
		player_since_year = '".$p["player_since_year"]."' WHERE player_id = '".$p["player_id"]."';");
		$return_values[0] = $result;
		$return_values[1] = DB::getError();
		return $return_values;
	}
	
	function retrievePositions()
	{
		$p = DB::query("SELECT * FROM ".$this->table_prefix."positions;");
		
		if($p === false)
		{
			$return_values[0] = false;
			$return_values[1] = DB::getError();
			return $return_values;
		}
		$positions = array();
		while($position = mysqli_fetch_assoc($p))
		{
			$positions[$position["position_id"]] = $position;
		}
		return $positions;
	}
}

?>