<?php
	
	class Image
	{
		function retrieveAllImages()
		{
			Database::connect();
			$images = mysql_query("SELECT * FROM image_library ORDER BY date_uploaded DESC;");
			Database::disconnect();
			while($img = mysql_fetch_assoc($images))
			{
				$ext 										= explode(".",$img['img_url']);
				$img["img_ext"]						= $ext[1];
				$img["date_uploaded_real"] 	= date("d M. 'y H:i",$img["date_uploaded"]);
				if(isset($img["img_description"]))
				{
					$img["img_description"] 	= substr($img['img_description'],0,50).'...';
				}
				$all_img[] = $img;
			}
			return $all_img;
		}
		
		function saveImage($data)
		{
			$now = time();
			Database::connect();
			$result = mysql_query("INSERT INTO image_library (img_name, img_url, date_uploaded) VALUES ('$data[name]', '$data[img_name]', '$now');");
			Database::disconnect();
			return $result;
		}
		function editImage($data)
		{
			$now = time();
			Database::connect();
			if($data['prod_id'] == NULL){
				$where = "img_id = '$data[img_id]'";
			}else{
				if($data['prod_id']!=NULL){
					$where = "prod_id = '$data[prod_id]'";
				}else{
					$where = "img_url LIKE '%$data[slug]%'";
				}
			}
			$query = "UPDATE image_library SET img_name = '$data[name]', img_url = '$data[slug]', date_uploaded = '$now' WHERE $where;";
			$result = mysql_query($query);
			Database::disconnect();
			return $result;
		}
		
		function deleteImage($root, $id)
		{
			Database::connect();
			$img		= mysql_fetch_assoc(mysql_query("SELECT * FROM image_library WHERE img_id = '$id';"));
			$result	= mysql_query("DELETE FROM image_library WHERE img_id = '$id';");
			Database::disconnect();
			if($result === false)
			{
				return false;
			}
			else
			{
				// Delete image
				unlink($root . "media/images/" . $img["img_url"]);
				unlink($root . "media/images/thumb_75_" . $img["img_url"]);
				return true;
			}
		}
		
		/*
		*	@function uploadImageWithThumbnails Upload an image to a specific folder on the server including cropped thumbnails
		*	@param file $img The original image to upload (from $_FILES[file_name])
		*	@param string $imgnname for the new image(s) to upload (will be converted into URL friendly name)
		*	@param string $destination_path Directory path to the folder where the image(s) should be uploaded (example: /var/www/html/mysite.com/images/)
		*	@param array $thumb_sizes 2D array with all sizes of thumbs to be created
		*	@param int $quality Image quality of the thumbnails (default = 51)
		*	@param int $max_size Max allowed upload size of the original file (in Megabytes, default = 2)
		*	@param boolean $overwrite Overwrite existing images in case if it has the same file_name (default = false)
		*	@return array Returns a 2D array with two indexes [0] boolean (TRUE if image is uploaded or else it's FALSE) [1] string (uploaded image name OR error message)
		*/
		function uploadImageWithThumbnails($img, $img_name, $destination_path, $thumb_sizes, $quality = 75, $max_size = 2, $overwrite = false)
		{
			// Calculate the quality param in case it's a PNG file (different than other image types)
			$png_quality			= floor(9 * ($quality / 100));
			$max_size_bytes	= ($max_size * 1048576);
			if(empty($img["name"]))
			{
				// No name probably means no file
				$msg[0] = false;
				$msg[1] = "No image given to process or upload.";
				return $msg;
			}
			elseif($img["size"] > $max_size_bytes)
			{
				// Exceeded max. file size
				$msg[0] = false;
				$msg[1] = "Uploaded image is bigger than allowed ($max_size MB).";
				return $msg;
			}
			elseif ($img["error"] > 0)
			{
				// File is corrupted
				$msg[0] = false;
				$msg[1] = "Uploaded image is corrupted ($img[error]).";
				return $msg;
			}
			else
			{
				switch($img["type"])
				{
					case "image/gif":
					$ext = ".gif";
					break;
					
					case "image/jpeg":
					$ext = ".jpg";
					break;
					
					case "image/pjpeg":
					$ext = ".jpg";
					break;
					
					case "image/png":
					$ext = ".png";
					break;
					
					case "image/x-png":
					$ext = ".png";
					break;
					
					default:
					$ext = "unknown";
					break;
				}
				if($ext === "unknown")
				{
					// Not a supported file type
					$msg[0] = false;
					$msg[1] = "Uploaded image is not a GIF, JPG or PNG file.";
					return $msg;
				}
				else
				{
					$original_file = $this->createImageSlug($img_name, 48) . $ext;
					if($overwrite === false && file_exists($original_file))
					{
						// A file with a similar name exists
						$msg[0] = false;
						$msg[1] = "This file name ('".$original_file."') already exists in folder '$destination_path'.";
						return $msg;
					}
					else
					{
						// Calculate original file data (preparation for processing thumbs)
						$img_info	= GetImageSize($img["tmp_name"]);
						$original_width		= $img_info[0];
						$original_height		= $img_info[1];
						$ratio					= $original_width / $original_height;
						// Check what is the biggest thumbnail to make (original file has to be at least this big)
						$biggest_thumb_width		= 0;
						$biggest_thumb_height		= 0;
						if(is_array($thumb_sizes))
						{
							foreach($thumb_sizes as $t)
							{
								if(empty($t[0]) || empty($t[1]))
								{
									// Structure of 2D thumb sizes is incorrect
									$msg[0] = false;
									$msg[1] = "Unexpected thumbnail sizes given.";
									return $msg;
								}
								if($t[0] > $biggest_thumb_width)
								{
									$biggest_thumb_width = $t[0];
								}
								if($t[1] > $biggest_thumb_height)
								{
									$biggest_thumb_height = $t[1];
								}
							}
						}
						if($original_width < $biggest_thumb_width || $original_height < $biggest_thumb_height)
						{
							// Original file is smaller than biggest thumbnail
							$msg[0] = false;
							$msg[1] = "The uploaded file has to be at least ".$biggest_thumb_width."x".$biggest_thumb_height.".";
							return $msg;
						}
						else
						{
							// Upload original file
							move_uploaded_file($img["tmp_name"], $destination_path . $original_file);
							$all_imgs[] = $destination_path . $original_file;
							// Create all thumbnails
							if(is_array($thumb_sizes))
							{
								foreach($thumb_sizes as $t)
								{
									$thumb_width		= $t[0];
									$thumb_height		= $t[1];
									$thumb_ratio			= $thumb_width / $thumb_height;
									$thumb_file_name	= $destination_path . "thumb_".$thumb_width."_".$original_file;
									// If original image fits exactly this thumbnail's dimensions, save to server
									if($original_width === $thumb_width && $original_height === $thumb_height)
									{
										
										switch($ext)
										{
											case ".jpg":
												$srcimg	= imagecreatefromjpeg($destination_path . $original_file); 
												$success	= imagejpeg($srcimg, $thumb_file_name, $quality);
											break;
											
											case ".gif":
												$srcimg	= imagecreatefromgif($destination_path . $original_file); 
												$success	= imagegif($srcimg, $thumb_file_name, $quality);
											break;
											
											case ".png":
												$srcimg	= imagecreatefrompng($destination_path . $original_file); 
												$success	= imagepng($srcimg, $thumb_file_name, $png_quality);
											break;
										}
										if($success === false)
										{
											// Problem saving the thumbnail
											$msg[0] = false;
											$msg[1] = "There was a problem saving the ".$thumb_width."x".$thumb_height." thumbnail (exact size as original).";
											return $this->abortImageUpload($all_imgs, $msg);
										}
									}
									else
									{
										if($ratio > $thumb_ratio)
										{
											// Original file proportion is bigger in width
											$new_height		= $thumb_height;
											$new_width		= round(($original_width * ($new_height / $original_height)));
											$crop_x			= round(($new_width - $thumb_width) / 2);
											$crop_y			= 0;
										}
										else
										{
											// Original file proportion is bigger in height
											$new_width		= $thumb_width;
											$new_height		= round($original_height * ($new_width / $original_width));
											$crop_x			= 0;
											$crop_y			= round(($new_height - $thumb_height) / 2);
										}
										// Adjust crop starting position based on resize proportion
										$thumb_times_smaller = $original_height / $new_height;
										$crop_x 			= $crop_x * $thumb_times_smaller;
										$crop_y 			= $crop_y * $thumb_times_smaller;
										// Thumbnail canvas size
										$destimg			= imagecreatetruecolor($thumb_width,$thumb_height);
										// Prepare image based on original file
										switch($ext)
										{
											case ".jpg":
											$srcimg = imagecreatefromjpeg($destination_path . $original_file); 
											break;
											
											case ".gif":
											$srcimg = imagecreatefromgif($destination_path . $original_file); 
											break;
											
											case ".png":
											$srcimg = imagecreatefrompng($destination_path . $original_file); 
											break;
										}
										// Resize the source image into destination image
										$success = imagecopyresampled($destimg,$srcimg,0,0,$crop_x,$crop_y,$new_width,$new_height,ImageSX($srcimg),ImageSY($srcimg));
										if($success === false)
										{
											// Problem resizing the image
											$msg[0] = false;
											$msg[1] = "There was a problem resizing to a ".$thumb_width."x".$thumb_height." thumbnail.";
											return $this->abortImageUpload($all_imgs, $msg);
										}
										// Save the resized image on the server
										switch($ext)
										{
											case ".jpg":
											$success = imagejpeg($destimg, $thumb_file_name,$quality);
											break;
											
											case ".gif":
											$success = imagegif($destimg, $thumb_file_name,$quality);
											break;
											
											case ".png":
											$success = imagepng($destimg, $thumb_file_name, $png_quality);
											break;
										}
										if($success === false)
										{
											// Problem saving the resized thumbnail
											$msg[0] = false;
											$msg[1] = "There was a problem saving the resized ".$thumb_width."x".$thumb_height." thumbnail.";
											return $this->abortImageUpload($all_imgs, $msg);
										}
									}
									// Keep track of successfull images
									$all_thumbs[] = $thumb_file_name;
									// Save some resources
									unset($msg, $destimg, $srcimg, $thumb_file_name, $thumb_width, $thumb_height, $thumb_ratio, $new_width, $new_height, $crop_x, $crop_y, $thumb_times_smaller);
								}
							}
							// Everything went well!
							$msg[0] = true;
							$msg[1] = $original_file;
							return $msg;
						}
					}
				}
			}
		}
		
		/*
		*	@function createImageSlug Creates a URL-friendly version of a phrase
		*	@param string $phrase The phrase to be slugged
		*	@param int $maxLength defines the maximum length of the new slug (default = 28)
		*	@return string Returns a slug
		*/
		function createImageSlug($phrase, $maxLength = 28)
		{
			$result = strtolower($phrase);
			$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
			$result = trim(preg_replace("/[\s-]+/", " ", $result));
			$result = trim(substr($result, 0, $maxLength));
			$result = preg_replace("/\s/", "_", $result);
			$result = $result . "_" . time();
			return $result;
		}
		
		/*
		*	@function abortImageUpload Removes previously uploaded images from a batch
		*	@param array $all_imgs Array with paths to images (example: /var/www/patch/to/myimage.jpg)
		*	@param array $msg Array with a message to return (related to uploadImageWithThumbnails function)
		*	@return array Returns the $msg parameter
		*/
		function abortImageUpload($all_imgs, $msg)
		{
			foreach($all_imgs as $img)
			{
				if(file_exists($img))
				{
					unlink($img);
				}
			}
			return $msg;
		}
	}
	
?>