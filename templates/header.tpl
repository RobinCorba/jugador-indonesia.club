<!doctype html>
<html>
	<head lang="en">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<title>Jugador CF</title> 
	<meta name="description" content="Jugador CF Indonesia,the amateur football club with attitude!" />	    
	<meta name="keywords" content="Jugador, Indonesia, football, statistics">
	<meta property="og:title" content="Jugador CF Statistics">

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="fancybox/jquery.fancybox-v=2.1.5.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/font-awesome.min.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/cards.css">

	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    

	<link rel="prefetch" href="images/zoom.png">
    <link rel="stylesheet" href="css/animations.css" type="text/css">

	</head>

	<body>
	<div class="navbar navbar-fixed-top" data-activeslide="1">
        <div class="container">
        <!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>


        <div class="nav-collapse collapse navbar-responsive-collapse">
            <ul class="nav row">
                <li data-slide="1" class="col-12 col-sm-2"><a id="menu-link-1" href="#slide-1" title="Back to the start..."><i class="fa fa-home icon"></i> <span class="text">Home</span></a></li>
                <li data-slide="2" class="col-12 col-sm-2"><a id="menu-link-2" href="#slide-news" title="Read all about it"><i class="fa fa-newspaper-o icon"></i> <span class="text">News</span></a></li>
                <li data-slide="3" class="col-12 col-sm-2"><a id="menu-link-2" href="#slide-stats" title="All kinds of statistics"><i class="fa fa-line-chart icon"></i> <span class="text">Statistics</span></a></li>
                <li data-slide="4"class="col-12 col-sm-2"><a id="menu-link-3" href="#slide-team" title="View all our players"><i class="fa fa-futbol-o icon"></i> <span class="text">Team Roster</span></a></li-->
                <!--li data-slide="4"class="col-12 col-sm-2"><a id="menu-link-4" href="#slide-4" title="COMING SOON"><i class="fa fa-camera icon"></i> <span class="text">COMING SOON</span></a></li>
                <!--li data-slide="3" class="col-12 col-sm-2"><a id="menu-link-3" href="#slide-3" title="Next Section"><span class="icon icon-briefcase"></span> <span class="text">PORTFOLIO</span></a></li>
                <li data-slide="4" class="col-12 col-sm-2"><a id="menu-link-4" href="#slide-4" title="Next Section"><span class="icon icon-gears"></span> <span class="text">PROCESS</span></a></li>
                <li data-slide="5" class="col-12 col-sm-2"><a id="menu-link-5" href="#slide-5" title="Next Section"><span class="icon icon-heart"></span> <span class="text">CLIENTS</span></a></li>
                <li data-slide="6" class="col-12 col-sm-2"><a id="menu-link-6" href="#slide-6" title="Next Section"><span class="icon icon-envelope"></span> <span class="text">CONTACT</span></a></li-->
            </ul>
            <div class="row">
                <div class="col-sm-2 active-menu"></div>
            </div>
        </div><!-- /.nav-collapse -->
	</div><!-- /.container -->
	</div><!-- /.navbar -->


	<!-- === Arrows === >
	<div id="arrows">
	<div id="arrow-up" class="disabled"></div>
	<div id="arrow-down"></div>
	<div id="arrow-left" class="disabled visible-lg"></div>
	<div id="arrow-right" class="disabled visible-lg"></div>
	</div>< /.arrows -->