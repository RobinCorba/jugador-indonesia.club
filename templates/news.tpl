<div class="slide story" id="slide-news" data-slide="2">
	<div class="container">
		<div class="row title-row">
			<div class="col-12 font-thin">News</div>
		</div>
        <div class="row line-row">
            <div class="hr">&nbsp;</div>
        </div>
        {foreach from="$news" item="article"}
        <div class="row subtitle-row">
            <div class="col-12 font-thin">{$article.post_title}</div>
        </div>
        <div class="row">
            <div class="col-12 font-thin">{$article.post_content}</div>
        </div>
        {/foreach}
	</div>
</div>