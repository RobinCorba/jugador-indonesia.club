{include file="header.tpl"}
	
	<!-- === MAIN Background === -->
	<div class="slide story" id="slide-1" data-slide="1">
		<div class="container">
			<div id="home-row-1" class="row clearfix">
				<div class="col-12">
					<h1 class="font-semibold">JUGADOR CF</h1>
					<h4 class="font-thin">Fan Site &amp; Statistics Portal</h4>
				</div>
			</div>
			<div id="home-row-2" class="row clearfix">
				<div class="col-12 col-sm-4"><div class="home-hover navigation-slide" data-slide="2"><i class="fa fa-newspaper-o"></i></div><span>News</span></div>
				<div class="col-12 col-sm-4"><div class="home-hover navigation-slide" data-slide="3"><i class="fa fa-line-chart"></i></div><span>Statistics</span></div>
				<div class="col-12 col-sm-4"><div class="home-hover navigation-slide" data-slide="4"><i class="fa fa-futbol-o"></i></div><span>Team Roster</span></div>
				<!--div class="col-12 col-sm-4"><div class="home-hover navigation-slide" data-slide="4"><i class="fa fa-camera"></i></div><span>Coming soon</span></div-->
			</div>
		</div>
	</div>
	
	{include file="news.tpl"}
	
	<!-- Statistics -->
	<div class="slide story" id="slide-stats" data-slide="3">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin">Statistics</div>
			</div>
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div>
			<div class="row subtitle-row animatedParent animateOnce">
				<div class="col-12 font-thin">We have played</div>
                <h2 class="font-semibold animated growIn medium delay-500">{$stats.games.total} matches</h2>
			</div>
			<div class="row content-row stats">
				<div class="col-12 col-lg-4 col-sm-6 animatedParent animateOnce">
                    <div class="animated growIn medium delay-500">
                        <h1><i class="icon fa fa-trophy"></i></h1>
                        <h2 class="font-semibold">Total wins</h2>
                        <h1 class="font-semibold">{$stats.games.won}</h1>
                        <h3 class="font-thin">matches</h3>
                   </div>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 animatedParent animateOnce">
                    <div class="animated growIn medium delay-500">
                        <h1><i class="icon fa fa-battery-half"></i></h1>
                        <h2 class="font-semibold">Total draws</h2>
                        <h1 class="font-semibold">{$stats.games.drew}</h1>
                        <h3 class="font-thin">matches</h3>
                    </div>
				</div>
				<div class="col-12 col-lg-4 col-sm-6 animatedParent animateOnce">
                    <div class="animated growIn medium delay-500">
                        <h1><i class="icon fa fa-frown-o"></i></h1>
                        <h2 class="font-semibold">Total losses</h2>
                        <h1 class="font-semibold">{$stats.games.lost}</h1>
                        <h3 class="font-thin">matches</h3>
                    </div>
				</div>
			</div>
            
            <div class="row subtitle-row animatedParent animateOnce" style="padding:0px; margin-top:100px;">
				<div class="col-12 font-thin">Our biggest win was on</div>
				<div class="col-12 font-semibold animated growIn medium delay-500">{$stats.games.biggest_win.date}</div>
            </div>
            <div class="row subtitle-row animatedParent animateOnce" style="padding-top:0px;">
                <div class="col-12 font-thin">against</div>
                <div class="col-12 font-semibold animated growIn medium delay-500">{$stats.games.biggest_win.opponent} ({$stats.games.biggest_win.score})</div>
			</div>
            
            <!-- Most goals -->
            <div class="row content-row stats">
                <div class="row col-lg-5 subtitle-row animatedParent animateOnce" style="margin-top:100px;">
                    <h2 class="col-12 font-thin">The Top Scorer</h2>
                    <div class="col-12 font-semibold animated growIn medium delay-500"><img src="./media/images/players/{$stats.games.top_scorers.0.player_picture}" alt="" class="stats_picture" /></div>
                    <div class="col-12 font-semibold animated growIn medium delay-1000">{$stats.games.top_scorers.0.player_display_name}</div>
                </div>
                
                <div class="row col-lg-7 subtitle-row animatedParent animateOnce" data-sequence="500" style="margin-top:100px;">
                    <h2 class="col-12 font-thin">Top 10 Goal Scorers</h2>
                    <table class="top_scorers">
                        <tbody>
                            {section name=player loop=10}
                                {if $stats.games.top_scorers[player].total_goals > 0}
                                    <tr class="animated fadeInUp fast" data-id="{$smarty.section.player.iteration}">
                                        <td>{$smarty.section.player.iteration}.</td>
                                        <td>{$stats.games.top_scorers[player].player_display_name}</td>
                                        <td>{$stats.games.top_scorers[player].total_goals} goal{if $stats.games.top_scorers[player].total_goals > 1}s{/if}</td>
                                    </tr>
                                {/if}
                            {/section}
                        </tbody>
                    </table>
                </div>
            </div>
            
            <!-- Most clean sheets -->
            <div class="row content-row stats">
                <div class="row col-lg-5 subtitle-row animatedParent animateOnce" style="margin-top:100px;">
                    <h2 class="col-12 font-thin">Most Clean Sheets</h2>
                    <div class="col-12 font-semibold animated growIn medium delay-500"><img src="./media/images/players/{$stats.games.top_defenders.0.player_picture}" alt="" class="stats_picture" /></div>
                    <div class="col-12 font-semibold animated growIn medium delay-1000">{$stats.games.top_defenders.0.player_display_name}</div>
                </div>
                
                <div class="row col-lg-7 subtitle-row animatedParent animateOnce" data-sequence="500" style="margin-top:100px;">
                    <h2 class="col-12 font-thin">Top 10 Defenders</h2>
                    <table class="top_scorers">
                        <tbody>
                            {section name=player loop=10}
                                {if $stats.games.top_defenders[player].total_clean_sheets > 0}
                                    <tr class="animated fadeInUp fast" data-id="{$smarty.section.player.iteration}">
                                        <td>{$smarty.section.player.iteration}.</td>
                                        <td>{$stats.games.top_defenders[player].player_display_name}</td>
                                        <td>{$stats.games.top_defenders[player].total_clean_sheets} clean sheet{if $stats.games.top_defenders[player].total_clean_sheets > 1}s{/if}</td>
                                    </tr>
                                {/if}
                            {/section}
                        </tbody>
                    </table>
                </div>
            </div>
            
		</div>
	</div>
	
	<!-- Statistics -->
    
	<div class="slide story" id="slide-team" data-slide="4">
		<div class="container">
			<div class="row title-row" style="padding-bottom:0.5em;">
                <i class="fa fa-futbol-o"></i>
				<div class="col-12 font-semibold">TEAM ROSTER</div>
			</div>
            <div class="row line-row" id="team_roster_line">
				<div class="hr">&nbsp;</div>
			</div>
            
            <div class="row">
                <h3 class="col-12 font-thin">View each individual player</h3>
            </div>
            
            <div class="row">
                <div class="col-12">
                    {include file="card.tpl"}
                </div>
                <h3 class="col-12">
                    <input type="text" placeholder="Search a player..." name="player_search" id="player_filter" />
                    <ul class="player_list">
                    {foreach from=$stats.games.players item="player"}
                    <li class="player_list_item font-light" data-player-id="{$player.player_id}">{$player.player_display_name}</li>
                    {/foreach}
                    </ul>
                </h3>
            </div>
		</div>
	</div>
	
	<!-- Statistics -->
	<!--div class="slide story" id="slide-4" data-slide="4">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin">Coming soon</div>
			</div>
		</div>
	</div-->
	
	<!--div class="slide story" id="slide-2" data-slide="2">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin">Our last recorded game was against <span class="font-semibold">Lorem Ipsum</span></div>
			</div>
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div>
			<div class="row subtitle-row">
				<div class="col-12 font-thin">This is what <span class="font-semibold">we do best</span></div>
			</div>
			<div class="row content-row">
				<div class="col-12 col-lg-3 col-sm-6">
					<p><i class="icon icon-eye-open"></i></p>
					<h2 class="font-thin">Visual <span class="font-semibold">Identity</span></h2>
					<h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
				</div>
				<div class="col-12 col-lg-3 col-sm-6">
					<p><i class="icon icon-laptop"></i></p>
					<h2 class="font-thin">Web <span class="font-semibold">Design</span></h2>
					<h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
				</div>
				<div class="col-12 col-lg-3 col-sm-6">
					<p><i class="icon icon-tablet"></i></p>
					<h2 class="font-thin">Mobile <span class="font-semibold">Apps</span></h2>
					<h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
				</div>
				<div class="col-12 col-lg-3 col-sm-6">
					<p><i class="icon icon-pencil"></i></p>
					<h2 class="font-semibold">Development</h2>
					<h4 class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
				</div>
			</div>
		</div>
	</div-->
	
	<!-- === SLide 3 - Portfolio -->
	<!--div class="slide story" id="slide-3" data-slide="3">
		<div class="row">
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p01-large.jpg"><img class="thumb" src="images/portfolio/p01-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p02-large.jpg"><img class="thumb" src="images/portfolio/p02-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p09-large.jpg"><img class="thumb" src="images/portfolio/p09-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p010-large.jpg"><img class="thumb" src="images/portfolio/p10-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p05-large.jpg"><img class="thumb" src="images/portfolio/p05-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p06-large.jpg"><img class="thumb" src="images/portfolio/p06-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p07-large.jpg"><img class="thumb" src="images/portfolio/p07-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p08-large.jpg"><img class="thumb" src="images/portfolio/p08-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p03-large.jpg"><img class="thumb" src="images/portfolio/p03-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p04-large.jpg"><img class="thumb" src="images/portfolio/p04-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p11-large.jpg"><img class="thumb" src="images/portfolio/p11-small.jpg" alt=""></a></div>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" class="fancybox" href="images/portfolio/p12-large.jpg"><img class="thumb" src="images/portfolio/p12-small.jpg" alt=""></a></div>
		</div>
	</div-->
	
	<!--div class="slide story" id="slide-4" data-slide="4">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin">See us <span class="font-semibold">at work</span></div>
			</div>
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div>
			<div class="row subtitle-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-10 font-light">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div>
			<div class="row content-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-2">
					<p><i class="icon icon-bolt"></i></p>
					<h2 class="font-thin">Listening to<br><span class="font-semibold" >your needs</span></h2>
					<h4 class="font-thin">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</h4>
				</div>
				<div class="col-12 col-sm-2">
					<p><i class="icon icon-cog"></i></p>
					<h2 class="font-thin">Project<br><span class="font-semibold">discovery</span></h2>
					<h4 class="font-thin">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</h4>
				</div>
				<div class="col-12 col-sm-2">
					<p><i class="icon icon-cloud"></i></p>
					<h2 class="font-thin">Storming<br><span class="font-semibold">our brains</span></h2>
					<h4 class="font-thin">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</h4>
				</div>
				<div class="col-12 col-sm-2">
					<p><i class="icon icon-map-marker"></i></p>
					<h2 class="font-thin">Getting<br><span class="font-semibold">there</span></h2>
					<h4 class="font-thin">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</h4>
				</div>
				<div class="col-12 col-sm-2">
					<p><i class="icon icon-gift"></i></p>
					<h2 class="font-thin">Delivering<br><span class="font-semibold">the product</span></h2>
					<h4 class="font-thin">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</h4>
				</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div>
		</div>
	</div-->
	
	<!-- === Slide 5 === -->
	<!--div class="slide story" id="slide-5" data-slide="5">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-thin"><span class="font-semibold">Clients</span> we’ve worked with</div>
			</div>
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div>
			<div class="row subtitle-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-10 font-light">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. <br/><br/> The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero.</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div>
			<div class="row content-row">
				<div class="col-1 col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-2"><img src="images/client01.png" alt=""></div>
				<div class="col-12 col-sm-2"><img src="images/client02.png" alt=""></div>
				<div class="col-12 col-sm-2"><img src="images/client03.png" alt=""></div>
				<div class="col-12 col-sm-2"><img src="images/client04.png" alt=""></div>
				<div class="col-12 col-sm-2"><img src="images/client05.png" alt=""></div>
				<div class="col-1 col-sm-1 hidden-sm">&nbsp;</div>
			</div>
		</div>
	</div-->
	
	<!-- === Slide 6 / Contact === -->
	<!--div class="slide story" id="slide-6" data-slide="6">
		<div class="container">
			<div class="row title-row">
				<div class="col-12 font-light">Leave us a <span class="font-semibold">message</span></div>
			</div>
			<div class="row line-row">
				<div class="hr">&nbsp;</div>
			</div>
			<div class="row subtitle-row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-10 font-light">You can find us literally anywhere, just push a button and we’re there</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div>
			<div id="contact-row-4" class="row">
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
				<div class="col-12 col-sm-2 with-hover-text">
					<p><a target="_blank" href="#"><i class="icon icon-phone"></i></a></p>
					<span class="hover-text font-light ">+44 4839-4343</span></a>
				</div>
				<div class="col-12 col-sm-2 with-hover-text">
					<p><a target="_blank" href="#"><i class="icon icon-envelope"></i></a></p>
					<span class="hover-text font-light ">munter@blacktie.co</span></a>
				</div>
				<div class="col-12 col-sm-2 with-hover-text">
					<p><a target="_blank" href="#"><i class="icon icon-home"></i></a></p>
					<span class="hover-text font-light ">London, England<br>zip code 98443</span></a>
				</div>
				<div class="col-12 col-sm-2 with-hover-text">
					<p><a target="_blank" href="#"><i class="icon icon-facebook"></i></a></p>
					<span class="hover-text font-light ">facebook/blacktie_co</span></a>
				</div>
				<div class="col-12 col-sm-2 with-hover-text">
					<p><a target="_blank" href="#"><i class="icon icon-twitter"></i></a></p>
					<span class="hover-text font-light ">@BlackTie_co</span></a>
				</div>
				<div class="col-sm-1 hidden-sm">&nbsp;</div>
			</div>
		</div>
	</div-->
{include file="footer.tpl"}