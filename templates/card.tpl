<div class="player_card back">
    <div class="number"></div>
    <div class="position"></div>
    <div class="name"></div>
    <div class="stats" id="stat-1">
        <span></span>
        <span></span>
    </div>
    <div class="stats" id="stat-2">
        <span></span>
        <span></span>
    </div>
    <div class="stats" id="stat-3">
        <span></span>
        <span></span>
    </div>
    <div class="stats-footer"></div>
    <div class="picture" style="background-image:url('./media/images/player_cards/default_card_picture.png');"></div>
</div>