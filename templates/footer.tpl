</body>

	<!-- SCRIPTS -->
	<script src="js/html5shiv.js"></script>
	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<!--script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/jquery-migrate-1.2.1.min.js"></script-->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="fancybox/jquery.fancybox.pack-v=2.1.5.js"></script>
	<script src="js/script.js"></script>
	<script src='js/css3-animate-it.js'></script>
    
	{literal}
	<script>
    
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74684719-1', 'auto');
    ga('send', 'pageview');
    
	$(document).ready(function(e)
    {
		var lis = $('.nav > li');
		menu_focus( lis[0], 1 );
		
		$(".fancybox").fancybox({
			padding: 10,
			helpers: {
				overlay: {
					locked: false
				}
			}
		});
        
        var players = new Array;
        {/literal}
        {foreach from=$stats.games.players item="player"}
        {literal}
            players[{/literal}{$player.player_id}{literal}] = {/literal}{$player|@json_encode}{literal};
        {/literal}
        {/foreach}
        {literal}
        
        $("#player_filter").keyup(function()
        {
            var q_string = $(this).val().toLowerCase();
            $(".player_list_item").each(function()
            {
                if($(this).text().toLowerCase().search(q_string) === -1)
                {
                    $(this).hide();
                }
                else
                {
                    $(this).show();
                }
            });
        });
        
        $(".player_list_item").click(function()
        {
            // Reset the card
            $(".player_card .number").empty();
            $(".player_card .name").empty();
            $(".player_card #stat-1 span").empty();
            $(".player_card #stat-2 span").empty();
            $(".player_card #stat-3 span").empty();
            
            // Fill data
            var player_data = players[$(this).attr("data-player-id")];
            $(".player_card").attr("class","player_card "+player_data["player_status"]);
            $(".player_card .name").text(player_data.player_display_name);
            if(player_data.player_no > 0)
            {
                $(".player_card .number").text(player_data.player_no);
            }
            $(".player_card .position").text(player_data.player_card_position);
            $(".player_card .picture").css("background-image", "url('./media/images/player_cards/"+player_data.player_card_picture+"')");
            $(".player_card .stats-footer").text(player_data.player_status_readable);
            
            var current_stat = 1;
            
            $(".player_card #stat-"+current_stat+" span:first-child").text("Goals");
            $(".player_card #stat-"+current_stat+" span:last-child").text(player_data.total_goals);
            
            if(player_data.total_goals > 0)
            {
                current_stat += 1;
                $(".player_card #stat-"+current_stat+" span:first-child").text("Last goal");
                $(".player_card #stat-"+current_stat+" span:last-child").text(player_data.last_goal_date_formatted);
            }
            
            current_stat += 1
            $(".player_card #stat-"+current_stat+" span:first-child").text("Joined");
            $(".player_card #stat-"+current_stat+" span:last-child").text(player_data.member_since);
            
            window.location.href = "#team_roster_line";
        });
	
	});
	</script>
	{/literal}

</html>