<?php
	
	require_once("library/class.news.php");
	$class_news		= new News();
	
	$article = false;
	if(isset($_GET["article"]))
	{
		// From article id
		$article = $class_news->retrieveArticle($class_security->makeSafeNumber($_GET["article"]));
	}
	elseif(isset($_REQUEST["title"]))
	{
		// From SEO friendly slug
		$article = $class_news->retrieveArticleWithTitle($class_security->makeSafeString($_REQUEST["title"]));
	}
	
	if($article === false)
	{
		// No specific event requested or found
		$news = $class_news->retrieveRecentArticles(10);
		$smarty->assign("news", $news);
		$smarty->assign("seo_title",				"Events in the past");
		$smarty->assign("seo_description",	"Events in the past at the Boumas'.");
		$smarty->assign("seo_keywords",		"events, pictures");
	}
	else
	{
		$smarty->assign("article",			$article);
		$pics = $class_news->retrieveEventPictures($article["article_id"]);
		$smarty->assign("pictures",		$pics);
		$smarty->assign("seo_title",		$article["article_title"]);
		$smarty->assign("seo_desc",		$article["article_description"]);
		$smarty->assign("seo_tags",		$article["article_keywords"]);
	}
	$smarty->display("events.tpl");

?>