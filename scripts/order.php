<?php
	
	require_once("library/class.product.php");
	
	if(!empty($_POST["cart_contents"]))
	{
		if(!empty($_POST["action"]) && $_POST["action"] == "finish")
		{
			$order_name		= addslashes($_POST["order_name"]);
			$order_phone	= addslashes($_POST["order_phone"]);
			$order_mail		= addslashes($_POST["order_mail"]);
			$order_address	= addslashes($_POST["order_address"]);
			$order_cart		= addslashes($_POST["cart_contents"]);
			$order_number	= rand(1000, 9999) . "-" . rand(1000, 9999);
			$order_date		= date("Y/m/d H:i"). " hrs";
			$cart_content	= json_decode($_POST["cart_contents"], true);
			
			//echo "In cart: ".$order_cart;
			
			$grand_total		= 0;
			foreach($cart_content as $product)
			{
				$grand_total += $product["product_price_total"];
			}
			
			
			
			// select order number first (check unique)
			
			Database::connect();
			mysql_query("INSERT INTO orders VALUES ('', '$order_name', '$order_phone', '$order_mail', '$order_address', '$order_cart', '$grand_total', '$order_number', '$order_date','site','by-us');");
			Database::disconnect();
			
			$mail_order_details = "";
			foreach($cart_content as $product)
			{
				$mail_order_details .=
				"<tr>
					<td>".$product["product_name"]."</td>
					<td>".$product["product_price"]."</td>
					<td>".$product["product_quantity"]."</td>
					<td>".$product["product_price_total"]."</td>
				</tr>";
			}
			
			//print_r($cart_content);
			//exit();
			
			// Mail to the Bouma's
			$to 				=  "order@theboumas.net, webmaster@theboumas.net";
			$date_now	= date("F jS, Y");
			$subject		= "Order $order_number from the website";
			$message =
				'<p>Dear Bouma\'s,</p>
				<p>An order just came in from www.theboumas.net. Here are the details:</p>
				<h2>Order Details</h2>
				<table>
					<tbody>
						<tr>
							<td style="width:250px;">Product</td>
							<td style="width:100px;">Price</td>
							<td style="width:100px;">Quantity</td>
							<td style="width:150px;">Total</td>
						</tr>
						'.$mail_order_details.'
						<tr>
							<td colspan="3" style="text-align:right;">Grand total</td>
							<td>'.$grand_total.'</td>
						</tr>
					</tbody>
				</table>
				<h2>Contact Details</h2>
				<table>
					<tbody>
						<tr>
							<td>Name</td>
							<td>'.$order_name.'</td>
						</tr>
						<tr>
							<td>Phone no.</td>
							<td>'.$order_phone.'</td>
						</tr>
						<tr>
							<td>E-mail address</td>
							<td>'.$order_mail.'</td>
						</tr>
						<tr>
							<td>Delivery address</td>
							<td>'.$order_address.'</td>
						</tr>
						<tr>
							<td>Order number</td>
							<td>'.$order_number.'</td>
						</tr>
						<tr>
							<td>Order date</td>
							<td>'.$order_date.'</td>
						</tr>
					</tbody>
				</table>';
			$content = '<html>
				<body style="text-align:center;">
					<div style="margin:10px auto; cursor:default; width:580px; border:1px solid #b3c7e7; text-align:left;">
						<div style="height:32px; background:#b3c7e7; font-size:18px; font-family:Arial, Verdana; line-height:32px; padding:5px 20px 5px 20px; color:#001e96;">
							<span style="float:left;">www.theboumas.net</span>
							<span style="float:right;">E-mail notification</span>
							<div style="clear:both;"></div>
						</div>
						<div style="background:#FFF; border-top:0px; font-size:14px; font-family: Arial, Verdana; line-height:16px; margin:15px 15px 25px 15px; color:#444;">
							'.$message.'
						</div>
					</div>
					<div style="font-size:10px; color:#777; font-family:Arial; margin-bottom:20px;">
						E-mail sent from <a href="'.$url.'" style="color:#B00; text-decoration:none;">www.theboumas.net</a>.<br />
						For any technical questions, contact the system administrator at <a href="mailto:webmaster@theboumas.net" style="color:#B00; text-decoration:none;">webmaster@theboumas.net</a><br />
					</div>
				</body>
			</html>';
			
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: The Boumas\'s Website <noreply@theboumas.net>' . "\r\n";
			mail($to, $subject, $content, $headers);
			
		
			// Mail to the user
			$to 				=  $order_mail;
			$date_now	= date("F jS, Y");
			$subject		= "Your order from The Boumas' ($order_number)";
			$message =
				'<p>Dear '.$order_name.',</p>
				<p>Thank you for your order at www.theboumas.net! Here are the details of your order:</p>
				<h2>Order Details</h2>
				<table>
					<tbody>
						<tr>
							<td style="width:250px;">Product</td>
							<td style="width:100px;">Price</td>
							<td style="width:100px;">Quantity</td>
							<td style="width:150px;">Total</td>
						</tr>
						'.$mail_order_details.'
						<tr>
							<td colspan="3" style="text-align:right;">Grand total</td>
							<td>'.$grand_total.'</td>
						</tr>
					</tbody>
				</table>
				<h3>Your contact details sent us:</h3>
				<table>
					<tbody>
						<tr>
							<td>Name</td>
							<td>'.$order_name.'</td>
						</tr>
						<tr>
							<td>Phone no.</td>
							<td>'.$order_phone.'</td>
						</tr>
						<tr>
							<td>E-mail address</td>
							<td>'.$order_mail.'</td>
						</tr>
						<tr>
							<td>Delivery address</td>
							<td>'.$order_address.'</td>
						</tr>
						<tr>
							<td>Order number</td>
							<td>'.$order_number.'</td>
						</tr>
						<tr>
							<td>Order date</td>
							<td>'.$order_date.'</td>
						</tr>
					</tbody>
				</table><br />
				<br />
				<h2>How to pay</h2>
				<p><b>Please wait for our call to confirm the delivery costs!</b></p>
				<p>After that, please transfer <b>Rp. '.$grand_total.'</b> + delivery fee to:</p>
				<p>Bank Central Asia (BCA)<br />
				Account number: 870-501-8274 (Matthijs Volker Bouma)<br />
				Don\'t forget to mention your ORDER NUMBER: <b>'.$order_number.'</b></p>
				<br />
				After that, sent us an e-mail at <a href="mailto:order@theboumas.net?subject=Payment confirmation '.$order_number.'">order@theboumas.net</a> to confirm your payment.';
			$content = '<html>
				<body style="text-align:center;">
					<div style="margin:10px auto; cursor:default; width:580px; border:1px solid #b3c7e7; text-align:left;">
						<div style="height:32px; background:#b3c7e7; font-size:18px; font-family:Arial, Verdana; line-height:32px; padding:5px 20px 5px 20px; color:#001e96;">
							<span style="float:left;">www.theboumas.net</span>
							<span style="float:right;">E-mail notification</span>
							<div style="clear:both;"></div>
						</div>
						<div style="background:#FFF; border-top:0px; font-size:14px; font-family: Arial, Verdana; line-height:16px; margin:15px 15px 25px 15px; color:#444;">
							'.$message.'
						</div>
					</div>
					<div style="font-size:10px; color:#777; font-family:Arial; margin-bottom:20px;">
						E-mail sent from <a href="'.$url.'" style="color:#B00; text-decoration:none;">www.theboumas.net</a>.<br />
						For any technical questions, contact the system administrator at <a href="mailto:webmaster@theboumas.net" style="color:#B00; text-decoration:none;">webmaster@theboumas.net</a><br />
					</div>
				</body>
			</html>';
			
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: The Boumas\'s Website <noreply@theboumas.net>' . "\r\n";
			mail($to, $subject, $content, $headers);
		
		
			
			$smarty->assign("order", 				$cart_content);
			$smarty->assign("order_total",		$grand_total);
			$smarty->assign("order_number", 	$order_number);
			
			$smarty->assign("seo_title", 			$page_content["page_title"]);
			$smarty->assign("seo_desc", 		$page_content["page_desc"]);
			$smarty->assign("seo_tags", 			$page_content["page_tags"]);
			$smarty->display("order_finish.tpl");
		}
		else
		{
			$cart_content	= json_decode($_POST["cart_contents"], true);
			
			$grand_total		= 0;
			foreach($cart_content as &$product)
			{
				$product["product_price_total"] = ($product["product_price"] * $product["product_quantity"]);
				$grand_total += $product["product_price_total"];
			}
			$smarty->assign("cart_content", 	json_encode($cart_content));
			$smarty->assign("order", 				$cart_content);
			$smarty->assign("order_total",		$grand_total);
			
			$smarty->assign("seo_title", 		$page_content["page_title"]);
			$smarty->assign("seo_desc", 	$page_content["page_desc"]);
			$smarty->assign("seo_tags", 		$page_content["page_tags"]);
			$smarty->display("order_register.tpl");
		}
	}
	else
	{
		//$smarty->assign("categories", $class_product->retrieveAllCategories());
		$smarty->assign("products", $class_product->getProducts());
		$smarty->assign("seo_title", 		$page_content["page_title"]);
		$smarty->assign("seo_desc", 	$page_content["page_desc"]);
		$smarty->assign("seo_tags", 		$page_content["page_tags"]);
		$smarty->display("order.tpl");
	}

?>