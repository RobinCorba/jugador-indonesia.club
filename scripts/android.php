<?php
	
	// Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN']))
	{
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
	date_default_timezone_set('Asia/Jakarta');
	require_once("../library/class.database.php");
	require_once("../library/class.security.php");
	$class_security = new Security();
	
	foreach($_GET as $key => $value)
	{
		$_GET[$key] = $class_security->makeSafeString($value);
	}
	
	if(!empty($_GET["c"]))
	{
		switch($_GET["c"])
		{
			case "products":
			require_once("../library/class.product.php");
			$class_product = new Product();
			switch($_GET["f"])
			{
				case "get":
				$data					= array();
				$data["version"]		= $class_product->retrieveVersion();
				$data["products"]	= $class_product->getProducts(true);
				exit(json_encode($data));
				break;
				
				default:
				exit("error--not understood");
				break;
			}
			break;
			
			case "order":
			//require_once("../library/class.product.php");
			//$class_product = new Product();
			switch($_GET["f"])
			{
				case "place":
				if(!empty($_POST["address"]))
				{
					$address_info	= json_decode($_POST["address"], true);
				}
				$customer_info	= json_decode($_POST["customer"], true);
				$delivery			= $class_security->makeSafeString($_POST["delivery"]);
				$order_name		= $class_security->makeSafeString($customer_info["name"]);
				$order_phone	= $class_security->makeSafeString($customer_info["phone"]);
				$order_mail		= $class_security->makeSafeString($customer_info["mail"]);
				if($delivery != "by-customer")
				{
					$order_address	= $class_security->makeSafeString($address_info["address"]);
				}
				else
				{
					$order_address = false;
				}
				$order_cart		= addslashes($_POST["cart"]);
				$order_number	= rand(1000, 9999) . "-" . rand(1000, 9999);
				$order_date		= date("Y/m/d H:i"). " hrs";
				$cart_content	= json_decode($_POST["cart"], true);
				
				Database::connect();
				$r = mysql_query("SELECT product_id AS id, product_price AS price, product_name AS name FROM products WHERE product_status = 'available';");
				Database::disconnect();
				$product_prices = array();
				while($p = mysql_fetch_assoc($r))
				{
					$product_prices[$p["id"]]["price"] = $p["price"];
					$product_prices[$p["id"]]["name"] = $p["name"];
				}
				
				$grand_total		= 0;
				$mail_order_details = "";
				foreach($cart_content as $key => $product)
				{
					$total_price_this_product = ($product_prices[$key]["price"] * $product["total"]);
					$grand_total += $total_price_this_product;
					$mail_order_details .=
					"<tr>
						<td>".$product_prices[$key]["name"]."</td>
						<td>".$product_prices[$key]["price"]."</td>
						<td>".$product["total"]."</td>
						<td>".$total_price_this_product."</td>
					</tr>";
				}
				
				
				//$cart_content	= json_decode($_POST["cart_contents"], true);
			
				//$grand_total		= 0;
				Database::connect();
				$cart_content_db = array();
				foreach($cart_content as $key => $value)
				{
					$product_info = mysql_fetch_assoc(mysql_query("SELECT * FROM products WHERE product_id = '".$key."';"));
					$product_info["product_quantity"]		= $value["total"];
					$product_info["product_price_total"]	= $product_info["product_price"] * $value["total"];
					$cart_content_db[] = $product_info;
				}
				Database::disconnect();
				$cart_content_db = json_encode($cart_content_db);
				
				
				Database::connect();
				mysql_query("INSERT INTO orders VALUES ('', '$order_name', '$order_phone', '$order_mail', '$order_address', '$cart_content_db', '$grand_total', '$order_number', '$order_date', 'app', '$delivery');");
				Database::disconnect();
				
				require_once("../library/class.mail.php");
				$class_mail = new Mail();
				$result = $class_mail->sendConfirmation($mail_order_details, $grand_total, $order_name, $order_phone, $order_mail, $order_address, $order_number, $order_date, $delivery);
				//$result[0] = true; //TEMP
				if($result[0] === true)
				{
						// Inform the boumas
						$class_mail->mailBoumas($mail_order_details, $grand_total, $order_name, $order_phone, $order_mail, $order_address, $order_number, $order_date, $delivery);
						exit("oscar-kilo--".$order_number);
				}
				else
				{
					exit("error--".$result[1]);
				}
				//exit(json_encode($class_product->getProducts()));
				break;
				
				default:
				exit("error--not understood");
				break;
			}
			break;
			
			default:
			exit("error--not understood");
			break;
		}
	}
	
	// Retrieve products
	if(!empty($_POST["product_id"]))
	{
		require_once(dirname(__FILE__)."/../../library/class.database.php");
		require_once(dirname(__FILE__)."/../../library/class.security.php");
		$class_security = new Security();
		Database::connect();
		$r = mysql_query("SELECT * FROM products WHERE product_id = '".$class_security->makeSafeNumber($_POST["product_id"])."' LIMIT 0,1;");
		Database::disconnect();
		if($r === false)
		{
			exit(base64_encode("error--query-issue"));
		}
		else
		{
			$product = mysql_fetch_assoc($r);
			if($product["product_id"] == "")
			{
				exit(base64_encode("error--not-found"));
			}
			else
			{
				exit(base64_encode("oscar-kilo--".json_encode($product)));
			}
		}
	}
	else
	{
		exit(base64_encode("error--no-post-values"));
	}
	
	
	
?>